#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  Doge Soaker
#     by Soco soco@socosftware
#     available free from www.SoCoSoftware.com
#     Licenced under the MIT License (MIT)
#        (see license text at bottom of code)
#
#   Report bugs to bugreport@socosoftware.com
#   Submit suggestions to suggestions@socosoftware.com
#   Request support from support@socosoftware.com
#
# Donate to SoCo:
# 
#    BTC  1SoCo1gCvnMGgwUK4FrJvsPUPAUfoKa6h   BTC
#
#    LTC  LSoCoLDiViv4eK47sWDgU399vGZ7C74TSY  LTC
#
#    DOGE DSoCoDwXVGgfAjpm1PQsYeXr5HCvJMnFEj  DOGE
#
#    FTC  72SoCoCkby6j44G9L53qRmFfE55kc145vR  FTC
#
#    VTC  VbSoCopz7x9tBnULwQutSd1ZwVGqNejfUf  VTC
#
#########################################################
##
## Changes:
##
## Notes:
##    Testing /recv :Doger@doger.dogecoinirc.com PRIVMSG #silarus :No I am Spartacus!
##
########################################################
# Local Commands:
#   /dshelp    - Display available commands
#   /dsstart   - Start threads and activates operations
#   /dsstop    - Stops threads
#   /dsrestart - Stops then starts threads
#   /dsreset   - Resets active user list
#   /dspause   - Temporarily disables initiating new tipouts
#   /dsresume  - Resumes accepting new tipouts
#   /dsdebug   - Dumps stat info to debug output
#   /dssave	   - Save current config to file
#   /dsreload  - Unloads then reloads the plugin
#
# Remote Commands:
#   !users  - (operator only) displays the active user list in the channel
#   !dssoak - (operator only) manually cause soak of specified amount without sending tip
#             (this allows you to soak bot's remaining balances)
#   !help   - displays short help message
#   !dshelp - displays long help message
#   !active - user command to display count of active users
#
########################################################
# Usage:
#   1) Load plugin in XChat (xchat.org) IRC client (Window(menu)->Plugins and Scripts...)
#   2) Tip bot will collect a list of active users from all channels occupied by the XChat instance. Being 
#      in more than one channel is not supported and can have unexpected consequences.
#   4) Receiving either a channel tip or a tip from private message will trigger soaking
#   5) Soaking will split the tipped value between all users who have been active in the last 10 minutes
#      a) The tipper will not be included 
#      b) Specified ignored users will not be included (bot nicks)
#      c) A WHOIS test will be done on all included users for authentication checking
#      d) If there is no authenticated users or not enough tip to spread the minimum tip evenly, 
#         the soak will tipback the sender
#      e) If enabled, tip confirmations will be tracked and after a period of time tips that failed will
#         be retried.
#
########################################################
###### EDIT THESE LINES FOR YOUR CONFIGURATION ########
#----Name/Version----------------------
Short_Name = "Doge Soaker"
Source_Website = "https://bitbucket.org/SoCo_cpp/doge-soaker/"
Version_Name = "2.1.1.35"
Long_Name = "Doge Soaker " + Version_Name + " by SoCo soco@socosoftware.com" + " source " + Source_Website
C_IRC_Server_Name = "freenode.net" # used in twitter post
#----Options---------------------------
C_XChat_Plugin = False
C_Use_Multiprocessing = False  # won't work for XChat
C_Version_Public = True # !help command publicly reports DS version
C_DateTimeFormat = "%d/%m/%y %I:%M:%S%p" # debug date/time stamp format
C_Enable_Twitter_Status = False
C_Enable_Diaspora_Status = False
#----Files/Directories------------------
C_Data_Directory = "~/.dogesoaker/"
C_Twitter_Command = C_Data_Directory + "cltwitterpost"
C_Twitter_Max_Length = 140
C_Diaspora_Command = C_Data_Directory + "cliaspora -a dogesoaker@failure.net post public -i "
#----Tip Config-------------------------
C_Auto_Start = True # only for C_XChat_Plugin, overridden otherwise
C_Tip_Confirmation_Resending = False # enables tracking of tip confirmations and resending tips that never confirmed (Doesn't work properly for Doger)
C_Soak_Channel_Default = "#dogecoin" # Channel which is notified of soak when PM tip is received
C_TipBotNick = "Doger" # this tipbot will be used to send tips (in PM)
#------------
C_Operator_Channel = "#dogesoaker-ops4" #"#dogecoin-bots" #"#dogesoaker-ops"
C_OperatorNicks = ["SoCo_cpp", "SoCo_cpp_", "SoCo_cpp__"]
#----Min/Max Counts-------------------
C_Minimum_Tip_Value = 2
#----Timings---------------------------
C_Notify_PM_Delay = 0.5 		# seconds
C_Notify_CMD_Delay = 0.3 		# seconds 
C_Notify_Msg_Delay = 0.5 		# seconds
C_Soak_Delay = 6 				# seconds (was 12)
C_TipConfirm_Timeout = 15		# seconds wait for replies (was 12)
C_TipConfirm_Timeout_Long = 120 # seconds replies missed, likely fido lagged, long wait (not used for Doger mode)
C_User_Active_Threshold = 600 	# seconds (10 min) [default]
C_Thread_WaitStop_Timeout = 10 	# seconds 
C_Max_PrivMsg_Length = 390 		# Freenode seems to be 400, we'll dial it back slightly
C_DeadChannel_Timeout = 1200.0 	# seconds (20 Minutes)
C_HeartbeatCheck_Delay = 180.0 	# seconds (3 minutes)
C_Heartbeat_Timeout = 300.0 	# seconds (5 minutes)
C_ActiveUser_Check_Rate = 120.0 # seconds (2 min = 120 sec)
#----Debug---------------------------
C_DEBUG_Simulate = False   # required for !test, substitutes Operator for Tipbot and Operator channel for soak channel for simulation testing
Debug_RestrictUser = False 	# restricts soaks to operators only
DEBUG_Echo_ChanMsg = False	# includes channel messages in debug log
DEBUG_Echo_PM = False		# includes PrivMsgs in debug log
DEBUG_Echo_CMD = False		# includes out going Command notifications to debug log
DEBUG_Notify = False		# includes verbose notification stack debug logging
DEBUG_AuthUpdate = False
DEBUG_User_Forgotten = True
DEBUG_MultiConfirm = False
DEBUG_Each_Confirm = False
DEBUG_Language_Fallback = True
DEBUG_Language_File_Parse = False
#-------------- Log Rollover
C_Log_Rollover_Dir = "archive"
C_Max_DebugLog_Rollover_Size = 2500000L # bytes (2.5 MB)
C_Max_DebugLog_Rollover_Filemask = "debug_%Y-%m-%d_%H-%M.log"
C_Max_AuditLog_Rollover_Size = 2500000L # bytes (2.5 MB)
C_Max_AuditLog_Rollover_Filemask = "audit_%Y-%m-%d_%H-%M.log.csv"
##---------- Constants for last debug option, DebugLevel
DL_NONE  = 0
DL_ERROR = 1
DL_FAIL  = 2
DL_WARN  = 3
DL_INFO  = 4
DL_DEBUG = 5
DL_ALL   = 100
##---
C_Debug_Level = DL_ALL
C_DEBUG_Print = False
##--------------
C_Tipbot_Nick_List = [C_TipBotNick]
##--------------
C_Bot_Ignore_Nick_List = ["hardc0re",           # general channel bot
							"Halvening",		# another name for hardc0re bot?
							"ExchangeBot",      # general channel bot
							"DogeXM",           # general channel bot for http://dogexm.com
							"FeedMeCoins",      # general channel bot
							"Doge_Soaker",      # general channel bot 
							"Doge_Soaker2",     # general channel bot
							"Doge_Blaster",     # alternate tip bot (legacy)
							"BlasterBot",		# alternate tip bot (legacy)
							"ExperimentalBot",  # general channel bot
							"addiebot",         # general channel bot for  http://addie.cc
							"torch-bot",        # general channel bot
							"bowietip",			# general channel bot
							"DogeFaucetCom",	# bot
							"DogeSrv",			# general channel bot
							"DogeSoak",			# alternative Soak tip bot
							"CodicAI",
							"DogeAI",
							"DogeAI_",
							"TomRiddleBot",
							"DogeStorm",
							"Forkening",
							"SuchSoakBot",
							"KeksCore",
							"MeepSoaker",
							"LukesCore"
							]
##--------------
C_Banned_Nick_List = []
##--------------
Plugin_Name = Short_Name
Plugin_String = Plugin_Name + ": "
##--------------
Public_Server_Name = Plugin_Name
if C_Version_Public:
	Public_Server_Name = Public_Server_Name + " " + Long_Name

C_Module_HelpMessage = (
"--------------------------------------",
"Commands: ",
"   /dshelp    - Display available commands (this message)",
"   /dsstart   - Start threads and activates operations",
"   /dsstop    - Stops threads",
"   /dsrestart - Stops then starts threads",
"   /dsreset   - Resets active user list",
"   /dspause   - Temporarily disables initiating new tipouts",
"   /dsresume  - Resumes accepting new tipouts",
"   /dsstatus  - Show module status",
"   /dsdebug   - Dump state info to debug log",
"   /dssave    - Save current config to file",
"   /dsreload  - Unloads then reloads the plugin",
"--------------------------------------"
)

########################################################
# Globally disabled pylint warnings/errors:
#
# pylint: disable=W0702 
# pylint: disable=E1103
########################################################
import os, fnmatch
from datetime import datetime, date
import time
import math, sys, traceback
import inspect
#-------------------
# pylint: disable=F0401
if C_XChat_Plugin:
	import xchat
else:
	C_Auto_Start = False # override auto start
	import embirc
	
if C_Use_Multiprocessing:
	import multiprocessing #doesn't work for XCHat because AttributeError: 'xchat.XChatOut' object has no attribute 'flush',  File "/usr/lib/python2.7/multiprocessing/forking.py", line 117, in __init__,  sys.stdout.flush()
else:
	import threading
	
# pylint: enable=F0401
#-------------------

__module_name__ = Short_Name
__module_version__ = Version_Name
__module_description__ = Long_Name

#######################################################
###                 TXChatDummy                     ###
#######################################################
# pylint: disable=W0613
class TXChatContextDummy:
	def __init__(self):
		pass
		
	def command(self, Msg):
		pass
	
	def get_info(self, Str):
		return "Nick"
		
class TXChatDummy:
	def __init__(self):
		self.EAT_NONE = 0
		self.EAT_ALL = 1

	def find_context(self, channel):
		return TXChatContextDummy()
		
	def get_context(self, ):
		return TXChatContextDummy()

	def command(self, Msg):
		pass

	def hook_unload(self, Func):
		pass
		
	def hook_print(self, Str, Func):
		pass
		
	def hook_command(self, Str, Func, Help):
		pass
	
	def hook_server(self, Str, Func):
		pass
		
if not C_XChat_Plugin:
	xchat = TXChatDummy()
# pylint: enable=W0613
#######################################################
###                 Utility                         ###
#######################################################

def CmdMatch(string, Cmd):
	if string.startswith(Cmd + " ") or string == Cmd:
		return True
	else:
		return False

def BamCmdMatch(string, Cmd):
	Cmd = Cmd.lower()
	BamCmd = "!" + Cmd
	string = string.lower()
	if string.startswith(Cmd + " ") or string == Cmd or string.startswith(BamCmd + " ") or string == BamCmd:
		return True
	else:
		return False

def Dogecode(ValueString, DefaultValue):
	# "Ð200" -> (int) 200
	if len(ValueString) == 0:
		return DefaultValue # might not be an int
	if len(ValueString) >= 2 and ValueString[0] == chr(195) and ValueString[1] == chr(144): # 2 byte unicode Ð
		ValueString = ValueString[2:]
	elif len(ValueString) >= 2 and ValueString[0] == chr(198) and ValueString[1] == chr(137): # 2 byte unicode Ð
		ValueString = ValueString[2:]
	elif ValueString[0] == 'Ð': # some single Ð
		ValueString = ValueString[1:]
	if not isInt(ValueString):
		return DefaultValue # might not be an int
	return int(ValueString)

def Sleep(seconds):
	if seconds <= 0.0: 
		return
	WaitTill = time.time() + seconds # don't use time.clock() here
	while time.time() < WaitTill:
			time.sleep(0.01)

def ConStringList(word):
	s = ""
	f = True
	for w in word:
		if not f:
			s = s + " "
		s = s + w
	return s

def isInt(String):
	try:
		int(String)
		return True
	except ValueError:
		return False

def isFloat(String):
	try:
		float(String)
		return True
	except ValueError:
		return False

def BoolYN(Value):
	if Value:
		return "Yes"
	else:
		return "No"
		
def ArrayContains(Array, Value):
	try:
		Array.index(Value) #ignore reply
		return True
	except: # exception for not found is stupid
		return False
	#for a in Array:
	#	if a == Value:
	#		return True
	#return False

	
def FormatNameValue(Name, Value, NameSpace):
	Msg = ""
	Msg = Msg + Name.rjust(NameSpace) + ": "
	if Value == None:
		Msg = Msg + "None"
	elif type(Value) is int or type(Value) is float:
		Msg = Msg + str(Value)
	elif type(Value) is bool:
		Msg = Msg + BoolYN(Value)
	elif type(Value) is str:
		Msg = Msg + Value
	else:
		Msg = Msg + str(Value)
	return Msg	


def DebugLog(Level, Message):
	global DebugLogger
	if not DebugLogger:
		print("Doge Soaker Error: validate DebugLogger failed while logging message: " + Message)
		print(traceback.format_exc())
		return
	try:
		DebugLogger.DebugLog(Level, Message)
	except:
		print("Doge Soaker Error: DebugLog exception while logging message: " + Message)
		print(traceback.format_exc())

def IndexOf(StringList, String):
	try:
		return StringList.index(String)
	except: # exception for not found is stupid
		return False

#######################################################
###                 TLockable                       ###
#######################################################

class TLockable():
	def __init__(self):
		self.Locked = False
		self.Nicks = []
		self.LockTimeout = 0.0
		self.References = []
		self.mutex = threading.Lock()
		
	def TryLock(self):
		self.mutex.acquire()
		return True
		
	def Unlock(self):
		self.mutex.release()
		
#######################################################
###                 TThread                         ###
#######################################################

class TThread():
	'''
	wrapper for the Linux-like python threads, with no stop
	and only one start allowed. To stop we join then delete
	'''
	def __init__(self, MainLoop):
		global C_Use_Multiprocessing
		self.Terminate = False
		self.Running = False
		self.CrashTest = False
		self.HeartBeat = time.time()
		self.TThreadThread = None
		self.DebugLockWarnTime = 0.0
		self.__MainLoop = MainLoop
		if C_Use_Multiprocessing:
			self.mutex = multiprocessing.Lock()
		else:
			self.mutex = threading.Lock()
        
	def Start(self):
		global C_XChat_Plugin
		if self.Running:
			DebugLog(DL_WARN, "TThread::Start already running")
			return
		self.CrashTest = False
		self.HeartBeat = time.time() 
		self.Terminate = False
		if C_Use_Multiprocessing:
			self.TThreadThread = multiprocessing.Process(target=self.TThreadMainLoop)
		else:
			self.TThreadThread = threading.Thread(target=self.TThreadMainLoop)
		self.TThreadThread.start()
        
	def Stop(self):
		if self.Running:
			self.Terminate = True
			self.TThreadThread.join()
			del self.TThreadThread
			self.Running = False # incase it didn't join for some reason
		else: 
			DebugLog(DL_WARN, "TThread::Stop not running")
		#self.References = []
		#self.Locked = True
		
	def WaitStop(self):
		global C_Thread_WaitStop_Timeout
		self.Terminate = True
		WaitTill = time.time() + C_Thread_WaitStop_Timeout
		while self.Running:
			self.Terminate = True
			if time.time() >= WaitTill:
				DebugLog(DL_ERROR, "TThread::WaitStop timedout waiting for stop, ClassName: " + str(self.__class__.__name__))
				self.Running = False
				return
			time.sleep(0.1)
			
	def TryLock(self): # overridden base TLockable.TryLock
		self.mutex.acquire()
		return True

	def Unlock(self): #
		self.mutex.release()

	def SafeSleep(self, seconds):
		if seconds <= 0.0: 
			return
		if seconds <= 0.02:
			time.sleep(seconds)
			return
		WaitTill = time.time() + seconds # don't use time.clock() here
		while not self.Terminate and time.time() < WaitTill:
				time.sleep(0.01)
        
	def SetMainLoop(self, MainLoop):
		self.__MainLoop = MainLoop
        
	def TThreadMainLoop(self):
		self.Running = True
		self.__MainLoop()
		self.Running = False
        
	def TouchHeartbeat(self):
		if not self.TryLock():
			return False
		self._TouchHeartbeat()
		self.Unlock()
		return True

	def _TouchHeartbeat(self):
		self.HeartBeat = time.time()

	def TouchHeartbeatLocked(self):
		self.HeartBeat = time.time()
		
	def HeartbeatIsOlder(self, OldTime):
		if not self.TryLock():
			return False
		r = self._HeartbeatIsOlder(OldTime)
		self.Unlock()
		return r

	def _HeartbeatIsOlder(self, OldTime):
		if OldTime > self.HeartBeat:
			return True
		else:
			return False
		
	def HeartbeatAge(self):
		if not self.TryLock():
			return False
		r = self._HeartbeatAge()
		self.Unlock()
		return r

	def _HeartbeatAge(self):
		return time.time() - self.HeartBeat
	
#######################################################
###                 TUserList                       ###
#######################################################
class TUserListEntry:
	def __init__(self):
		self.Nick = ""
		self.HostUser = ""
		self.Host = ""
		self.RealName = ""
		self.AuthNick = ""
		
class TUserList(TLockable):
	def __init__(self):
		TLockable.__init__(self)
		self.Nicks = []
		self.Users = []
		self.ErrorOnNicks = False
	
	def AddNick(self, Nick):
		self.TryLock()
		self._AddNick(Nick)
		self.Unlock()
		
	def _AddNick(self, Nick):
		if self.ErrorOnNicks:
			DebugLog(DL_WARN, "TUserList::_AddNick refrenced on an ErrorOnNicks list trace: " + traceback.format_exc())
			return
		self.Nicks.append(Nick)

	def AddUniqueNick(self, Nick):
		self.TryLock()
		self._AddUniqueNick(Nick)
		self.Unlock()
		
	def _AddUniqueNick(self, Nick):
		if self.ErrorOnNicks:
			DebugLog(DL_WARN, "TUserList::_AddUniqueNick refrenced on an ErrorOnNicks list trace: " + traceback.format_exc())
			return
		if not self._ContainsNick(Nick):
			self.Nicks.append(Nick)

	def AddUniqueUser(self, Nick):
		self.TryLock()
		User = self._AddUniqueUser(Nick)
		self.Unlock()
		return User
	
	def _AddUniqueUser(self, Nick):
		User = self._FindUserByNick(Nick)
		if User == False:
			User = TUserListEntry()
			User.Nick = Nick
			self.Users.append(User)
		return User

	def FindUserByNick(self, Nick):
		self.TryLock()
		ret = self._FindUserByNick(Nick)
		self.Unlock()
		return ret

	def _FindUserByNick(self, Nick):
		for User in self.Users:
			if User.Nick == Nick:
				return User
		return False

	def FindUnAuthUserNicks(self, Nicks):
		UnAuthNicks = []
		self.TryLock()
		for Nick in Nicks:
			Auth = False
			for User in self.Users:
				if User.Nick == Nick:
					if User.AuthNick != "":
						Auth = True
					break
			if not Auth:
				UnAuthNicks.append(Nick)
		self.Unlock()
		return UnAuthNicks

	def RemoveByNick(self, RemNick):
		self.TryLock()
		self._RemoveNick(RemNick)
		self._RemoveUserByNick(RemNick)
		self.Unlock()

	def RemoveNick(self, RemNick):
		self.TryLock()
		self._RemoveNick(RemNick)
		self.Unlock()
		
	def _RemoveNick(self, RemNick):
		for idx in range(0, len(self.Nicks)):
			if self.Nicks[idx] == RemNick:
				del self.Nicks[idx]
				return

	def RemoveUserByNick(self, RemNick):
		self.TryLock()
		self._RemoveUserByNick(RemNick)
		self.Unlock()

	def _RemoveUserByNick(self, RemNick):
		#DebugLog(DL_DEBUG, "TUserList::RemoveUserByNick searching for user by Nick: " + RemNick)
		for idx in range(0, len(self.Users)):
			if self.Users[idx].Nick == RemNick:
				#DebugLog(DL_DEBUG, "TUserList::RemoveUserByNick searching found user by Nick: " + RemNick + " at idx: " + str(idx))
				del self.Users[idx]
				return
	
	def ContainsNick(self, TestNick):
		self.TryLock()
		r = self._ContainsNick(TestNick)
		self.Unlock()
		return r
        
	def _ContainsNick(self, TestNick):
		if self.ErrorOnNicks:
			DebugLog(DL_WARN, "TUserList::_ContainsNick refrenced on an ErrorOnNicks list trace: " + traceback.format_exc())
			return False
		for Nick in self.Nicks:
			if Nick == TestNick:
				return True
		return False

	def Clear(self):
		self.TryLock()
		self._Clear()
		self.Unlock()
		
	def _Clear(self):
		del self.Nicks[:]
		del self.Users[:]
		
#######################################################
###                 TDebugLogger                    ###
#######################################################

class TDebugLogger(TLockable):
	def __init__(self, DataDir):
		TLockable.__init__(self)
		self.LogFileName = False
		self.DebugLevel = False
		self.DateTimeFormat = False
		self.DataDirectory = ""
		self.BotNick = ""
		self.DebugPrint = False
		self.Init(DataDir)
		
	def Init(self, DataDir):
		self.TryLock()
		self._Init(DataDir)
		self.Unlock()
		self.DebugLog(DL_INFO, "------------Loaded " + __module_name__ + " " + __module_version__ + "------------")

	
	def _Init(self, DataDir):
		global DogeSoaker, C_Debug_Level, C_DateTimeFormat, C_DEBUG_Print, C_Log_Rollover_Dir
		self.DebugLevel = C_Debug_Level
		self.DateTimeFormat = C_DateTimeFormat
		self.DebugPrint = C_DEBUG_Print
		self.DataDirectory = DataDir
		self.BotNick = DogeSoaker.GetBotNick()
		self.LogFileName = os.path.expanduser(self.DataDirectory + self.BotNick + "/debug.log") # update value with exppanded path
		#print("Setting LogFileName to: '" + self.LogFileName + ", DataDir: '" + self.DataDirectory + "' botnick: '" + self.BotNick + "'")
		if self.DebugLevel != DL_NONE:
			Dir = os.path.dirname(self.LogFileName)
			if not os.path.isdir(Dir):
				print("Creating debug log folder")
				os.makedirs(Dir)
			Dir = os.path.expanduser(self.DataDirectory + self.BotNick + "/" + C_Log_Rollover_Dir)
			if not os.path.isdir(Dir):
				print("Creating debug log archive folder")
				os.makedirs(Dir)
			if not os.path.exists(self.LogFileName):
				print("Creating debug log file")
				open(self.LogFileName, 'w').close()  # touch log file

	def DebugLog(self, Level, Message):
		global C_Max_DebugLog_Rollover_Size, C_Max_DebugLog_Rollover_Filemask, C_Log_Rollover_Dir
		if self.LogFileName == False or self.DebugLevel == False or self.DateTimeFormat == False:
			print("Error: Debug Log not properly initialized")
			exit(1)
		self.TryLock()
		if Level <= self.DebugLevel:
			f = open(self.LogFileName, "a+") 
			if self.DebugPrint:
				print(Message)
			
			if Level == DL_NONE:
				LevelName = " NONE"
			elif Level == DL_ERROR:
				LevelName = "ERROR"
			elif Level == DL_FAIL:
				LevelName = " FAIL"
			elif Level == DL_WARN:
				LevelName = " WARN"
			elif Level == DL_INFO:
				LevelName = " INFO"
			elif Level == DL_DEBUG:
				LevelName = "DEBUG"
			elif Level == DL_ALL:
				LevelName = "  ALL"
			else:
				LevelName = "UNKOWN"
				
			f.write(datetime.now().strftime(self.DateTimeFormat) + "[" + LevelName + "]> " + Message + "\n")
			f.seek(0, os.SEEK_END)
			LogSize = f.tell()
			f.close()
			if LogSize > C_Max_DebugLog_Rollover_Size:
				RollFileName = os.path.expanduser(self.DataDirectory + self.BotNick + "/" + C_Log_Rollover_Dir + "/" + datetime.now().strftime(C_Max_DebugLog_Rollover_Filemask))
				print("DebugLog rolling over from: " + self.LogFileName + " to: " + RollFileName + " bytes: " + str(LogSize) + " bytes")
				os.rename(self.LogFileName, RollFileName)				
		#else:
		#	print("DebugLog: Level too low " + str(Level))
		self.Unlock()


#######################################################
###                 TAuditLog                       ###
#######################################################

class TAuditLog(TLockable):
	def __init__(self, DataDir):
		TLockable.__init__(self)
		self.LogFileName = False
		self.DateTimeFormat = False
		self.BotNick = ""
		self.DataDirectory = ""
		self.SimulateMode = False
		self.Init(DataDir)

	def Init(self, DataDir):
		self.TryLock()
		self._Init(DataDir)
		self.Unlock()
    
	def _Init(self, DataDir):
		global DogeSoaker, C_DEBUG_Simulate, C_DateTimeFormat, C_Log_Rollover_Dir
		self.BotNick = DogeSoaker.GetBotNick()
		self.DataDirectory = DataDir
		self.DateTimeFormat = C_DateTimeFormat
		self.SimulateMode = C_DEBUG_Simulate
		self.LogFileName = os.path.expanduser(self.DataDirectory + self.BotNick + "/audit.log.csv")
		Dir = os.path.dirname(self.LogFileName)
		if not os.path.isdir(Dir):
			print("Creating audit log folder")
			os.makedirs(Dir)
		Dir = os.path.expanduser(self.DataDirectory + self.BotNick + "/" + C_Log_Rollover_Dir)
		if not os.path.isdir(Dir):
			print("Creating audit log archive folder")
			os.makedirs(Dir)
		if not os.path.exists(self.LogFileName):
			print("Creating audit log file")
			f = open(self.LogFileName, 'w') # touch log 		
			f.write("DateTime,Type,From,Value,UserValue,ConfirmedUsers,UnconfrimedUsers,Notes\n")
			f.close()
		
	def DateTimeString(self):
		if self.DateTimeFormat == False:
			DebugLog(DL_ERROR, "TAuditLog::DateTimeString validate DateTimeFormat failed")
			exit(1)
		return datetime.now().strftime(self.DateTimeFormat)
		
	def Log(self, Message):
		global C_Max_AuditLog_Rollover_Size, C_Max_AuditLog_Rollover_Filemask, C_Log_Rollover_Dir
		if self.LogFileName == False:
			DebugLog(DL_ERROR, "TAuditLog::Log validate LogFileName failed")
			exit(1)
		self.TryLock()
		f = open(self.LogFileName, "a+") 
		f.write(Message + "\n")
		f.seek(0, os.SEEK_END)
		LogSize = f.tell()
		f.close()
		if LogSize > C_Max_AuditLog_Rollover_Size:
			RollFileName = os.path.expanduser(self.DataDirectory + self.BotNick + "/" + C_Log_Rollover_Dir + "/" + datetime.now().strftime(C_Max_AuditLog_Rollover_Filemask))
			print("AuditLog rolling over from: " + self.LogFileName + " to: " + RollFileName + " bytes: " + str(LogSize) + " bytes")
			os.rename(self.LogFileName, RollFileName)				
		self.Unlock()
		
	def LogCommaSeperated(self, Type, From, ChannelName, Value, UserValue, ConfirmedUsers, UnconfirmedUsers, Notes):
		#DebugLog(DL_DEBUG, "LogCommaSeperated type: " + Type + ", From: " + From)
		if self.SimulateMode:
			Type = "(simulate)" + Type
		Msg = self.DateTimeString() + "," + Type +  "," + From + "," + ChannelName + ","  + str(Value) + "," + str(UserValue) + "," + str(len(ConfirmedUsers)) + "," + " ".join(ConfirmedUsers) + "," + str(len(UnconfirmedUsers)) + "," + " ".join(UnconfirmedUsers) + "," + Notes
		self.Log(Msg)
		
	def LogTipback(self, FromNick, ChannelName, Value, Reason):
		self.LogCommaSeperated("Refused Soak", FromNick, ChannelName, Value, 0, [], [], Reason)
		
	def LogSoak(self, FromNick, ChannelName, Value, UserValue, ConfirmedUsers, UnconfirmedUsers, Notes, IsAnon):
		if IsAnon == True:
			FromNick = "Anonymous(" + FromNick + ")"
		self.LogCommaSeperated("Soak", FromNick, ChannelName, Value, UserValue, ConfirmedUsers, UnconfirmedUsers, Notes)
		
#######################################################
###                 TConfigFile                     ###
#######################################################
class TConfigFile(TLockable):
	def __init__(self, DataDir):
		TLockable.__init__(self)
		self.BotNick = False
		self.ConfigFileName = False
		self.DataDirectory = False
		self.OperatorNicks = []
		self.TipbotNicks = []
		self.IgnoreNicks = []
		self.IgnoreHosts = []
		self.Channels = []
		self.Init(DataDir)

	def Init(self, DataDir):
		self.TryLock()
		self._Init(DataDir)
		self.Unlock()

	def _Init(self, DataDir):
		global DogeSoaker
		self.DataDirectory = DataDir
		self.BotNick = DogeSoaker.GetBotNick()
		self.ConfigFileName = os.path.expanduser(self.DataDirectory + self.BotNick + "/dogesoaker.config") # update value with exppanded path
		Dir = os.path.dirname(self.ConfigFileName)
		if not os.path.isdir(Dir):
			print("Creating config folder")
			os.makedirs(Dir)

	def ConfigExists(self):
		return os.path.exists(self.ConfigFileName)	

	def Save(self):
		self.TryLock()
		try:
			f = open(self.ConfigFileName, "w")
			ret = self._Write(f)
			f.close()
		except:
			DebugLog(DL_WARN, "TConfigFile::Save could not open config file for writing: " + self.ConfigFileName)
			DebugLog(DL_DEBUG, traceback.format_exc())
			ret = False
		self.Unlock()
		return ret
	
	def _Write(self, f):
		global Long_Name, UserMonitors, OperatorList, TipbotList, IgnoreList, IgnoreHostList
		f.write("Application=" + Long_Name + "\n")
		f.write("CfgVer=2\n")
		for Nick in OperatorList.Nicks:
			f.write("OP-Nick=" + Nick + "\n")
		for Nick in TipbotList.Nicks:
			f.write("Tipbot-Nick=" + Nick + "\n")
		for Nick in IgnoreList.Nicks:
			f.write("Ignore-Nick=" + Nick + "\n")
		for Nick in IgnoreHostList.Nicks:
			f.write("Ignore-Host=" + Nick + "\n")
		for UserMonitor in UserMonitors.UserMonitors:
			if UserMonitor.ChannelJoined:
				AllUserNames = UserMonitor.AllUserNames()
				#CfgVer 2,  Channels=<ChannelName>,<HoldTime>,<LangID>,<LastTime>,<UserNameList>
				f.write("Channel=" + UserMonitor.ChannelName + "," + str(UserMonitor.UserHoldTime) + "," + UserMonitor.languageID + "," + str(time.time()) + "," + " ".join(AllUserNames) +  "\n")
		return True
		
	def Load(self):
		self.TryLock()
		try:
			f = open(self.ConfigFileName, "r")
			data = f.read()
			f.close()
			ret = self._Read(data)
		except:
			DebugLog(DL_WARN, "TConfigFile::Load could not open config file for reading: " + self.ConfigFileName)
			DebugLog(DL_DEBUG, traceback.format_exc())
			ret = False
		self.Unlock()
		return ret
	
	def _Read(self, data):
		global UserMonitors, OperatorList, TipbotList, IgnoreList, IgnoreHostList, C_Tipbot_Nick_List, C_TipBotNick
		OperatorList.Clear()
		TipbotList.Clear()
		IgnoreList.Clear()
		IgnoreHostList.Clear()
		C_Tipbot_Nick_List = []
		
		lines = data.split("\n")
		ConfigVer = 0
		LineNum = 0
		for line in lines:
			LineNum += 1
			line = line.strip()
			if line != "":
				if line[-1] == '\r':
					line = line[:-1] # trim carriage return if present
				fields = line.split("=")
				if len(fields) < 2:
					DebugLog(DL_WARN, "TConfigFile::_Read invalid line format on line number: " + str(LineNum))
				else:
					key = fields[0]
					value = "=".join(fields[1:]) # slice and join in case there are more equal signs
					if key == "Application":
						DebugLog(DL_INFO, "TConfigFile::_Read reading config written by: " + value)
					elif key == "CfgVer":
						ConfigVer = int(value)
						DebugLog(DL_DEBUG, "TConfigFile::_Read reading config version: " + str(ConfigVer))
					elif key == "OP-Nick":
						OperatorList.AddNick(value)
					elif key == "Tipbot-Nick":
						C_Tipbot_Nick_List.append(value)
						TipbotList.AddNick(value)
						C_TipBotNick = C_Tipbot_Nick_List[0]
					elif key == "Ignore-Nick":
						IgnoreList.AddNick(value)
					elif key == "Ignore-Host":
						IgnoreHostList.AddNick(value)
					elif key == "Channel":
						params = value.split(",")
						if len(params) < 2 or not isFloat(params[1]):
							DebugLog(DL_WARN, "TConfigFile::_Read invalid channel config format on line number: " + str(LineNum))
						else:
							if ConfigVer == 1:
								#CfgVer 1,  Channels=0<ChannelName>,1<HoldTime>,2<LastTime>,3<UserNameList>
								ChannelName = params[0]
								ActiveTime = float(params[1])
								ActiveUserTime = float(params[2])
								ActiveUsers = params[3].split(" ")
								langID = "default"
							elif ConfigVer == 2:
								#CfgVer 2,  Channels=0<ChannelName>,1<HoldTime>,2<LangID>,3<LastTime>,4<UserNameList>
								ChannelName = params[0]
								ActiveTime = float(params[1])
								langID = params[2]
								ActiveUserTime = float(params[3])
								ActiveUsers = params[4].split(" ")
							else:
								DebugLog(DL_WARN, "TConfigFile::_Read ConfigVer " + str(ConfigVer) + " not supported.")
								ChannelName = False
							if ChannelName != False:
								UserMonitor = UserMonitors.FindAddChannel(ChannelName)
								if UserMonitor == False:
									DebugLog(DL_WARN, "TConfigFile::_Read UserMonitors JoinAddChannel failed for channel: " + ChannelName + ", line number: " + str(LineNum))
								else:
									UserMonitor.ChannelName = ChannelName
									UserMonitor.UserHoldTime = ActiveTime
									UserMonitor.languageID = langID
									if date.fromtimestamp(ActiveUserTime) == date.today():
										for Nick in ActiveUsers:
											UserMonitor.InjectActiveUser(Nick, ActiveUserTime)
										UserMonitor.ForgetOldUserStats() # make sure these aren't really old active users added
					elif key[0] != "#":
						DebugLog(DL_WARN, "TConfigFile::_Read unsupported config key on line number: " + str(LineNum))
		return True
		
#######################################################
###                 TTextLanguage                    ###
#######################################################
class TTextLanguage:
	def __init__(self, languageID, languageName):
		self.languageID = languageID
		if languageName == False:
			self.Name = "(New Language)"
		else:
			self.Name = languageName
		self.fallbackLanguageID = "default"
		self.TextIDs = []
		self.Texts = []
		
	def AddText(self, textID, message):
		tidx = self.FindTextID(textID)
		if tidx != -1:
			DebugLog(DL_WARN, "TTextLanguage::AddText " + self.Name + " (" + self.languageID + ") text ID " + textID + " is already set. Ignoring...")
			DebugLog(DL_WARN, " - Previous definition: " + self.Texts[tidx])
			DebugLog(DL_WARN, " - Duplicate definition: " + message)
			return False
		self.TextIDs.append(textID)
		self.Texts.append(message)
		return True
	
	def FindTextID(self, textID):
		idx = 0
		for tid in self.TextIDs:
			if tid == textID:
				return idx
			idx = idx + 1
		return -1
		
	def GetText(self, textID, fallback = True):
		global TextHandler, DEBUG_Language_Fallback
		tidx = self.FindTextID(textID)
		if tidx == -1:
			if DEBUG_Language_Fallback == True:
				DebugLog(DL_WARN, "TTextLanguage::GetText (fallback) textID '" + textID + "' not found in langID '" + self.languageID + "', trying to fall back to languageID '" + self.fallbackLanguageID + "'")
			if not TextHandler or fallback != True:
				return "<text ID '" + textID + "' missing for language '" + self.languageID + "'>"
			flang = TextHandler.GetLanguage(self.fallbackLanguageID)
			if flang == False:
				return "<text ID '" + textID + "' failed. Fallback language '" + self.fallbackLanguageID + "' missing for language '" + self.languageID + "'>"
			return flang.GetText(textID, False)
		return self.Texts[tidx]
		

#######################################################
###                 TTextHandler                    ###
#######################################################
class TTextHandler:
	def __init__(self):
		self.defaultLanguageID = "en"
		self.Languages = []
		
	def Clear(self):
		self.Languages = []
		
	def Init(self):
		global DefaultLanguageText
		self.Clear()
		if not DefaultLanguageText or DefaultLanguageText == False or DefaultLanguageText == "":
			DebugLog(DL_WARN, "TTextHandler::Init no hardcoded DefaultLanguageText found")
		elif self.ParseFileData(DefaultLanguageText) == False:
			DebugLog(DL_WARN, "TTextHandler::Init ParseFileData DefaultLanguageText failed")
			return False
		else:
			DebugLog(DL_INFO, "TTextHandler::Init default language definition loaded")
		if self.LoadText() == False:
			DebugLog(DL_WARN, "TTextHandler::Init LoadText failed")
			return False
		return True
	
	def LoadText(self):
		global C_Data_Directory
		fileName = os.path.realpath(os.path.expanduser(C_Data_Directory + "dogesoaker.lang"))
		if os.path.exists(fileName) == False:
			DebugLog(DL_INFO, "TTextHandler::LoadText no language file found, skipping. Looked for: " + fileName)
			return True # not a failure, language file is optional
		data = False
		try:
			f = open(fileName, "r")
			data = f.read()
			f.close()
		except:
			DebugLog(DL_WARN, "TTextHandler::LoadText could not open language file for reading: " + fileName)
			DebugLog(DL_DEBUG, traceback.format_exc())
			return False
		if not data or data == False:
			DebugLog(DL_WARN, "TTextHandler::LoadText validate data read failed")
			return False
		if self.ParseFileData(data) == False:
			DebugLog(DL_WARN, "TTextHandler::LoadText ParseFileData failed")
			return False
		DebugLog(DL_INFO, "TTextHandler::LoadText loaded language file: " + fileName)
		return True
		
	def ParseFileData(self, data):
		lines = data.split("\n")
		curLanguage = False
		idx = 0
		for line in lines:
			(success, curLanguage) = self.ParseFileLine(idx, line, curLanguage)
			if success == False:
				DebugLog(DL_WARN, "TTextHandler::ParseFileData ParseFileLine " + str(idx) + " failed")
				return False
			idx = idx + 1
		return True
	
	def ParseFileLine(self, index, line, curLanguage):
		global DEBUG_Language_File_Parse
		line = line.strip()
		if line == "":
			return (True, curLanguage)
		if line.startswith("#"): # comment
			return (True, curLanguage)
		
		if line.startswith(":"): # Language section   :<LangID> <Language Readable Name>
			line = line[1:] # trim off colon
			parts = line.split(" ")
			langID = False
			langName = False
			if len(parts) > 0:
				langID = parts[0]
				if len(parts) > 1:
					langName = " ".join(parts[1:])
			if langID == False:	
				DebugLog(DL_WARN, "TTextHandler::ParseFileLine parse langauge section tag on line " + str(index) + " failed")
				return (False, curLanguage)
			curLanguage = self.AddLanguage(langID, langName)
			if DEBUG_Language_File_Parse == True:
				DebugLog(DL_DEBUG, "TTextHandler::ParseFileLine line " + str(index) + ", language section found: " + langID)
		else:
			if curLanguage == False:
				DebugLog(DL_WARN, "TTextHandler::ParseFileLine no language section specified, line " + str(index) + " failed")
				return (False, curLanguage)
			parts = line.split("=")
			if len(parts) == 0:
				DebugLog(DL_WARN, "TTextHandler::ParseFileLine parse definition line " + str(index) + " failed")
				return (False, curLanguage)
			if len(parts) > 1: # message had an = in it, concantinate them into part 1
				parts[1] = "=".join(parts[1:])
			textID = parts[0].strip()
			message = parts[1].strip()
			if message != "": # I guess a message can be blank
				if message.startswith('\"'):
					message = message[1:-1] # strip quotes
			#---------------
			# mIRC formatting
			#---------------
			# ^N normal 0xf
			# ^C color 0x3  (\3NN or \3NN,MM, N=foreground, M=background)
			# ^B bold 0x2
			# ^I italic 0x1D (29)
			# ^U underline 0x1F (31)
			#---------------
			# ^A 0x1 for actions
			# 0x04 used as placeholder for litteral escape character replaced at end.
			message = message.replace("^^", "\x04").replace("^A", "\x01").replace("^N", "\x0F").replace("^C", "\x03").replace("^B", "\x02").replace("^I", "\x1D").replace("^U", "\x1F").replace("\x04", "^") # formatting escapes
			if curLanguage.AddText(textID, message) == False:
				DebugLog(DL_WARN, "TTextHandler::ParseFileLine add text from line " + str(index) + " failed")
				return (False, curLanguage)
			if DEBUG_Language_File_Parse == True:
				DebugLog(DL_DEBUG, "TTextHandler::ParseFileLine line " + str(index) + ", text entry added (" + curLanguage.languageID + ") ID: '" + textID + "' : '" + message+ "'")
		return (True, curLanguage)
		
	def AddLanguage(self, languageID, languageName): # languageName is False when not specified
		self.Languages.append(TTextLanguage(languageID, languageName))
		return self.Languages[-1] # last one should be the new one
		
	def GetLanguage(self, languageID):
		if languageID == "default":
			return self.GetLanguage(self.defaultLanguageID)
		for l in self.Languages:
			if l.languageID == languageID:
				return l
		return False
		
	def GetLanguageIDList(self):
		rlist = ["default"]
		for l in self.Languages:
			rlist.append(l.languageID)
		return rlist
	
	def GetTextDef(self, textID):
		return self.GetTextLang(textID, "default")
		
	def GetTextChan(self, textID, ChannelName):
		global UserMonitors, DEBUG_Language_Fallback
		if ChannelName == False: # many times ChannelName is False when not set, get default
			#if DEBUG_Language_Fallback == True:
			#	DebugLog(DL_WARN, "TTTextHandler::GetTextChan (fallback) textID '" + textID + "' Channel Name is false")
			return self.GetTextLang(textID, "default")
		if not UserMonitors:
			DebugLog(DL_ERROR, "TTextHandler::GetTextChan validate UserMonitors failed")
			return "<TTTextHandler::GetTextChan failed to get text ID '" + textID + "'>"
		UserMonitor = UserMonitors.FindChannel(ChannelName)
		if UserMonitor == False:
			if DEBUG_Language_Fallback == True:
				DebugLog(DL_WARN, "TTTextHandler::GetTextChan (fallback) textID '" + textID + "' channel: " + ChannelName + " not tracked by a usermonitor")
			return self.GetTextLang(textID, "default")
		return self.GetTextLang(textID, UserMonitor.languageID)
	
	def GetTextLang(self, textID, languageID):
		lang = self.GetLanguage(languageID)
		if lang == False:
			return "<TTTextHandler::GetTextLang failed to get language '" + languageID + "' for text ID '" + textID + "'>"
		return lang.GetText(textID)
		
#######################################################
###                 TEvent                         ###
#######################################################
class TEvent(TLockable):
	
	def __init__(self):
		TLockable.__init__(self)
		self.Type = "(new object)"
		self.FromName = False
		self.ChannelName = False
		self.Done = False
		self.Value = False
		self.TimeOut = time.time() + 10.0 # time out in 10 seconds if no timeout properly set

	def SetReply(self, Type, FromName):
		self.TryLock()
		self._SetReply(Type, FromName)
		self.Unlock()
		return True
		
	def _SetReply(self, Type, FromName):
		self.Type = Type
		self.FromName = FromName
		if self.Type == "dsbal":
			self.TimeOut = time.time() + 15.0

	def SetExpectTip(self, Type, FromName, ChannelName, Value):
		self.TryLock()
		self._SetExpectTip(Type, FromName, ChannelName, Value)
		self.Unlock()
		return True
		
	def _SetExpectTip(self, Type, FromName, ChannelName, Value):
		self.Type = Type
		self.FromName = FromName
		self.ChannelName = ChannelName
		self.Value = Value
		self.TimeOut = time.time() + 60.0
		
	def HandleTimeout(self):
		self.TryLock()
		r = self._HandleTimeout()
		self.Unlock()
		return r
		
	def _HandleTimeout(self):
		global TextHandler
		if self.FromName == False:
			DebugLog(DL_WARN, "TEvent::HandleTimeout validate FromName failed")
			return False
		if self.Type == "dsbal":
			NotifyMessageThread.AddNotify("event timeout dsbal", self.FromName, TextHandler.GetTextChan("EVENT_BAL_TIMEOUT", self.ChannelName))
		elif self.Type == "donate":
			NotifyMessageThread.AddNotify("event timeout donate", self.FromName, TextHandler.GetTextChan("EVENT_DONATE_TIMEOUT", self.ChannelName))
		elif self.Type == "anonsoak":
			NotifyMessageThread.AddNotify("event timeout anonsoak", self.FromName, TextHandler.GetTextChan("EVENT_ANONSOAK_TIMEOUT", self.ChannelName))
		else:
			DebugLog(DL_WARN, "TEvent::HandleTimeout no timeout action for Type: " + self.Type)
			return False			
		return True			

	def SetDone(self):
		self.TryLock()
		self.Done = True
		self.Unlock()
		return True
			
	def IsType(self, Type):
		self.TryLock()
		if self.Type == Type:
			r = True
		else:
			r = False
		self.Unlock()
		return r

	def IsFromName(self, FromName):
		self.TryLock()
		if self.FromName != False and self.FromName.lower() == FromName.lower():
			r = True
		else:
			r = False
		self.Unlock()
		return r

	def IsFromNameValue(self, FromName, Value):
		self.TryLock()
		if self.FromName != False and self.Value != False and self.FromName.lower() == FromName.lower() and self.Value == Value:
			r = True
		else:
			r = False
		self.Unlock()
		return r
		
#######################################################
###                 TEventThread                    ###
#######################################################
class TEventThread(TThread):

	def __init__(self):
		TThread.__init__(self, self.MainLoop)
		self.Active = False
		self.Delay = 5.0 # seconds
		self.Events = []
		
	def Start(self):
		if not self.Active:
			TThread.Start(self)
		
	def Stop(self):
		self.Terminate = True
		
	def Cancel(self): 
		self.Terminate = True        
	
	def Reset(self):
		if not self.TryLock():
			return False
		self._Reset()
		self.Unlock()
		return True
		
	def _Reset(self):
		del self.Events[:]

	def IsBusy(self):
		if not self.TryLock():
			return False
		r = self._IsBusy()
		self.Unlock()
		return r
		
	def _IsBusy(self):
		if len(self.Events) == 0:
			return False
		else: 
			return True	
	def AddReplyEvent(self, Type, FromName):
		if not self.TryLock():
			return False
		r = self._AddReplyEvent(Type, FromName)
		self.Unlock()
		return r

	def _AddReplyEvent(self, Type, FromName):
		DebugLog(DL_DEBUG, "TEventThread AddReplyEvent type: " + Type + ", FromName: " + FromName)
		Event = TEvent()
		if not Event.SetReply(Type, FromName):
			return False
		self.Events.append(Event)
		return True		

	def AddExpectTipEvent(self, Type, FromName, ChannelName, Value):
		if not self.TryLock():
			return False
		r = self._AddExpectTipEvent(Type, FromName, ChannelName, Value)
		self.Unlock()
		return r

	def _AddExpectTipEvent(self, Type, FromName, ChannelName, Value):
		DebugLog(DL_DEBUG, "TEventThread _AddExpectTipEvent type: " + Type + ", FromName: " + FromName + ", Channel: " + ChannelName + ", Value: " + str(Value))
		Event = TEvent()
		if not Event.SetExpectTip(Type, FromName, ChannelName, Value):
			return False
		self.Events.append(Event)
		return True		
		
	def FindEventType(self, Type):
		if not self.TryLock():
			return False
		r = self._FindEventType(Type)
		self.Unlock()
		return r

	def _FindEventType(self, Type):
		for Event in self.Events:
			if Event.IsType(Type):
				return Event
		return False		

	def FindFromName(self, FromName):
		if not self.TryLock():
			return False
		r = self._FindFromName(FromName)
		self.Unlock()
		return r

	def _FindFromName(self, FromName):
		for Event in self.Events:
			if Event.IsFromName(FromName):
				return Event
		return False				
				
	def FindFromNameValue(self, FromName, Value):
		if not self.TryLock():
			return False
		r = self._FindFromNameValue(FromName, Value)
		self.Unlock()
		return r

	def _FindFromNameValue(self, FromName, Value):
		for Event in self.Events:
			if Event.IsFromNameValue(FromName, Value):
				return Event
		return False				

	def MainLoop(self):
		time.sleep(0.001)
		self.Active = True
		if self.Terminate: 
			return # test once, before try block to prevent Unlock
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG Events thread started")
		while not self.Terminate:
			if not self.TryLock():
				return False
			try: # try/finally trap for Terminating/Unlock'ing/DeReference'ing
				if len(self.Events):
					Modified = True # force first itteration
					while Modified:
						Modified = False
						for idx in range(0, len(self.Events)):
							Event = self.Events[idx]
							if Event.Done == True or time.time() > Event.TimeOut:
								DebugLog(DL_DEBUG, "TEventThread Event idx: " + str(idx) + " done or expired. Type: " + Event.Type + ", FromName: " + Event.FromName + ", Done: " + BoolYN(Event.Done) + ", Timeout in: " + str(Event.TimeOut - time.time()) + " sec")
								if Event.Done == False:
									Event.HandleTimeout()
								del self.Events[idx]
								Modified = True
								break # break from for loop, itterate next while loop
						
			except:
				DebugLog(DL_ERROR, "TEventThread exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
			finally:
				self.Active = False
				self.TouchHeartbeatLocked()
				self.Unlock()
			if self.Terminate: 
				return
			self.SafeSleep(self.Delay)
			if self.CrashTest == True:
				raise Exception("TEventThread Crash Test Activated!")
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG Events thread done")


#######################################################
###                 TNotify                         ###
#######################################################

class TNotify(TLockable):
	def __init__(self, FromName, ToName, Message):
		TLockable.__init__(self)
		self.FromName = FromName
		self.ToName = ToName
		self.Message = Message
        
	def Execute(self):
		self.TryLock()
		self._Execute()
		self.Unlock()
		
	def _Execute(self):
		if self.ToName == "\x01CMD":
			#DebugLog(DL_DEBUG, "Executing Notify command: " + self.Message)
			IRC.Command(self.Message)
		else:
			#DebugLog(DL_DEBUG, "Executing Notify message to " + self.ToName + ": " + self.Message)
			IRC.PM(self.ToName, self.Message)
			#DebugLog(DL_DEBUG, "Executing Notify message done")

       
#######################################################
###                 TNotifyThread                   ###
#######################################################

class TNotifyThread(TThread):

	def __init__(self):
		TThread.__init__(self, self.MainLoop)
		self.Active = False
		self.Notifications = []
		self.NotifySpeed = 4.20 # seconds, Slowest rate's delay
		self.NofifyVelocity = 0.0 # percent of NotifySpeed 
		
	def Start(self):
		if not self.Active:
			TThread.Start(self)
		
	def Clear(self):
		if not self.TryLock():
			return False
		del self.Notifications[:]
		self.Unlock()
		
	def Stop(self):
		self.Terminate = True
		
	def Cancel(self): 
		self.Terminate = True        
	
	def Reset(self):
		if not self.TryLock():
			return False
		self._Reset()
		self.Unlock()
		
	def _Reset(self):
		del self.Notifications[:]
		self.NofifyVelocity = 0.0 

	def IsBusy(self):
		if not self.TryLock():
			return False
		Result = self._IsBusy()
		self.Unlock()
		return Result
		
	def _IsBusy(self):
		if len(self.Notifications) == 0:
			return False
		else: 
			return True	
			
	def Count(self):
		if not self.TryLock():
			return False
		Result = self._Count()
		self.Unlock()
		return Result
		
	def _Count(self):
		return len(self.Notifications)

	def AddCommand(self, Message):
		#DebugLog(DL_DEBUG, "Add Notify Command: " + Message)
		if not self.TryLock():
			return False
		notify = self._AddCommand(Message)
		self.Unlock()
		return notify
		
	def _AddCommand(self, Message):
		notify = self._AddNotify("", "\x01CMD", Message)
		return notify
	
	def AddNotify(self, FromName, ToName, Message):
		#DebugLog(DL_DEBUG, "Add Notify from " + FromName + " to " + ToName + ": " + Message)
		if not self.TryLock():
			return False
		notify = self._AddNotify(FromName, ToName, Message)
		self.Unlock()
		return notify
		
	def _AddNotify(self, FromName, ToName, Message):
		notify = TNotify(FromName, ToName, Message)
		self.Notifications.append(notify)
		return notify

	def AddPriorityNotify(self, FromName, ToName, Message):
		#DebugLog(DL_DEBUG, "Add Priority Notify from " + FromName + " to " + ToName + ": " + Message)
		if not self.TryLock():
			return False
		notify = self._AddPriorityNotify(FromName, ToName, Message)
		self.Unlock()
		return notify
		
	def _AddPriorityNotify(self, FromName, ToName, Message):
		notify = TNotify(FromName, ToName, Message)
		self.Notifications.insert(0, notify)
		return notify
	
	def UpdateRateNotify(self, ChargedFee):
		if ChargedFee:
			self.NofifyVelocity = 1.50
		else:
			self.NofifyVelocity = 1.20
		
	def MainLoop(self):
		time.sleep(0.001)
		self.Active = True
		if self.Terminate: 
			return # test once, before try block to prevent Unlock
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG Notify thread started")
		while not self.Terminate:
			if not self.TryLock():
				DebugLog(DL_WARN, "TNotifyThread MainLoop try lock failed")
				return
			try: # try/finally trap for Terminating/Unlock'ing/DeReference'ing
				if len(self.Notifications):
					self.Notifications[0].Execute()
					del self.Notifications[0]
				elif self.NofifyVelocity > 0.0: # no notifications
					self.NofifyVelocity = self.NofifyVelocity - 0.15
					if self.NofifyVelocity < 0.0:
						self.NofifyVelocity = 0.0
			except:
				DebugLog(DL_ERROR, "TNotifyThread exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
			finally:
				self.Active = False
				self.TouchHeartbeatLocked()
				self.Unlock()
			if self.Terminate: 
				return
			self.SafeSleep(self.NofifyVelocity * self.NotifySpeed)
			if self.NofifyVelocity < 1.0:
				self.NofifyVelocity = self.NofifyVelocity + 0.2
			if self.CrashTest == True:
				raise Exception("TNotifyThread Crash Test Activated!")
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG Notify thread done")
        
        
#######################################################
###                 TPrivMsg                        ###
#######################################################

class TPrivMsg(TLockable):
	def __init__(self, FromName, ToName, Message):
		TLockable.__init__(self)
		self.FromName = FromName
		self.ToName = ToName
		self.Message = Message
        
	def Execute(self):
		self.TryLock()
		self._Execute()
		self.Unlock()

	def _Execute(self):
		global TipoutThread, NotifyTipThread, OperatorList, TipbotList, NotifyCommandThread, Long_Name
		global NotifyMessageThread, DogeSoaker, C_Tip_Confirmation_Resending, C_XChat_Plugin
		global C_TipBotNick, C_Tipbot_Nick_List, IgnoreList, IgnoreHostList, C_Soak_Channel_Default, UserMonitors, IRC, Long_Name, PrivMsgHandlerThread, Events

		Handled = True
		if OperatorList.ContainsNick(self.FromName):
			AuthorizedOperator = True
		else:
			AuthorizedOperator = False
			
		if TipbotList.ContainsNick(self.FromName):
			AuthorizedTipbot = True
		else:
			AuthorizedTipbot = False
			
		#DebugLog(DL_DEBUG, "PrivMsg Executing (op: " + BoolYN(AuthorizedOperator) + ", tipbot: " + BoolYN(AuthorizedTipbot) + ") from " + self.FromName + " to " + self.ToName + " : " + self.Message)
		if CmdMatch(self.Message.lower(), "balance") or CmdMatch(self.Message.lower(), "!balance") or CmdMatch(self.Message.lower(), "withdraw") or CmdMatch(self.Message.lower(), "!withdraw") or CmdMatch(self.Message.lower(), "!tip"):
			Msg = "Sorry I only automate use of the '" + C_TipBotNick + "' tipbot. For balance, withdraw, or other tipbot account features message the tipbot, " + C_TipBotNick + ". Example: /msg " + C_TipBotNick + " help"
			NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)				
		elif CmdMatch(self.Message.lower(), "dshelp") or CmdMatch(self.Message.lower(), "!dshelp") or CmdMatch(self.Message.lower(), "help") or CmdMatch(self.Message.lower(), "!help"):
			Msg = "Hi " + self.FromName + ", I keep track of active users in the " + C_Soak_Channel_Default + " channel. When someone sends me a tip, I spread it out evenly between the active users in a 'soak'. Doge Soaker is provided as a free service with absolutely no warranty or guarantee; Use at your own risk. Use !commands for command list. For disputes or issues please contact the bot operator: "
			if len(OperatorList.Nicks):
				Msg = Msg + OperatorList.Nicks[0]
			else:
				Msg = Msg + "<None!>"
			NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)		
		elif CmdMatch(self.Message.lower(), "version") or CmdMatch(self.Message.lower(), "!version"):
			NotifyMessageThread.AddNotify("help reply", self.FromName, Long_Name)			
		elif CmdMatch(self.Message.lower(), "bark") or CmdMatch(self.Message.lower(), "!bark"):
			Msg = DogeSoaker.GetBarkMessage(False)
			NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)			
		elif CmdMatch(self.Message.lower(), "active") or CmdMatch(self.Message.lower(), "!active"):
			words = self.Message.split(" ")
			Msg = ""
			if len(words) != 2:
				Msg = "Sorry, " + self.FromName + ", please specify the channel you are interested in. Example: !active " + C_Soak_Channel_Default
			else:
				ChannelName = words[1]
				Msg = self.FromName + ", " + DogeSoaker.GetActiveUserMessage(ChannelName) + " (" + ChannelName + ")"
			NotifyMessageThread.AddNotify("active reply", self.FromName, Msg)
		elif CmdMatch(self.Message.lower(), "donate") or CmdMatch(self.Message.lower(), "!donate"):
			words = self.Message.split(" ")
			Msg = ""
			if len(words) != 2 or not isInt(words[1]):
				Msg = "Sorry, " + self.FromName + ", please specify the value you are going to tip as a donation. Example: !donate 500"
			else:
				Value = int(words[1])
				Events.AddExpectTipEvent("donate", self.FromName, "(No Channe)", Value)
				Msg = self.FromName + ", the next tip for exactly Ɖ" + str(Value) + " that you send by messaging " + C_TipBotNick + " directly, within the next 60 seconds, will be kept as a donation. Example: /msg " + C_TipBotNick + " !tip " + DogeSoaker.GetBotNick() + " " + str(Value)
			NotifyMessageThread.AddNotify("donate reply", self.FromName, Msg)			
		elif CmdMatch(self.Message.lower(), "anonsoak") or CmdMatch(self.Message.lower(), "!anonsoak"):
			words = self.Message.split(" ")
			Msg = ""
			if len(words) != 3 or not isInt(words[2]):
				Msg = "Sorry, " + self.FromName + ", please specify the channel and value you are interested in anonymously soaking. Example: !anonsoak " + C_Soak_Channel_Default + " 500"
			else:
				ChannelName = words[1]
				Value = int(words[2])
				UserMonitor = UserMonitors.FindChannel(ChannelName)
				if UserMonitor == False:
					Msg = "Sorry, " + self.FromName + ", I don't seem to be tracking the " + ChannelName + " channel."
				else:
					Events.AddExpectTipEvent("anonsoak", self.FromName, ChannelName, Value)
					Msg = self.FromName + ", the next tip for exactly Ɖ" + str(Value) + " that you send by messaging " + C_TipBotNick + " directly, within the next 60 seconds, will be anonymously soaked to " + ChannelName + ". Example: /msg " + C_TipBotNick + " !tip " + DogeSoaker.GetBotNick() + " " + str(Value)
			NotifyMessageThread.AddNotify("active reply", self.FromName, Msg)			
		elif CmdMatch(self.Message.lower(), "find") or CmdMatch(self.Message.lower(), "!find"):
			MsgWords = self.Message.split(" ")
			Msg = ""
			if len(MsgWords) != 2:
				Msg = "Sorry, " + self.FromName + ", soak activity is specific to each channel. Please specify the channel you are interested in. Example: !find " + C_Soak_Channel_Default
			else:
				ChannelName = MsgWords[1]
				Msg = self.FromName + ", " + DogeSoaker.GetActiveUserMessage(ChannelName)
				UserMonitor = UserMonitors.FindChannel(ChannelName)
				if UserMonitor == False:
					Msg = "Sorry, " + self.FromName + ", Channel " + ChannelName + " not found. I don't know about channels I'm not it. Example: !find " + C_Soak_Channel_Default
				elif UserMonitor.FindUser(self.FromName) != False:
					DebugLog(DL_INFO, "User PM find request from: "+ self.FromName + ", channel: " + ChannelName + ", result: Yes")
					Msg = "Yes, " + self.FromName + ", you are in the active users list for " + ChannelName + "."						
				else:
					DebugLog(DL_INFO, "User PM find request: "+ self.FromName + ", channel: " + ChannelName + ", result: No")
					Msg = "Sorry, " + self.FromName + ", you are not in the active users list for " + ChannelName + "."
			NotifyMessageThread.AddNotify("find reply", self.FromName, Msg)		
		elif not AuthorizedOperator and (CmdMatch(self.Message.lower(), "commands") or CmdMatch(self.Message.lower(), "!commands")):
			NotifyMessageThread.AddNotify("help reply", self.FromName, "!commands - This message, !help - Long help message, !version - Long bot name and version, !bark - Soaker status, !active - Active shibe count for soak, !find - Test if you are considered active by the soaker, !donate - Tell soaker to expect a donation tip, !anonsoak - Tell soaker to expect a tip to anonymously soak a channel")
		elif not AuthorizedTipbot and not AuthorizedOperator:
			Msg = "Hi " + self.FromName + ", I am an automated chat bot. Please message me !help for more information."
			NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)
		elif AuthorizedOperator: 
			words = self.Message.split()
			if CmdMatch(self.Message, "!commands"):
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): command request")
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "!commands - this list, !soak <value> - manual soak, !pause - pauses accepting new soaks, !resume - resumes accepting new soaks, !reset - resets many internal things, !start - starts internal threads, !stop - stops internal threads, !resend <on|off> - enables resending, !time <channel> <seconds> - sets active time threshold, !resendforce - forgoes timers to begin resend process")
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "!bannick <-a|-r|-l> <UserNick> - manage banned nick list (soaks), !banhost <-a|-r|-l> <HostMask> - manage banned host list (soaks), !tipbot <TipbotNick> - change tipbot, !reconnect - reconnect to IRC server, !version - Long bot name and version, !join <channel> - join, !part <channel> [<message>]- part, !dump - dumps state to debug log, !users <channel> - list active users for channel")
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "!joinall - joins all channels in memory (loaded from config), !save - manualy saves config, !prep - prepare for soak early by validating active users, !reload - unloads then reloads the plugin module, !recover - reloads the plugin module without saving config, !dsbal - Check balance, !dspm <channel/nick> <message> - send any message, !lang <channel> <lang id>, !channels - list all channels")
				if C_XChat_Plugin == True:
					NotifyMessageThread.AddNotify("auth reply", self.FromName, "!rawlog <on|off> - enable raw client logging")
			elif CmdMatch(self.Message, "!lang"): 
				ShowUsage = True
				Msg = ""
				if len(words) < 2:
					Msg = "Valid language ID's: " + ", ".join(TextHandler.GetLanguageIDList()) + ". "
				else:
					ChannelName = words[1]
					UserMonitor = UserMonitors.FindChannel(ChannelName)
					if UserMonitor == False:
						Msg = "Language: Failed to find channel " + ChannelName + ". "
					else:
						if len(words) > 2:
							langID = words[2]
							if TextHandler.GetLanguage(langID) == False:
								Msg = "Language: language ID: " + langID + " is unknown or invalid. Valid language ID's: " + ", ".join(TextHandler.GetLanguageIDList()) + ". "
							else:
								UserMonitor.languageID = langID
								DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): channel: " + ChannelName + ", language set to: " + langID)
								Msg = "Language: channel " + ChannelName + ", language ID set to: " + UserMonitor.languageID
								ShowUsage = False
						else:
							Msg = "Language: channel " + ChannelName + ", current language ID: " + UserMonitor.languageID
							ShowUsage = False
				if ShowUsage:
					Msg += "Language usage: !lang <#channel> <language ID>"
				NotifyMessageThread.AddNotify("lang reply", self.FromName, Msg)	
			elif CmdMatch(self.Message, "!tipbot"): 
				Msg = ""
				if len(words) > 1:
					tblist = " ".join(words[1:]).split(",")
					if len(tblist):
						C_Tipbot_Nick_List = []
						TipbotList.Clear()
						for Nick in tblist:
							Nick = Nick.strip()
							C_Tipbot_Nick_List.append(Nick)
							TipbotList.AddNick(Nick)
							IgnoreList.AddUniqueNick(Nick)
						C_TipBotNick = C_Tipbot_Nick_List[0]
						Msg = Msg + "Tipbot nick changed"
					else:
						Msg = Msg + "Specify at least one nick. Tipbot nick config usage: !tipbot <MainTipbotNick>[,<OtherTipbotNick]>]"
				else:
					Msg = Msg + "Tipbot nick config usage: !tipbot <MainTipbotNick>[,<OtherTipbotNick]>]"
				Msg = Msg + ", Current tipbot nicks: " + ", ".join(C_Tipbot_Nick_List)
				NotifyMessageThread.AddNotify("auth reply", self.FromName, Msg)
			elif CmdMatch(self.Message, "!bannick"): 
				Msg = ""
				Usage = "Banned user list usage: !bannick <-a|-r|-l> <UserNick> (-a add, -r remove, -l list)"
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): ban user nick list command: " + self.Message)
				if len(words) > 2 and len(words[1]) and len(words[2]):
					Cmd = words[1]
					UserNick = words[2]
					if CmdMatch(Cmd, "-a"): 
						IgnoreList.AddUniqueNick(UserNick)
						Msg = Msg + "User nick " + UserNick + " added to banned list."
					elif CmdMatch(Cmd, "-r"): 
						IgnoreList.RemoveNick(UserNick)
						Msg = Msg + "User nick " + UserNick + " removed from banned list."
					else:
						Msg = Msg + Usage
				elif len(words) > 1 and CmdMatch(words[1], "-l"):
						Msg = Msg + "Banned user list: " + ", ".join(IgnoreList.Nicks)
				else:
					Msg = Msg + Usage
				NotifyMessageThread.AddNotify("auth reply", self.FromName, Msg)
			elif CmdMatch(self.Message, "!banhost"): 
				Msg = ""
				Usage = "Banned user list usage: !banhost <-a|-r|-l> <HostMask> (-a add, -r remove, -l list)"
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): ban user host list command: " + self.Message)
				if len(words) > 2 and len(words[1]) and len(words[2]):
					Cmd = words[1]
					UserNick = words[2]
					if CmdMatch(Cmd, "-a"): 
						IgnoreHostList.AddUniqueNick(UserNick)
						Msg = Msg + "Host mask '" + UserNick + "' added to banned list."
					elif CmdMatch(Cmd, "-r"): 
						IgnoreHostList.RemoveNick(UserNick)
						Msg = Msg + "Host mask '" + UserNick + "' removed from banned list."
					else:
						Msg = Msg + Usage
				elif len(words) > 1 and CmdMatch(words[1], "-l"):
						Msg = Msg + "Banned host mask list: '" + "', '".join(IgnoreHostList.Nicks) + "'"
				else:
					Msg = Msg + Usage
				NotifyMessageThread.AddNotify("auth reply", self.FromName, Msg)
			elif CmdMatch(self.Message, "!pause"): 
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): pausing")
				DogeSoaker.Pause()
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "pausing")
			elif CmdMatch(self.Message, "!resume"):
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): resuming")
				DogeSoaker.Resume()
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "resuming")
			elif CmdMatch(self.Message, "!reset"):
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): resetting")
				DogeSoaker.Reset()
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "resetting")
			elif CmdMatch(self.Message, "!start"):
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): starting")
				DogeSoaker.Stop()
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "starting")
			elif CmdMatch(self.Message, "!stop"):
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): stopping")
				DogeSoaker.Stop()
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "stopping")
			elif CmdMatch(self.Message, "!resend"):
				Msg = ""
				if len(words) < 2:
					words.append("bad value")
				if words[1].lower() == "on":
					C_Tip_Confirmation_Resending = True
					Msg = Msg + "Resending changed"
					DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): turned resend ON")
				elif words[1].lower() == "off":
					C_Tip_Confirmation_Resending = False
					Msg = Msg + "Resending changed"
					DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): turned resend OFF")
				else:
					Msg = Msg + "Resending usage: !resend <on|off>"
				if C_Tip_Confirmation_Resending:
					Msg = Msg + ", Current Status: on"
				else:
					Msg = Msg + ", Current Status: off"
				NotifyMessageThread.AddNotify("auth reply", self.FromName, Msg)
			elif CmdMatch(self.Message, "!time"):
				Msg = ""
				if len(words) < 2 or len(words) > 3 or (len(words) == 3 and not isInt(words[2])):
					Msg = "Active time usage: !time <#channel> <seconds>"
				else:
					ChannelName = words[1]
					UserMonitor = UserMonitors.FindChannel(ChannelName)
					if UserMonitor == False:
						Msg = "Active time: Failed to find channel " + ChannelName + ". Active time usage: !time <#channel> <seconds>"
					else:
						Msg = "Active time "
						if len(words) == 3:
							Seconds = int(words[2])
							UserMonitor.UserHoldTime = Seconds
							Msg += "changed "
							DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): channel: " + ChannelName + ", active time set to: " + str(UserMonitor.UserHoldTime))
						Msg += "for channel: " + ChannelName + ", current active time: " + str(UserMonitor.UserHoldTime) + " seconds" 
				NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)
			elif CmdMatch(self.Message, "!channel"):
				Msg = ""
				if len(words) < 2:
					Msg = Msg + "Default channel usage: !channel <#channel>"
				else:
					C_Soak_Channel_Default = words[1]
					Msg = Msg + "Default Channel changed"
					DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): default channel set to: " + C_Soak_Channel_Default)
				Msg = Msg + ", Current default channel: " + C_Soak_Channel_Default + "[!!!Warning, depreciated!!!]"
				NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)					
			elif CmdMatch(self.Message, "!resendforce"):
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): resend force issued")
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "Resend force issued.")
				DogeSoaker.Resend()
			elif CmdMatch(self.Message, "!rawlog"):
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): rawlog issued")
				if C_XChat_Plugin == True:
					NotifyMessageThread.AddNotify("auth reply", self.FromName, "rawlog: Not supported in XChat plugin mode")
				else:
					if len(words) < 2:
						NotifyMessageThread.AddNotify("auth reply", self.FromName, "rawlog usage: !rawlog <on|off> (no request of current state)")
					else:
						if " ".join(words[1:]).strip().lower() != "on":
							mode = False
							modeStr = "off"
						else:
							mode = True
							modeStr = "on"
						IRC.SetRawLog(mode)
						NotifyMessageThread.AddNotify("auth reply", self.FromName, "rawlog: set to " + modeStr)
			elif CmdMatch(self.Message, "!reconnect"):
				DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): reconnect issued")
				NotifyMessageThread.AddNotify("auth reply", self.FromName, "Recconect issued.")
				Sleep(2)
				print("DogeSoaker forcing reconnect via remote command from: " + self.FromName)
				IRC.Reconnect()
			elif CmdMatch(self.Message, "!join"):
				Msg = ""
				if len(words) < 2:
					Msg = Msg + "Join channel usage: !join <#channel>"
				else:
					ChannelName = words[1]
					Msg = Msg + "Join Channel " + ChannelName + " issued."
					DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): joined channel issued: " + ChannelName)
					IRC.Join(ChannelName)
				NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)	
			elif CmdMatch(self.Message, "!joinall"):
				DebugLog(DL_INFO, "Got user join all request from: " + self.FromName)
				ChannelNames = DogeSoaker.JoinAllChannels()
				if ChannelNames == "":
					Msg = "No channels in memory to join."
				else:
					Msg = "Attempting to join the following channels: " + ChannelNames
				NotifyMessageThread.AddNotify("joinall reply", self.FromName, Msg)
			elif CmdMatch(self.Message, "!part"):
				Msg = ""
				if len(words) < 2:
					Msg = Msg + "Part channel usage: !part <#channel> [<message>]"
				else:
					ChannelName = words[1]
					if len(words) >= 3:
						PartMsg = " ".join(words[2:])
						PartMsg = PartMsg.strip()
					else:
						PartMsg = ""
					Msg += "Part Channel " + ChannelName + " issued"
					if PartMsg != "":
						Msg += " (" + PartMsg + ")"
					DebugLog(DL_WARN, "Authorized Operator (" + self.FromName + "): part issued for Channel: " + ChannelName + ", Message: " + PartMsg)
					IRC.Part(ChannelName, PartMsg)
				NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)
			elif CmdMatch(self.Message, "!dssoak"):
				if len(words) != 3 or not isInt(words[2]):
					NotifyMessageThread.AddNotify("dssoak reply", self.FromName, "Required format: !dssoak <channel> <amount>")
				else:
					Value = int(words[2])
					ChannelName = words[1]
					UserMonitor = UserMonitors.FindChannel(ChannelName)
					if UserMonitor == False:
						NotifyMessageThread.AddNotify("dssoak reply", self.FromName, "Failed to find channel " + ChannelName + ".")
					else:
						TipID = "[-dssoak-]"
						DebugLog(DL_INFO, "DSSoak request channel: " + ChannelName + ", value: " + str(Value) + ", tip id: " + TipID)
						UserMonitor.Soak(ChannelName, self.FromName, Value, TipID, True) # True - From PM, so it shows as Anonymous
						if not TipoutThread.ConfirmedTipID(TipID):
							DebugLog(DL_WARN, "!dssoak ConfirmedTipID didn't find new PM tipout to insta-confirm TipID: " + TipID)
			elif CmdMatch(self.Message, "!dump"):
				DebugLog(DL_INFO, "Got debug dump request from: " + self.FromName)
				DogeSoaker.DebugDump()
				NotifyMessageThread.AddNotify("dump reply", self.FromName, "State info dumped to debug log.")
			elif CmdMatch(self.Message, "!users"):
				DebugLog(DL_INFO, "Got user list request from: " + self.FromName)
				ReplyMsg = ""
				UserMonitor = False
				ChannelName = False
				AllUsers = []
				if len(words) > 1:
					ChannelName = words[1]
					UserMonitor = UserMonitors.FindChannel(ChannelName)
					ReplyMsg = "(" + ChannelName + ") "
				if UserMonitor == False:
					if ChannelName != False:
						ReplyMsg = "Channel " + ChannelName + " not found. "
					ReplyMsg += "Required format: !users <channel>"
				else:
					AllUsers = UserMonitor.AllUsers()
					ReplyMsg = str(len(AllUsers)) + " active users (" + ChannelName + "): "
				for User in AllUsers:
					if (len(ReplyMsg) + len(User.Name) + 1) > C_Max_PrivMsg_Length:
						ReplyMsg += "..."
						NotifyMessageThread.AddNotify("users reply", self.FromName, ReplyMsg)
						ReplyMsg = "..."
					ReplyMsg += User.Name + "," # + "(" + ("%0.1f" % UserMonitor.RatePerMin(User.Rate)) + "),"
				NotifyMessageThread.AddNotify("users reply", self.FromName, ReplyMsg)
			elif CmdMatch(self.Message, "!crashtest"):
				DebugLog(DL_INFO, "Got user crash test request from: " + self.FromName)
				ReplyMsg = ""
				UserMonitor = False
				ChannelName = False
				if len(words) > 1:
					ChannelName = words[1]
					UserMonitor = UserMonitors.FindChannel(ChannelName)
				if UserMonitor == False:
					if ChannelName != False:
						ReplyMsg = "Channel " + ChannelName + " not found. "
					ReplyMsg += "Required format: !crashtest <channel>"
				else:
					ReplyMsg = "CrashTesting channel: " + ChannelName
				NotifyMessageThread.AddNotify("crashtest reply", self.FromName, ReplyMsg)
				if UserMonitor != False:
					try:
						UserMonitor.CrashTest()
					except:
						pass
					NotifyMessageThread.AddNotify("crashtest reply", self.FromName, "Done")
			elif CmdMatch(self.Message, "!reload") or CmdMatch(self.Message, "!recover"):
				DebugLog(DL_INFO, "Got user reload/recover request from: " + self.FromName)
				IRC.PM(self.FromName, "Issuing plugin reload/recover. No message will be sent when ready again, use a query to test for ready. Remember you may want to issue a !prep command.")
				Sleep(2.0)
				if CmdMatch(self.Message, "!recover"):
					DogeSoaker.AutoSave = False
				IRC.ReloadPlugin()
			elif CmdMatch(self.Message, "!save"):
				DebugLog(DL_INFO, "Got user save request from: " + self.FromName)
				if DogeSoaker.SaveConfig():
					Msg = "Config saved."
				else:
					Msg = "Config save failed."
				NotifyMessageThread.AddNotify("save reply", self.FromName, Msg)
			elif CmdMatch(self.Message, "!dsver") or CmdMatch(self.Message, "!version"):
				DebugLog(DL_INFO, "Got user version request from: " + self.FromName)
				NotifyMessageThread.AddNotify("save reply", self.FromName, Long_Name)					
			elif CmdMatch(self.Message, "!prep"):
				DebugLog(DL_INFO, "Got user prep request from: " + self.FromName)
				WhoisCount = 0
				for UserMonitor in UserMonitors.UserMonitors:
					UnAuthNicks = TipoutThread.FindUnAuthUserNicks(UserMonitor.AllUserNames())
					for Nick in UnAuthNicks:
						NotifyCommandThread.AddCommand("WHOIS " + Nick)
						WhoisCount += 1
				NotifyMessageThread.AddNotify("prep reply", self.FromName, "Queued " + str(WhoisCount) + " users for verfication.")								
				DebugLog(DL_INFO, "Prep request from: " + self.FromName + " DONE")
			elif CmdMatch(self.Message, "!dsbal"):
				DebugLog(DL_INFO, "Got user balance request from: " + self.FromName)
				Events.AddReplyEvent("dsbal", self.FromName)
				NotifyCommandThread.AddNotify("balance request", C_TipBotNick, "!balance")
			elif CmdMatch(self.Message, "!dspm"):
				if len(words) < 3:
					NotifyMessageThread.AddNotify("dspm reply", self.FromName, "Required format: !dspm <channel/nick> <message>")
				else:
					Dest = words[1]
					Message = ' '.join(words[2:])
					DebugLog(DL_INFO, "Got operator pm request from: " + self.FromName + ", Dest: " + Dest + ", Message: " + Message)
					NotifyMessageThread.AddNotify("dspm", Dest, Message)
			elif CmdMatch(self.Message, "!channels"):
				ChannelNameList = UserMonitors.GetChannelNames(True, False) # IncludeJoined, IncludeUnjoined
				JoinedChannels = ", ".join(ChannelNameList)
				ChannelNameList = UserMonitors.GetChannelNames(False, True) # IncludeJoined, IncludeUnjoined
				UnjoinedChannels = ", ".join(ChannelNameList)
				Msg = "Channels joined: " + JoinedChannels
				if JoinedChannels == "":
					Msg += "<none>"
				if UnjoinedChannels != "":
					Msg += " - Just in memory: " + UnjoinedChannels
				NotifyMessageThread.AddNotify("channels", self.FromName, Msg)
			else:
				Handled = False
		else:
			Handled = False
		if Handled == False and AuthorizedTipbot:
			#DebugLog(DL_DEBUG, "PrivMsg Executing from detected tip bot")
			if self.Message.startswith(DogeSoaker.GetBotNick() + ": Done "): # AuthorizedTipbot, (single) tip confirm from Doger
				# SoCo_cpp: Done [6315b33e]
				DebugLog(DL_ERROR, "Single tip not supported")
				# pylint: disable=W0212
				# use private version, because TPrivMsg.Execute is called while PrivMsgHandlerThread is already locked
				# Disabled access of protected member warning, since we are manually locking/unlocking and accessing internal function directly
				PrivMsgHandlerThread._ForgetSpannedMessage(self.FromName, self.ToName)
				# pylint: enable=W0212
			elif self.Message.startswith(DogeSoaker.GetBotNick() + ": Tipped: "): # AuthorizedTipbot, mtip confirm from Doger
				# SoCo_cpp: Tipped: Doge_Soaker 2 DogeXM 50 [b56f900b] Failed: User1 (offline)
				words = self.Message.split()
				TippedIdx = IndexOf(words, "Tipped:")
				FailedIdx = IndexOf(words, "Failed:")
				
				if TippedIdx == False:
					DebugLog(DL_WARN, "PM tip confirm has not Tipped field. Bad message: " + self.Message)
					return # bad message
					
				if FailedIdx != False:
					TipWords = words[TippedIdx+1:FailedIdx]
					FailWords = words[FailedIdx+1:]
				else:
					TipWords = words[TippedIdx+1:]
					FailWords = []
				TipID = TipWords[-1]
				# pylint: disable=W0212
				# use private version, because TPrivMsg.Execute is called while PrivMsgHandlerThread is already locked
				# Disabled access of protected member warning, since we are manually locking/unlocking and accessing internal function directly
				if len(TipID) != 10 or TipID[0] != "[" or TipID[-1] != "]":
					DebugLog(DL_DEBUG, "PM tip confirm doesn't have a valid TipID, assuming spanned message. Expected to be TipID: " + TipID + ", message: " + self.Message)
					PrivMsgHandlerThread._SetSpannedMessage(self.FromName, self.ToName, self.Message) 
					return
				# use private version, because TPrivMsg.Execute is called while PrivMsgHandlerThread is already locked
				PrivMsgHandlerThread._ForgetSpannedMessage(self.FromName, self.ToName)
				# pylint: enable=W0212
				TipWords = TipWords[:-1] # trim TipID from words
				idx = 0
				ConfValue = False
				ConfUsers = []
				FailUsers = []
				for word in TipWords:
					if (idx % 2) == 0: # name
						#DebugLog(DL_DEBUG, "PM tip confirmed [" + str(idx) + "](name): " + word)
						ConfUsers.append(word)
					else: # value
						#DebugLog(DL_DEBUG, "PM tip confirmed [" + str(idx) + "](value): " + word)
						if not isInt(word):
							DebugLog(DL_WARN, "PM tip confirmed bad value: " + word + ", at TipWord idx: " + str(idx) + ", words: " + " ".join(words) + ", message: " + self.Message)
							return # bad message
						Value = int(word)
						if ConfValue != Value:
							if ConfValue == False:
								ConfValue = Value
							else:
								DebugLog(DL_WARN, "PM tip confirmed mismatching value at TipWord idx: " + str(idx) + ", Value: " + str(Value) + " ConfValue: " + str(ConfValue))
								return # bad message
					idx += 1
				if FailedIdx != False:
					idx = 0
					for word in FailWords:
						if (idx % 2) == 0: # name
							#DebugLog(DL_DEBUG, "PM tip confirmed failure [" + str(idx) + "](name): " + word)
							FailUsers.append(word)
						#else: # reason
							#DebugLog(DL_DEBUG, "PM tip confirmed failure [" + str(idx) + "](reason): " + word)
						idx += 1
				if len(ConfUsers):
					TipoutThread.UpdateTipConfirmMulti(ConfUsers, ConfValue)
				if len(FailUsers):
					TipoutThread.TipFailDetectedMulti(FailUsers, ConfValue)
			elif self.Message.startswith("Such "): # AuthorizedTipbot, Doger privmsg tip
				# 0    1    2    3      4  5 
				#Such User has tipped you Ɖ1 (to claim /msg Doger help) [869cb109]
				# pylint: disable=W0212
				# use private version, because TPrivMsg.Execute is called while PrivMsgHandlerThread is already locked
				# Disabled access of protected member warning, since we are manually locking/unlocking and accessing internal function directly
				PrivMsgHandlerThread._ForgetSpannedMessage(self.FromName, self.ToName)
				# pylint: enable=W0212
				words = self.Message.split()
				if len(words) < 5:
					DebugLog(DL_INFO, "Bad Doger prvmsg tip word count (" + str(len(words)) + ") " + self.Message)
					return # bad message
				if words[2] != "has" or words[3] != "tipped" or words[4] != "you":
					DebugLog(DL_INFO, "Bad Doger prvmsg tip 'just sent you' section (" + str(len(words)) + ") " + self.Message)
					return # bad message
				SendUser = words[1]
				if len(SendUser) == 0:
					DebugLog(DL_INFO, "Bad Doger prvmsg tip Nick '" + SendUser + "': " + self.Message)
					return # bad message
				ValueString = words[5]
				Value = Dogecode(ValueString, -1)
				if Value <= 0: # -1 fail to parse, 0 invalid tip amount
					DebugLog(DL_INFO, "Bad Doger prvmsg tip value: " + self.Message + " * ValueString: '" + ValueString + "', Value: " + str(Value))
					return # bad message
				TipID = words[-1]
				if len(TipID) != 10:
					DebugLog(DL_INFO, "PM bad ID length TipID: " + TipID)
					return # bad message
				if TipID[0] != "[" or TipID[-1] != "]":
					DebugLog(DL_INFO, "PM bad ID TipID: " + TipID)
					return # bad message
				if TipoutThread.ConfirmedTipID(TipID):
					DebugLog(DL_DEBUG, "PM tip found existing tipout for matching id, updated if applicable, TipID: " + TipID)
					return # Tipout already exist waiting for this confirmation, now set to start
					
				Event = Events.FindFromNameValue(SendUser, Value)
				if Event == False: 
					Event = Events.FindFromName(SendUser)
					if Event == False:
						DebugLog(DL_WARN, "PM tip no events found for Nick: " + SendUser + ", expecially for Value: " + str(Value))
						Msg = "if you want to soak, please tip in the channel you want to soak in. I only accept non-channel tips that I am expecting now. See !commands for list of commands that may expect a tip"
						NewTipout = TipoutThread.AddTipback("(PrivMsg)", SendUser, Value, Msg, TipID)
						if NewTipout:
							NewTipout.FromPrivMsg = True
					else: # expecting a tip, but doesn't match
						if Event.Type == "donate":
							DebugLog(DL_WARN, "PM tip Event Type: " + Event.Type + ", From: " + SendUser + ", not correct value. Wanted: " + str(Event.Value) + ", Got: " + str(Value))
							Msg = "I am expecting a donation tip from you for exactly Ɖ" + str(Event.Value) + ". To be sure to only keep tips you want to donate, I must return this one for Ɖ" + str(Value)
						elif Event.Type == "anonsoak":
							DebugLog(DL_WARN, "PM tip Event Type: " + Event.Type + ", From: " + SendUser + ", not correct value. Wanted: " + str(Event.Value) + ", Got: " + str(Value))
							Msg = "I am expecting an anonymous soak tip from you for exactly Ɖ" + str(Event.Value) + ". To be sure to only soak tips you want to soak, I must return this one for Ɖ" + str(Value)
						else:
							DebugLog(DL_WARN, "PM tip Event Type: " + Event.Type + ", not supported. Expecting tip but not of value.")
							Msg = SendUser + ", I seem to have messed up knowning what you wanted to do with this tip. Please try again and notify an operator of this error"
						NewTipout = TipoutThread.AddTipback(None, SendUser, Value, Msg, TipID)
						if NewTipout:
							NewTipout.FromPrivMsg = True
				else: # Event found of correct Nick and Value
					if Event.Type == "donate":
						NotifyMessageThread.AddNotify("donate reply", SendUser, "I have received your donation of Ɖ" + str(Value) + ". Thank you for your support!")
						Event.SetDone()
					elif Event.Type == "anonsoak":
						if Event.ChannelName == False:
							UserMonitor = False
						else:
							UserMonitor = UserMonitors.FindChannel(Event.ChannelName)
						if UserMonitor == False:
							if Event.ChannelName == False:
								Msg = "I don't seeem to be tracking that channel anymore. Canceling anonymous soak"
							else:
								Msg = "I don't seeem to be tracking the " + Event.ChannelName + " channel anymore. Canceling anonymous soak"
							NewTipout = TipoutThread.AddTipback(None, SendUser, Value, Msg, TipID)
							if NewTipout:
								NewTipout.FromPrivMsg = True
						else: # channel found, all is go
							DebugLog(DL_DEBUG, "PM tip, event expected, Anonymous soak, request FromName: " + Event.FromName + ", ChannelName: " + Event.ChannelName + ", value: " + str(Value) + ", tip id: " + TipID)
							UserMonitor.Soak(Event.ChannelName, Event.FromName, Value, TipID, True) # True - From PM, so it shows as Anonymous
							if not TipoutThread.ConfirmedTipID(TipID):
								DebugLog(DL_WARN, "PM tip, event expected, Anonymous soak ConfirmedTipID didn't find new PM tipout to insta-confirm TipID: " + TipID)
							NotifyMessageThread.AddNotify("donate reply", Event.FromName, "Anonymous soaking " + Event.ChannelName + " with Ɖ" + str(Value))
							Event.SetDone()
					else:
						DebugLog(DL_WARN, "PM tip Event Type: " + Event.Type + ", not supported")
						Msg = SendUser + ", I seem to have messed up knowning what you wanted to do with this tip. Please try again and notify an operator of this error"
						NewTipout = TipoutThread.AddTipback(None, SendUser, Value, Msg, TipID)
						if NewTipout:
							NewTipout.FromPrivMsg = True
						Event.SetDone()
			elif self.Message.startswith(DogeSoaker.GetBotNick() + ": Your balance is"):
				#DogeSoaker: Your balance is Ɖ8774 (+Ɖ1000 unconfirmed)
				words = self.Message.split()
				BalanceWords = ConStringList(words[4:])
				Event = Events.FindEventType("dsbal")
				if Event == False or Event.FromName == False:
					DebugLog(DL_INFO, "PM balance received, but no request event found: " + BalanceWords)
				else:
					DebugLog(DL_DEBUG, "PM balance found dsbal reply event FromName: " + Event.FromName + ", BalanceWords: " + BalanceWords)
					NotifyMessageThread.AddNotify("dsbal event reply", Event.FromName, "Balance: " + BalanceWords)			
					Event.SetDone()	
			else:
				# pylint: disable=W0212
				# use private version, because TPrivMsg.Execute is called while PrivMsgHandlerThread is already locked
				# Disabled access of protected member warning, since we are manually locking/unlocking and accessing internal function directly
				if PrivMsgHandlerThread._ContinueSpannedMessage(self.FromName, self.ToName, self.Message):
					DebugLog(DL_DEBUG, "PM from Authorized tipbot ContinueSpannedMessage successful")
				else:
					# use private version, because TPrivMsg.Execute is called while PrivMsgHandlerThread is already locked
					DebugLog(DL_WARN, "PM from Authorized tipbot unhandled, from: " + self.FromName + ", to: " + self.ToName + ", message: " + self.Message)
					PrivMsgHandlerThread._ForgetSpannedMessage(self.FromName, self.ToName)
				# pylint: enable=W0212
				
	def HandleUserCommand(self, HandleAll):
		global C_TipBotNick, C_Soak_Channel_Default, NotifyMessageThread, DogeSoaker
		words = self.Message.split(" ")
		if BamCmdMatch(self.Message, "balance") or BamCmdMatch(self.Message, "withdraw") or BamCmdMatch(self.Message, "tip"):
			Msg = "Sorry I only automate use of the '" + C_TipBotNick + "' tipbot. For balance, withdraw, or other tipbot account features message the tipbot, " + C_TipBotNick + ". Example: /msg " + C_TipBotNick + " help"
			NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)				
		elif BamCmdMatch(self.Message, "dshelp") or  BamCmdMatch(self.Message, "help"):
			Msg = "Hi " + self.FromName + ", I keep track of active users in the " + C_Soak_Channel_Default + " channel. When someone sends me a tip, I spread it out evenly between the active users in a 'soak'. Doge Soaker is provided as a free service with absolutely no warranty or guarantee; Use at your own risk. Use !commands for command list. For disputes or issues please contact the bot operator: "
			if len(OperatorList.Nicks):
				Msg = Msg + OperatorList.Nicks[0]
			else:
				Msg = Msg + "<None!>"
			NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)		
		elif BamCmdMatch(self.Message, "version"):
			NotifyMessageThread.AddNotify("help reply", self.FromName, Long_Name)			
		elif BamCmdMatch(self.Message, "bark"):
			Msg = DogeSoaker.GetBarkMessage(False)
			NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)			
		elif BamCmdMatch(self.Message, "active"):
			if len(words) != 2:
				Msg = "Sorry, " + self.FromName + ", please specify the channel you are interested in. Example: !active " + C_Soak_Channel_Default
			else:
				ChannelName = words[1]
				Msg = self.FromName + ", " + DogeSoaker.GetActiveUserMessage(ChannelName) + " (" + ChannelName + ")"
			NotifyMessageThread.AddNotify("active reply", self.FromName, Msg)			
		elif BamCmdMatch(self.Message, "donate"):
			if len(words) != 2 or not isInt(words[1]):
				Msg = "Sorry, " + self.FromName + ", please specify the value you are going to tip as a donation. Example: !donate 500"
			else:
				Value = int(words[1])
				Events.AddExpectTipEvent("donate", self.FromName, "(No Channe)", Value)
				Msg = self.FromName + ", the next tip for exactly Ɖ" + str(Value) + " that you send by messaging " + C_TipBotNick + " directly, within the next 60 seconds, will be kept as a donation. Example: /msg " + C_TipBotNick + " !tip " + DogeSoaker.GetBotNick() + " " + str(Value)
			NotifyMessageThread.AddNotify("donate reply", self.FromName, Msg)			
		elif BamCmdMatch(self.Message, "anonsoak"):
			if len(words) != 3 or not isInt(words[2]):
				Msg = "Sorry, " + self.FromName + ", please specify the channel and value you are interested in anonymously soaking. Example: !anonsoak " + C_Soak_Channel_Default + " 500"
			else:
				ChannelName = words[1]
				Value = int(words[2])
				UserMonitor = UserMonitors.FindChannel(ChannelName)
				if UserMonitor == False:
					Msg = "Sorry, " + self.FromName + ", I don't seem to be tracking the " + ChannelName + " channel."
				else:
					Events.AddExpectTipEvent("anonsoak", self.FromName, ChannelName, Value)
					Msg = self.FromName + ", the next tip for exactly Ɖ" + str(Value) + " that you send by messaging " + C_TipBotNick + " directly, within the next 60 seconds, will be anonymously soaked to " + ChannelName + ". Example: /msg " + C_TipBotNick + " !tip " + DogeSoaker.GetBotNick() + " " + str(Value)
			NotifyMessageThread.AddNotify("active reply", self.FromName, Msg)			
		elif BamCmdMatch(self.Message, "find"):
			MsgWords = self.Message.split(" ")
			Msg = ""
			if len(MsgWords) != 2:
				Msg = "Sorry, " + self.FromName + ", soak activity is specific to each channel. Please specify the channel you are interested in. Example: !find " + C_Soak_Channel_Default
			else:
				ChannelName = MsgWords[1]
				Msg = self.FromName + ", " + DogeSoaker.GetActiveUserMessage(ChannelName)
				UserMonitor = UserMonitors.FindChannel(ChannelName)
				if UserMonitor == False:
					Msg = "Sorry, " + self.FromName + ", Channel " + ChannelName + " not found. I don't know about channels I'm not it. Example: !find " + C_Soak_Channel_Default
				elif UserMonitor.FindUser(self.FromName) != False:
					DebugLog(DL_INFO, "User PM find request from: "+ self.FromName + ", channel: " + ChannelName + ", result: Yes")
					Msg = "Yes, " + self.FromName + ", you are in the active users list for " + ChannelName + "."						
				else:
					DebugLog(DL_INFO, "User PM find request: "+ self.FromName + ", channel: " + ChannelName + ", result: No")
					Msg = "Sorry, " + self.FromName + ", you are not in the active users list for " + ChannelName + "."
			NotifyMessageThread.AddNotify("find reply", self.FromName, Msg)		
		elif BamCmdMatch(self.Message, "commands"):
			NotifyMessageThread.AddNotify("help reply", self.FromName, "!commands - This message, !help - Long help message, !version - Long bot name and version, !bark - Soaker status, !active - Active shibe count for soak, !find - Test if you are considered active by the soaker, !donate - Tell soaker to expect a donation tip, !anonsoak - Tell soaker to expect a tip to anonymously soak a channel")
		elif HandleAll:
			Msg = "Hi " + self.FromName + ", I am an automated chat bot. Please message me !help for more information."
			NotifyMessageThread.AddNotify("help reply", self.FromName, Msg)
		else:
			return False
		return True
							
				
#######################################################
###                 TPrivMsgHandlerThread           ###
#######################################################

class TPrivMsgHandlerThread(TThread):

	def __init__(self):
		TThread.__init__(self, self.MainLoop)
		self.Active = False
		self.PrvMsgs = []
		self.Delay = 1.0 # seconds (overridden by constant)
		self.SpannedMessage = False
		self.SpannedMessageFrom = False
		self.SpannedMessageTo = False
		self.SpannedMessageTime = 0.0
		
	def Start(self):
		TThread.Start(self)
		
	def Stop(self):
		self.Terminate = True
	
	def Cancel(self): 
		self.Terminate = True        
	
	def AddPrvMsg(self, FromName, ToName, Message):
		if not self.TryLock():
			return False
		pm = self._AddPrvMsg(FromName, ToName, Message)
		self.Unlock()
		return pm
		
	def _AddPrvMsg(self, FromName, ToName, Message):
		#DebugLog(DL_DEBUG, "TPrivMsgHandlerThread::AddPrvMsg Message: " + Message + ", prev count: " + str(len(self.PrvMsgs)))
		pm = TPrivMsg(FromName, ToName, Message)
		self.PrvMsgs.append(pm)
		return pm
		
	def SetSpannedMessage(self, FromName, ToName, Message):
		if not self.TryLock():
			return False
		self._SetSpannedMessage(FromName, ToName, Message)
		self.Unlock()

	def _SetSpannedMessage(self, FromName, ToName, Message):
		if self.SpannedMessage != False and self.SpannedMessageFrom != False and self.SpannedMessageTo != False:
			DebugLog(DL_WARN, "TPrivMsgHandlerThread::SetSpannedMessage already have spanned message, reaplacing previous with current, From: " + FromName + ", To: " + ToName + ", message: " + Message)
			DebugLog(DL_WARN, "    Previous span age: " + str(time.time() - self.SpannedMessageTime) + ", From: " + self.SpannedMessageFrom + ", To: " + self.SpannedMessageTo + ", message: " + self.SpannedMessage)
		self.SpannedMessage = Message
		self.SpannedMessageFrom = FromName
		self.SpannedMessageTo = ToName
		self.SpannedMessageTime = time.time()
		DebugLog(DL_DEBUG, "TPrivMsgHandlerThread::SetSpannedMessage, From: " + FromName + ", To: " + ToName + ", message: " + Message)

	def ContinueSpannedMessage(self, FromName, ToName, Message):
		if not self.TryLock():
			return False
		r = self._ContinueSpannedMessage(FromName, ToName, Message)
		self.Unlock()
		return r
		
	def _ContinueSpannedMessage(self, FromName, ToName, Message):
		DebugLog(DL_DEBUG, "TPrivMsgHandlerThread::ContinueSpannedMessage current, From: " + FromName + ", To: " + ToName + ", message: " + Message)
		if self.SpannedMessage != False and self.SpannedMessageFrom != False and self.SpannedMessageTo != False:
			DebugLog(DL_DEBUG, "    Previous span age: " + str(time.time() - self.SpannedMessageTime) + ", From: " + self.SpannedMessageFrom + ", To: " + self.SpannedMessageTo + ", message: " + self.SpannedMessage)
		else:
			DebugLog(DL_WARN, "TPrivMsgHandlerThread::ContinueSpannedMessage no previous spanned message")
			return False
		if FromName != self.SpannedMessageFrom or ToName != self.SpannedMessageTo:
			DebugLog(DL_WARN, "TPrivMsgHandlerThread::ContinueSpannedMessage previous spanned message From/To doesn't match.")
			return False
		if (time.time() - self.SpannedMessageTime) > 10.0: # seconds
			DebugLog(DL_WARN, "TPrivMsgHandlerThread::ContinueSpannedMessage previous spanned message has timed out, ignoring both.")
			return False
		Message = self.SpannedMessage + Message
		DebugLog(DL_DEBUG, "TPrivMsgHandlerThread::ContinueSpannedMessage re-adding re-assembled spanned message: " + Message)
		self._AddPrvMsg(FromName, ToName, Message)
		return True
	
	def ForgetSpannedMessage(self, FromName, ToName):
		if not self.TryLock():
			return False
		self._ForgetSpannedMessage(FromName, ToName)
		self.Unlock()

	def _ForgetSpannedMessage(self, FromName, ToName):
		if self.SpannedMessage == False and self.SpannedMessageFrom == False and self.SpannedMessageTo == False:
			#DebugLog(DL_DEBUG, "TPrivMsgHandlerThread::ForgetSpannedMessage no spanned message, From: " + FromName + ", To: " + ToName)
			return
		elif FromName == self.SpannedMessageFrom and ToName == self.SpannedMessageTo:
			DebugLog(DL_DEBUG, "TPrivMsgHandlerThread::ForgetSpannedMessage forgetting previous spanned message, age: " + str(time.time() - self.SpannedMessageTime) + ", From: " + self.SpannedMessageFrom + ", To: " + self.SpannedMessageTo + ", message: " + self.SpannedMessage)
			self.SpannedMessage = False
			self.SpannedMessageFrom = False
			self.SpannedMessageTo = False
			self.SpannedMessageTime = 0.0
		else:
			DebugLog(DL_DEBUG, "TPrivMsgHandlerThread::ForgetSpannedMessage current spanned message doesn't match, From: " + FromName + ", To: " + ToName)
			DebugLog(DL_DEBUG, "    current spanned message, age: " + str(time.time() - self.SpannedMessageTime) + ", From: " + self.SpannedMessageFrom + ", To: " + self.SpannedMessageTo + ", message: " + self.SpannedMessage)
		
	def MainLoop(self):
		time.sleep(0.001)
		self.Active = True
		if self.Terminate: 
			return # test once, before try block to prevent Unlock
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG Handle PrivMsg thread started")
		while not self.Terminate:
			if not self.TryLock():
				return False
			try: # try/finally trap for Terminating/Unlock'ing/DeReference'ing
				if len(self.PrvMsgs):
					self.PrvMsgs[0].Execute()
					del self.PrvMsgs[0]
			except:
				DebugLog(DL_ERROR, "TPrivMsgHandlerThread exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
				if len(self.PrvMsgs):
					del self.PrvMsgs[0] # delete exception causing message so we don't get stuck
			finally:
				self.Active = False
				self.TouchHeartbeatLocked()
				self.Unlock()
			if self.Terminate: 
				return
			self.SafeSleep(self.Delay)
			if self.CrashTest == True:
				raise Exception("TPrivMsgHandlerThread Crash Test Activated!")
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG Handle PrivMsg thread done")
			
#######################################################
###                 TTipout                         ###
#######################################################

class TTipout(TLockable):
	SOAKSTEP_START, SOAKSTEP_WAIT_CONFIRM, SOAKSTEP_WHOIS_REQUEST, SOAKSTEP_WHOIS_WAIT, SOAKSTEP_VERIFY_USERS, SOAKSTEP_CHAN_MSG, SOAKSTEP_PM_TIPS, SOAKSTEP_WAIT_TIP_CONF, SOAKSTEP_DONE, SOAKSTEP_FAIL_CONF, SOAKSTEP_RESEND, SOAKSTEP_RESEND_WAIT, SOAKSTEP_RESEND_SEND, SOAKSTEP_REFUSED, SOAKSTEP_REFUSED_WAIT = range(0, 15)
	TIPBACK_STEP_TIPBACK, TIPBACK_STEP_CHAN_MSG, TIPBACK_STEP_PM_TIP, TIPBACK_STEP_WAIT_CONF, TIPBACK_STEP_DONE, TIPBACK_STEP_DONE_WAIT, TIPBACK_STEP_RESEND, TIPBACK_STEP_RESEND_WAIT, TIPBACK_STEP_RESEND_SEND = range(0, 9)

	def __init__(self, Type, ChannelName, FromName, UserList, Value, Message, TipID):
		global TipoutThread
		TLockable.__init__(self)
		self.Type = Type
		self.FromName = FromName
		self.ChannelName = ChannelName
		self.TipID = TipID
		self.Message = Message
		self.UserList = UserList
		self.Value = Value
		self.UserValue = 0
		self.ConfirmedUserList = []
		self.VerifiedUserList = []
		self.InvalidUserList = []
		self.MissedUserList = []
		self.FailUserList = []
		self.AcceptingUserVerification = False
		self.TipFailCount = 0
		self.TipConfirmTimeout = 0.0
		self.TipConfirmTimeoutSpeed = TipoutThread.TipConfirmTimeoutSpeed # seconds, starts after last tip PM is sent, reset after each recieved tip confirm
		self.TipConfirmTimeoutSpeedLong = TipoutThread.TipConfirmTimeoutSpeedLong # seconds, starts after last tip PM is sent, but tips were missed and not reported f
		self.TipConfirmed = False
		self.Done = False
		self.FromPrivMsg = False
		self.Step = 0
		self.LastStep = 0
		self.LastDebugTime = 0.0
		self.ResendCount = 0
		self.MaxResend = 2

	def Resend(self):
		if self.Type == "soak":
			if self.TipConfirmTimeout != 0.0:
				self.TipConfirmTimeout = time.time() # set to ready now

	def TipFailDetected(self, User, Value):
		self.TryLock()
		self._TipFailDetected(User, Value)
		self.Unlock()
		
	def _TipFailDetected(self, User, Value):
		if self.Type == "dsoak" or self.Type == "dtipback":
			if User != False:
				if Value != False and Value != self.UserValue:
					DebugLog(DL_DEBUG, "Tipout::TipFailDetected (From: " + self.FromName + ", UserValue: " + str(self.UserValue) + "): Value: " + str(Value) + " doesn't match for failed tip to user: " + User)
					return
				for VUserName in self.VerifiedUserList:
					if VUserName == User:
						self.FailUserList.append(User)
						self.TipFailCount = self.TipFailCount + 1
						DebugLog(DL_DEBUG, "Tipout::TipFailDetected (From: " + self.FromName + ", UserValue: " + str(self.UserValue) + "): Counted failed tip of Value: " + str(Value) + " for user: " + User + " failed count: " + str(self.TipFailCount))
						if self.TipConfirmTimeout != 0.0:
							self.TipConfirmTimeout = time.time() + self.TipConfirmTimeoutSpeed # extend tip confirmation timeout wait
						return
			else: # no verification info user supplied
				if Value != False and Value != self.UserValue:
					DebugLog(DL_DEBUG, "Tipout::TipFailDetected (no User) (From: " + self.FromName + ", UserValue: " + str(self.UserValue) + "): Value: " + str(Value) + " doesn't match for failed tip to user: " + User)
					return
				self.TipFailCount = self.TipFailCount + 1
				DebugLog(DL_DEBUG, "Tipout::TipFailDetected (From: " + self.FromName + ", UserValue: " + str(self.UserValue) + "): Counted failed tip (no value or user data) fail count: " + str(self.TipFailCount))
				if self.TipConfirmTimeout != 0.0:
					self.TipConfirmTimeout = time.time() + self.TipConfirmTimeoutSpeed # extend tip confirmation timeout wait

	def TipFailDetectedMulti(self, NickList, ConfValue):
		self.TryLock()
		self._TipFailDetectedMulti(NickList, ConfValue)
		self.Unlock()
		
	def _TipFailDetectedMulti(self, NickList, ConfValue):
		if self.Type == "dsoak" or self.Type == "dtipback":
			if ConfValue == False or ConfValue == self.UserValue:
				if not ConfValue:
					ConfValue = 0
				for Nick in NickList:
					if ArrayContains(self.VerifiedUserList, Nick):
						self.FailUserList.append(Nick)
						self.TipFailCount += 1
						DebugLog(DL_DEBUG, "Tipout::_TipFailDetectedMulti (From: " + self.FromName + ", UserValue: " + str(self.UserValue) + "): Counted failed tip of Value: " + str(ConfValue) + " for user: " + Nick + " failed count: " + str(self.TipFailCount))
						if self.TipConfirmTimeout != 0.0:
							self.TipConfirmTimeout = time.time() + self.TipConfirmTimeoutSpeed # extend tip confirmation timeout wait

	def ConfirmedTipID(self, TipID):
		self.TryLock()
		r = self._ConfirmedTipID(TipID)
		self.Unlock()
		return r
		
	def _ConfirmedTipID(self, TipID):
		DebugLog(DL_DEBUG, "Tipout::ConfirmedTipID checking for TipID : " + TipID + " tipout TipID: " + self.TipID + " Type: " + self.Type + " Step: " + str(self.Step))
		if self.TipID == TipID:
			if self.Type == "dsoak" or self.Type == "dtipback":
				DebugLog(DL_DEBUG, "Tipout::ConfirmedTipID tipout set to confirmed, TipID: " + TipID)
				self.TipConfirmed = True
				return True
			else:
				DebugLog(DL_WARN, "Tipout::ConfirmedTipID not Doger tipout type, Type: " + self.Type + " TipID: " + TipID)
		return False

	def UpdateWhoIsAuth(self, Nick):
		self.TryLock()
		self._UpdateWhoIsAuth(Nick)
		self.Unlock()
		
	def _UpdateWhoIsAuth(self, Nick):
		global DEBUG_AuthUpdate
		if self.AcceptingUserVerification:
			for User in self.UserList:
				if User.Name == Nick: # Tipout includes this user
					for VUserName in self.VerifiedUserList:
						if VUserName == Nick: 
							DebugLog(DL_DEBUG, "Tipout::UpdateWhoIsAuth (from " + self.FromName + "): " + Nick + " already validated")
							return # already validated, nothing to do
					# Add to validated list
					if DEBUG_AuthUpdate == True:
						DebugLog(DL_DEBUG, "Tipout::UpdateWhoIsAuth tipout: (type: " + self.Type + ", from: " + self.FromName + ", uvalue/value: " + str(self.Value) + "/" + str(self.UserValue) + ") adding validation for nick: " + Nick)
					self.VerifiedUserList.append(User.Name) # User.Name should be Nick
					self.TipConfirmTimeout += 5.0
					return # done						

	def UpdateWhoIsEnd(self, Nick):
		self.TryLock()
		self._UpdateWhoIsEnd(Nick)
		self.Unlock()

	def _UpdateWhoIsEnd(self, Nick):
		#DebugLog(DL_DEBUG, "Tipout::UpdateWhoIsEnd (from " + self.FromName + "): " + Nick)
		if self.AcceptingUserVerification:
			for User in self.UserList:
				if User.Name == Nick:
					# Tipout includes this user
					#DebugLog(DL_DEBUG, "Tipout::UpdateWhoIsEnd (from " + self.FromName + "): " + Nick + " is tracked, checking " + str(len(self.VerifiedUserList)) + " verified...")
					for VUserName in self.VerifiedUserList:
						if VUserName == Nick: 
							#DebugLog(DL_DEBUG, "Tipout::UpdateWhoIsEnd (from " + self.FromName + "): " + Nick + " already validated")
							return # already authenticated, nothing to do
					# Not authenticated, add to invalid list
					DebugLog(DL_DEBUG, "Tipout::UpdateWhoIsEnd (from " + self.FromName + "): " + Nick + " adding INvalidation")
					self.InvalidUserList.append(User.Name) # User.Name should be Nick
					self.TipConfirmTimeout += 5.0
					return # done						

	def UpdateTipConfirm(self, Nick, Value):
		self.TryLock()
		self._UpdateTipConfirm(Nick, Value)
		self.Unlock()

	def _UpdateTipConfirm(self, Nick, Value):
		global DEBUG_Each_Confirm
		if self.Type == "soak" or self.Type == "dsoak":
			if Value != self.UserValue:
				DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirm confirm -" + self.Type + "- nick: " + Nick + " value: " + str(Value) + " doesn't match tipout's UserValue: " + str(self.UserValue))
				return
		elif self.Type == "tipback" or self.Type == "dtipback":
			if Value != self.Value:
				DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirm confirm -" + self.Type + "- nick: " + Nick + " value: " + str(Value) + " doesn't match tipout's Value: " + str(self.Value))
				return
		#DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirm checking for tracking of: " + Nick)
		for VUserName in self.VerifiedUserList:
			if VUserName == Nick: 
				#DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirm we are tracking " + Nick)
				for CUserName in self.ConfirmedUserList:
					if CUserName == Nick:
						DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirm (from " + self.FromName + "): " + Nick + "'s tip already confirmed")
						return # already confirmed, nothing to do
				if self.TipConfirmTimeout != 0.0:
					if DEBUG_Each_Confirm == True:
						DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirm (from " + self.FromName + "): " + Nick + "'s now confirmed, timeout extended")
					self.TipConfirmTimeout = time.time() + self.TipConfirmTimeoutSpeed # extend tip confirmation timeout wait
				self.ConfirmedUserList.append(Nick)
				return # done
		DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirm tipout: (type: " + self.Type + ", from: " + self.FromName + ", uvalue/value: " + str(self.Value) + "/" + str(self.UserValue) + ") confirm not tracked, nick: " + Nick + ", value: " + str(Value))
			
	def UpdateTipConfirmMulti(self, NickList, Value):
		self.TryLock()
		self._UpdateTipConfirmMulti(NickList, Value)
		self.Unlock()

	def _UpdateTipConfirmMulti(self, NickList, Value):
		global DEBUG_MultiConfirm
		if self.Type == "dsoak":
			if Value != self.UserValue:
				DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirmMulti confirm dsoak value: " + str(Value) + " doesn't match tipout's UserValue: " + str(self.UserValue) + " NickList: " + ",".join(NickList))
				return
		elif self.Type == "dtipback":
			if Value != self.Value:
				DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirm confirm -" + self.Type + "- value: " + str(Value) + " doesn't match tipout's Value: " + str(self.Value) + " NickList: " + ",".join(NickList))
				return
		else:
			DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirmMulti Tipout of this type: " + self.Type + " is not supposed to get Multi confirm. value: " + str(Value) + " NickList: " + ",".join(NickList))
			return
		if DEBUG_MultiConfirm == True:
			DebugLog(DL_DEBUG, "Tipout::UpdateTipConfirmMulti UpdateTipConfirm (tipout type: " + self.Type + ", FromName: " + self.FromName + ", Value: " + str(self.Value) + ", UserValue: " + str(self.UserValue) + ") Confirmed for value: " + str(Value) + ", for nicks (" + str(len(NickList)) + "): " + ",".join(NickList))
		for Nick in NickList:
			self._UpdateTipConfirm(Nick, Value)
			
	def IsCriticalBusy(self):
		self.TryLock()
		ret = self._IsCriticalBusy()
		self.Unlock()
		return ret
	
	def _IsCriticalBusy(self):
		if self.Type == "dsoak":
			if self.Step >= TTipout.SOAKSTEP_WAIT_TIP_CONF:
				return False
		elif self.Type == "dtipback":
			if self.Step >= TTipout.TIPBACK_STEP_WAIT_CONF:
				return False
		else:
			DebugLog(DL_ERROR, "Tipout::IsCriticalBusy unhandled tipout type '" + self.Type + "'")
		return True
	
	# pylint: disable=W0212
	# Disabled access of protected member warning, since Tipout is executed by TipoutThread which is already locked and we will be accessing TipoutThread private members

	def Execute(self):
		self.TryLock()
		self._Execute()
		self.Unlock()
	
	def _Execute(self):
		if self.Type == "soak":
			DebugLog(DL_ERROR, "Tipout::Execute 'soak' tipout disabled (legacy fido support), dumping debug")
			DogeSoaker.DebugDump()
			#self.StepSoak() # legacy fido support
		elif self.Type == "tipback":
			DebugLog(DL_ERROR, "Tipout::Execute 'tipback' tipout disabled (legacy fido support), dumping debug")
			DogeSoaker.DebugDump()
			#self.StepTipback() # legacy fido support
		elif self.Type == "bsoak":
			DebugLog(DL_ERROR, "Tipout::Execute 'bsoak' tipout disabled, dumping debug")
			DogeSoaker.DebugDump()
			#self.StepBlasterSoak()
		elif self.Type == "btipback":
			DebugLog(DL_ERROR, "Tipout::Execute 'btipback' tipout disabled, dumping debug")
			DogeSoaker.DebugDump()
			#self.StepBlasterTipback()
		elif self.Type == "dsoak":
			self.StepDogerSoak()
		elif self.Type == "dtipback":
			self.StepDogerTipback()
		else:
			DebugLog(DL_ERROR, "Tipout Step invalid type '" + self.Type + "'")			
			
	def StepDogerSoak(self):
		global IRC
		global IgnoreList, IgnoreHostList
		global NotifyTipThread
		global C_Minimum_Tip_Value, C_Max_PrivMsg_Length
		global AuditLog
		global TipoutThread # already locked when executing tipout, which called this function
		global C_Tip_Confirmation_Resending
		global TextHandler
		global C_Enable_Twitter_Status, C_Twitter_Command, C_IRC_Server_Name, C_Twitter_Max_Length, C_Enable_Diaspora_Status, C_Diaspora_Command
		if self.Step != self.LastStep or self.Step == 0:
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak Step " + str(self.Step) + "...")
			self.LastStep = self.Step
		if self.Step == TTipout.SOAKSTEP_START: # starting, need timout for confirm
			self.AcceptingUserVerification = False
			self.TipConfirmTimeout = time.time() + 10.0
			self.Step = TTipout.SOAKSTEP_WAIT_CONFIRM
		elif self.Step == TTipout.SOAKSTEP_WAIT_CONFIRM: # waiting for PM of tip confirm
			if self.TipConfirmed:
				self.Step = TTipout.SOAKSTEP_WHOIS_REQUEST
			elif time.time() > self.TipConfirmTimeout:
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak waiting for PM confirm timed out for TipID: " + self.TipID + ", Value: " + str(self.Value) + ", FromName: " + self.FromName)
				self.Message = TextHandler.GetTextChan("SOAK_REFUSE_NOTIPCONFIRM", self.ChannelName)
				self.TipConfirmed = True # falsely set to confirmed so tipout can die after tipping back
				self.Step = TTipout.SOAKSTEP_REFUSED
				return
		elif self.Step == TTipout.SOAKSTEP_WHOIS_REQUEST: # start process with whois request	
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak determine users...")
			self.ResendCount = 0
			self.TipConfirmTimeout = 0.0
			if self.Value < C_Minimum_Tip_Value:
					DebugLog(DL_WARN, "Tipout StepDogerSoak Value " + str(self.Value) + " smaller than min tip " + str(C_Minimum_Tip_Value) + " skipping whois tests")
					self.Message = TextHandler.GetTextChan("SOAK_REFUSE_MINTIP", self.ChannelName)
					self.Step = TTipout.SOAKSTEP_REFUSED
					return

			self.AcceptingUserVerification = True

			AuthCachedValid = []
			AuthCachedINValid = []
			AuthIgnored = []
			AuthWhois = []
			
			for User in self.UserList:
				if User.Name == "":
					DebugLog(DL_WARN, "Tipout StepDogerSoak ignoring empty User Name in UserList")
					self.InvalidUserList.append(User.Name)
				elif  self.FromName == User.Name:
					DebugLog(DL_DEBUG, "Tipout StepDogerSoak ignoring whois request for FromName " + User.Name + " (you're not soaking yourself)")
					self.InvalidUserList.append(User.Name)
				elif IgnoreList.ContainsNick(User.Name):
					AuthIgnored.append(User.Name)
					self.InvalidUserList.append(User.Name)
				else:
					AuthUser = TipoutThread.AuthUsers.FindUserByNick(User.Name)
					if AuthUser != False:
						if AuthUser.AuthNick != "":
							AuthCachedValid.append(User.Name)
							self.VerifiedUserList.append(User.Name) # already conrimed via cache
						else:
							AuthCachedINValid.append(User.Name)
							AuthWhois.append(User.Name)
					else:
						AuthWhois.append(User.Name)

			for Nick in AuthWhois:
				NotifyCommandThread.AddCommand("WHOIS " + Nick)
					
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak stats, user list: " + str(len(self.UserList)) + ", cached valid: " + str(len(AuthCachedValid)) + ", cached INvalid: " + str(len(AuthCachedINValid)) + ", ignored: " + str(len(AuthIgnored)) + ", whois(including INvalid): " + str(len(AuthWhois)))
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak verbose stats, cached valid   (" + str(len(AuthCachedValid)) + "): " + ",".join(AuthCachedValid))
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak verbose stats, cached INvalid (" + str(len(AuthCachedINValid)) + "): " + ",".join(AuthCachedINValid))
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak verbose stats, ignored        (" + str(len(AuthIgnored)) + "): " + ",".join(AuthIgnored))
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak verbose stats, whois queue    (" + str(len(AuthWhois)) + "): " + ",".join(AuthWhois))

			if len(AuthWhois) > 3: # for larger list, notify that we are started
				self.Message = TextHandler.GetTextChan("SOAK_AUTHCHECK", self.ChannelName)
				IRC.ChanMsg(self.ChannelName, self.Message)
			if len(AuthWhois) == 0:
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak no whois wait needed")
				self.Step = TTipout.SOAKSTEP_VERIFY_USERS
			else:
				self.TipConfirmTimeout = time.time() + (C_Notify_CMD_Delay * len(AuthWhois)) + 10.0
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak " + str(AuthWhois) + " Whois requests est at " + str(C_Notify_CMD_Delay) + " sec each starting, will time out in " + str((self.TipConfirmTimeout - time.time())))
				self.Step = TTipout.SOAKSTEP_WHOIS_WAIT
		elif self.Step == TTipout.SOAKSTEP_WHOIS_WAIT: # wait replies
			if len(self.UserList) == (len(self.VerifiedUserList) + len(self.InvalidUserList)):
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak whois complete, userList: " + str(len(self.UserList)) + " validList: " + str(len(self.VerifiedUserList)) + " INvalidList: " + str(len(self.InvalidUserList)) + " validate time left: " + str((self.TipConfirmTimeout - time.time())))
				self.Step = TTipout.SOAKSTEP_VERIFY_USERS
			elif time.time() > self.TipConfirmTimeout: 
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak whois wait timed out, userList: " + str(len(self.UserList)) + " validList: " + str(len(self.VerifiedUserList)) + " INvalidList: " + str(len(self.InvalidUserList)) + " validate time left: " + str((self.TipConfirmTimeout - time.time())))
				UserNames = []
				for User in self.UserList:
					UserNames.append(User.Name)
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak verbose stats, user list: (" + str(len(self.UserList)) + "): " + ",".join(UserNames))
				self.Step = TTipout.SOAKSTEP_VERIFY_USERS
		elif self.Step == TTipout.SOAKSTEP_VERIFY_USERS:
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak Verifying users...")
			AcceptedUsers = []
			AuthNicks = []
			Hosts = []

			if not TipoutThread.AuthUsers.TryLock(): 
				DebugLog(DL_WARN, "Tipout StepDogerSoak lock TipoutThread.AuthUsers failed")
				return
				
			self.AcceptingUserVerification = False
		
			for VerNick in self.VerifiedUserList:
				User = TipoutThread.AuthUsers._FindUserByNick(VerNick) #already locked, use bare version
				if User == False:
					DebugLog(DL_WARN, "Tipout StepDogerSoak Verified Nick: " + VerNick + " not found in AuthUsers")
				else:
					if User.Host != "" and ArrayContains(Hosts, User.Host):
						DebugLog(DL_INFO, "Tipout StepDogerSoak Nick: " + VerNick + " rejected for matching Host: " + User.Host)
					else:
						Hosts.append(User.Host)
						if User.AuthNick != "" and ArrayContains(AuthNicks, User.AuthNick):
							DebugLog(DL_INFO, "Tipout StepDogerSoak Nick: " + VerNick + " rejected for matching AuthNick: " + User.AuthNick)
						else:
							FullHost = "default"
							if User.HostUser != "" or User.Host != "":
								FullHost = User.HostUser + "@" + User.Host
								for HostMask in IgnoreHostList.Nicks:
									if fnmatch.fnmatch(FullHost, HostMask):
										DebugLog(DL_INFO, "Tipout StepDogerSoak Nick: " + VerNick + " rejected for matching Ignored Host Mask: " + HostMask + " FullHost: " + FullHost)
										FullHost = False
										break
							if FullHost != False:
								#DebugLog(DL_DEBUG, "Tipout StepDogerSoak Accepted Nick: " + VerNick + " Host: " + User.Host + " AuthNick: " + User.AuthNick)
								AuthNicks.append(User.AuthNick)
								AcceptedUsers.append(VerNick)

			TipoutThread.AuthUsers.Unlock()

			DebugLog(DL_DEBUG, "Tipout StepDogerSoak verification accepted " + str(len(AcceptedUsers)) + " of " + str(len(self.VerifiedUserList)) + " users.")
			self.VerifiedUserList = AcceptedUsers # reset to pruned list
			self.Step = TTipout.SOAKSTEP_CHAN_MSG
		elif self.Step == TTipout.SOAKSTEP_CHAN_MSG: # send channel message
			if len(self.VerifiedUserList) == 0:
				DebugLog(DL_WARN, "Tipout StepDogerSoak no valid users")
				self.Message = TextHandler.GetTextChan("SOAK_REFUSE_NOUSERS", self.ChannelName)
				self.Step = TTipout.SOAKSTEP_REFUSED
				return
			else:
				TipoutThread._TouchSoak()
				self.UserValue = int(math.floor(float(self.Value) / float(len(self.VerifiedUserList))))
				if self.UserValue < C_Minimum_Tip_Value:
					DebugLog(DL_WARN, "Tipout StepDogerSoak UserValue " + str(self.UserValue) + " smaller than min tip " + str(C_Minimum_Tip_Value))
					self.Message = TextHandler.GetTextChan("SOAK_REFUSE_NOTENOUGH", self.ChannelName).format(len(self.VerifiedUserList) * C_Minimum_Tip_Value)
					self.Step = TTipout.SOAKSTEP_REFUSED
					return
				else:
					FullMsg = ""
					if self.FromPrivMsg:
						FromUserMsg = TextHandler.GetTextChan("SOAK_ANONYMOUS_NAME", self.ChannelName)
					else:
						FromUserMsg = self.FromName
					
					if len(self.VerifiedUserList) > 1:
						textID = "SOAK_CHANMSG_HEADER_MULTI"
					else:
						textID = "SOAK_CHANMSG_HEADER_SINGLE"
					MsgHeader = TextHandler.GetTextChan(textID, self.ChannelName).format(FromUserMsg, len(self.VerifiedUserList), self.UserValue)
					Msg = MsgHeader + ": "
					if len(self.VerifiedUserList) < 10:
						Seperator = ", " # if it's a small list, space it out for readability
					else:
						Seperator = ","

					UserlistStyles = TextHandler.GetTextChan("SOAK_CHANMSG_USERLIST_STYLELIST", self.ChannelName)
					if UserlistStyles != "":
						UserlistStylelist = UserlistStyles.split(" ")
						if len(UserlistStylelist) == 0:
							UserlistStylelist = None
					else:
						UserlistStylelist = None
					uidx = 0
					First = True
					for UserName in self.VerifiedUserList:
						if (len(Msg) + len(UserName) + 2) > C_Max_PrivMsg_Length:
							Msg += "..."
							DebugLog(DL_DEBUG, "Tipout StepDogerSoak sending channel message: " + Msg)
							IRC.ChanMsg(self.ChannelName, Msg)
							FullMsg += Msg + " "
							Msg = "..."
							First = True
						if First:
							First = False
						else:
							Msg += Seperator
						if UserlistStylelist:
							Msg += UserlistStylelist[uidx]
							uidx += 1
							if uidx >= len(UserlistStylelist):
								uidx = 0
						Msg += UserName
					if Msg != "":
						DebugLog(DL_DEBUG, "Tipout StepDogerSoak sending channel message: " + Msg)
						FullMsg += Msg
						IRC.ChanMsg(self.ChannelName, Msg)
					if C_Enable_Twitter_Status == True and MsgHeader != "":
						Msg = "(" + self.ChannelName + ") " + MsgHeader + " " + self.TipID
						if len(Msg) > C_Twitter_Max_Length:
							Msg = Msg[:C_Twitter_Max_Length-4]
							Msg += "..."
						Msg = C_Twitter_Command + " \"" + Msg + "\""
						retValue = os.system(Msg)
						if retValue != 0:
							DebugLog(DL_DEBUG, "Tipout StepDogerSoak twitter post failed retValue: " + str(retValue) + ", command: " + Msg)
					if C_Enable_Diaspora_Status == True and FullMsg != "":
						Msg = "(" + self.ChannelName + ") " + FullMsg + " " + self.TipID
						Msg = C_Diaspora_Command + " \"" + Msg + "\""
						retValue = os.system(Msg)
						if retValue != 0:
							DebugLog(DL_DEBUG, "Tipout StepDogerSoak Diaspora post failed retValue: " + str(retValue) + ", command: " + Msg)
			self.Step = TTipout.SOAKSTEP_PM_TIPS
		elif self.Step == TTipout.SOAKSTEP_PM_TIPS: # send tip pm's
			if len(self.VerifiedUserList) == 0:
				DebugLog(DL_ERROR, "Tipout StepDogerSoak step " + str(self.Step) + " Error, no verified users.")
			else:
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak sending " + str(len(self.VerifiedUserList)) + " tip PMs of value: " + str(self.UserValue))
				Msg = ""
				UserMsg = ""
				MsgUserCount = 0
				for UserName in self.VerifiedUserList:
					UserMsg = UserName + " " + str(self.UserValue)
					if (len(Msg) + len(UserMsg)) > C_Max_PrivMsg_Length:
						NotifyTipThread.AddNotify("Soak", C_TipBotNick, "!mtip " + Msg)
						DebugLog(DL_INFO, "Tip message: " + Msg)
						Msg = ""
						MsgUserCount = 0
					if MsgUserCount != 0:
						Msg = Msg + " "
					Msg += UserMsg
					MsgUserCount += 1
				if MsgUserCount > 0:
					NotifyTipThread.AddNotify("Soak", C_TipBotNick, "!mtip " + Msg)
					DebugLog(DL_INFO, "Tip message: " + Msg)
			self.TipConfirmTimeout = 0.0
			self.Step = TTipout.SOAKSTEP_WAIT_TIP_CONF
		elif self.Step == TTipout.SOAKSTEP_WAIT_TIP_CONF: # Await tip confirmations
			if (len(self.ConfirmedUserList) + len(self.FailUserList)) >= len(self.VerifiedUserList):
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak all confirmations received")
				AuditLog.LogSoak(self.FromName, self.ChannelName, self.Value, self.UserValue, self.ConfirmedUserList, self.FailUserList, self.Message, self.FromPrivMsg)				
				self.Step = TTipout.SOAKSTEP_DONE
			elif self.TipConfirmTimeout == 0.0:
				if not NotifyTipThread.IsBusy(): # Don't start timer until all tips are out
					DebugLog(DL_DEBUG, "Tipout StepDogerSoak not all confirmed, Tiptout not busy, setting timer, timeout speed: " + str(self.TipConfirmTimeoutSpeed))
					self.TipConfirmTimeout = time.time() + self.TipConfirmTimeoutSpeed
				elif self.LastDebugTime == 0.0 or time.time() > self.LastDebugTime:
						DebugLog(DL_DEBUG, "Tipout StepDogerSoak not all confirmed (" + str(len(self.ConfirmedUserList) + len(self.FailUserList)) + " of " + str(len(self.VerifiedUserList)) + ") Tipout still busy with " + str(len(NotifyTipThread.Notifications)) + " notifications.")
						self.LastDebugTime = time.time() + 3.0
			elif time.time() > self.TipConfirmTimeout:
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak not all confirmed, Timedout age: " + str(time.time() - self.TipConfirmTimeout))
				if C_Tip_Confirmation_Resending:
					DebugLog(DL_DEBUG, "Tipout StepDogerSoak moving to resend step")
					self.Step = TTipout.SOAKSTEP_RESEND
				else:
					DebugLog(DL_DEBUG, "Confirmation Resending disabled")
					self.Step = TTipout.SOAKSTEP_FAIL_CONF					
			elif self.LastDebugTime == 0.0 or time.time() > self.LastDebugTime:
					DebugLog(DL_DEBUG, "Tipout StepDogerSoak not all confirmed  (" + str(len(self.ConfirmedUserList) + len(self.FailUserList)) + " of " + str(len(self.VerifiedUserList)) + ") tip confirm timeout: " + str(self.TipConfirmTimeout - time.time()) + " seconds")
					self.LastDebugTime = time.time() + 3.0
		elif self.Step == TTipout.SOAKSTEP_DONE: # Done
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak Tipout DONE")
			self.Done = True
		elif self.Step == TTipout.SOAKSTEP_FAIL_CONF: # Failed confirms
			del self.MissedUserList[:]
			for UserName in self.VerifiedUserList:
				Confirmed = False
				for ConfUserName in self.ConfirmedUserList:
					if UserName == ConfUserName:
						Confirmed = True
						break
				if not Confirmed:
					self.MissedUserList.append(UserName)					
			DebugLog(DL_WARN, "Tipout StepDogerSoak not all confirmed, Timedout/failed, reporting missed users")
			DebugLog(DL_WARN, "!!! WARN !!! " + str(len(self.MissedUserList)) + " tips have failed to be confirmed:")
			DebugLog(DL_WARN, "   FromUser: " + str(self.FromName))
			DebugLog(DL_WARN, "    channel: " + self.ChannelName)
			DebugLog(DL_WARN, "      Value: " + str(self.Value))
			DebugLog(DL_WARN, "  UserValue: " + str(self.UserValue))
			DebugLog(DL_WARN, "  SoakTipID: " + self.TipID)		
			DebugLog(DL_WARN, "  MissUsers(" + str(len(self.MissedUserList)) + "): " + ",".join(self.MissedUserList))		
			DebugLog(DL_WARN, "  ConfUsers(" + str(len(self.ConfirmedUserList)) + "): " + ",".join(self.ConfirmedUserList))		
			DebugLog(DL_WARN, "  FailUsers(" + str(len(self.FailUserList)) + "): " + ",".join(self.FailUserList))
			AuditLog.LogSoak(self.FromName, self.ChannelName, self.Value, self.UserValue, self.ConfirmedUserList, self.MissedUserList, self.Message, self.FromPrivMsg)				
			self.Step = TTipout.SOAKSTEP_DONE
		elif self.Step == TTipout.SOAKSTEP_RESEND: # resend
			self.ResendCount = self.ResendCount + 1
			if self.ResendCount > self.MaxResend:
				DebugLog(DL_WARN, "Tipout StepDogerSoak Maximum resend attempts tried, quitting")
				self.Step = TTipout.SOAKSTEP_FAIL_CONF
			else:
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak Resend: confirmed: " + str(len(self.ConfirmedUserList)) + " fail count: " + str(self.TipFailCount) + " verified users: " + str(len(self.VerifiedUserList)))
				if (len(self.ConfirmedUserList) + self.TipFailCount) >= len(self.VerifiedUserList):
					DebugLog(DL_DEBUG, "Tipout StepDogerSoak Resend: all known fails, short wait")
					self.TipConfirmTimeout = time.time() + 10.0 # got all confirms counting known fails, wait just a few and get to resending
				else:
					DebugLog(DL_DEBUG, "Tipout StepDogerSoak Resend: missed tips not accounted for, using short wait")
					self.TipConfirmTimeout = time.time() + 10.0 # missed tips with no fail message, 
				self.Step = TTipout.SOAKSTEP_RESEND_WAIT
		elif self.Step == TTipout.SOAKSTEP_RESEND_WAIT: # Initial resend delay
			if (len(self.ConfirmedUserList) + len(self.FailUserList)) >= len(self.VerifiedUserList):
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak Resend Wait got all tips")
				self.TipConfirmTimeout = 0.0
				self.Step = TTipout.SOAKSTEP_WAIT_TIP_CONF # got all tip confirms?, re-check for done
			elif time.time() > self.TipConfirmTimeout:
				self.Step = TTipout.SOAKSTEP_RESEND_SEND  # didn't get all confirms, ready for resend
		elif self.Step == TTipout.SOAKSTEP_RESEND_SEND: # Send missed tipouts again
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak Resend Send")
			del self.MissedUserList[:]
			for UserName in self.VerifiedUserList:
				Confirmed = False
				for ConfUserName in self.ConfirmedUserList:
					if UserName == ConfUserName:
						Confirmed = True
						break
				if not Confirmed:
					self.MissedUserList.append(UserName)					
			self.TipFailCount = 0
			self.TipConfirmTimeout = 0.0 # start timeout over
			if len(self.MissedUserList) == 0:
				DebugLog(DL_ERROR, "Tipout StepDogerSoak step " + str(self.Step) + " Error, no missed users.")
			else:
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak resending " + str(len(self.MissedUserList)) + " tip PMs of value: " + str(self.UserValue) + " to Users: " + ",".join(self.MissedUserList))
				self.Message = self.Message + "Resending: " + " ".join(self.MissedUserList) + " "
				Msg = ""
				UserMsg = ""
				MsgUserCount = 0
				for UserName in self.MissedUserList:
					UserMsg = UserName + " " + str(self.UserValue)
					if (len(Msg) + len(UserMsg)) > C_Max_PrivMsg_Length:
						DebugLog(DL_DEBUG, "Tipout StepDogerSoak resend PrivMsg size exceeded, queuing current mtip message: " + Msg)
						NotifyTipThread.AddNotify("Soak", C_TipBotNick, "!mtip " + Msg)
						Msg = ""
						MsgUserCount = 0
					if MsgUserCount != 0:
						Msg = Msg + " "
					Msg += UserMsg
					MsgUserCount += 1
				if MsgUserCount > 0:
					DebugLog(DL_DEBUG, "Tipout StepDogerSoak end of resend mtip build, queuing current mtip message: " + Msg)
					NotifyTipThread.AddNotify("Soak", C_TipBotNick, "!mtip " + Msg)
			self.Step = TTipout.SOAKSTEP_WAIT_TIP_CONF
		elif self.Step == TTipout.SOAKSTEP_REFUSED:
			self.TipConfirmTimeout = time.time() + 10.0
			DebugLog(DL_DEBUG, "Tipout StepDogerSoak Refused. FromName: " + self.FromName + ", Value: " + str(self.Value) + ", TipID: " + self.TipID + ", FromPrivMsg: " + str(self.FromPrivMsg) + " Message: " + self.Message)
			NewTipout = TipoutThread._AddTipback(self.ChannelName, self.FromName, self.Value, self.Message, self.TipID)
			if NewTipout:
				NewTipout.FromPrivMsg = self.FromPrivMsg
			if self.TipConfirmed:
				NewTipout.ConfirmedTipID(self.TipID) # resend TipID incase tipback missed it to prevent race condition
				#TipoutThread._ConfirmedTipID(self.TipID) # calls ConfirmedTipID on this Tipout wich is locked
			self.Step = TTipout.SOAKSTEP_REFUSED_WAIT
		elif self.Step == TTipout.SOAKSTEP_REFUSED_WAIT:
			if self.TipConfirmed:
				DebugLog(DL_DEBUG, "Tipout StepDogerSoak confirmed after refused TipID: " + self.TipID + ", Value: " + str(self.Value) + ", FromName: " + self.FromName)
				self.Done = True
			elif time.time() > self.TipConfirmTimeout:
				DebugLog(DL_WARN, "Tipout StepDogerSoak waiting for PM confirm refused soak timed out, setting done, for TipID: " + self.TipID + ", Value: " + str(self.Value) + ", FromName: " + self.FromName)
				self.Done = True
		else:        # Invalid Step
			DebugLog(DL_ERROR, "Tipout StepDogerSoak step invalid: " + str(self.Step))
			self.Done = True
		#DebugLog(DL_DEBUG, "Tipout StepDogerSoak Step...DONE (new step: " + str(self.Step) + ")")
		
	def StepDogerTipback(self):
		global AuditLog, TextHandler
		if self.Step != self.LastStep or self.Step == 0:
			DebugLog(DL_DEBUG, "Tipout StepDogerTipback Step " + str(self.Step) + "...")
			self.LastStep = self.Step
		if self.Step == TTipout.TIPBACK_STEP_TIPBACK:
			del self.VerifiedUserList[:]
			self.VerifiedUserList.append(self.FromName) # this will make update tip confirm work
			self.ResendCount = 0
			DebugLog(DL_DEBUG, "Tipout StepDogerTipback Tipback starting user: " + self.FromName + " value: " + str(self.Value))
			self.Step = TTipout.TIPBACK_STEP_CHAN_MSG
		elif self.Step == TTipout.TIPBACK_STEP_CHAN_MSG: 
			Msg = TextHandler.GetTextChan("SOAK_REFUSE_BASE", self.ChannelName).format(self.FromName, self.Message)
			if self.FromPrivMsg == True:
				DebugLog(DL_DEBUG, "Tipout StepDogerTipback PrivMsg message: " + self.Message)
				NotifyTipThread.AddNotify("Tipback", self.FromName, Msg)
			else:
				DebugLog(DL_DEBUG, "Tipout StepDogerTipback channel message: " + self.Message)
				IRC.ChanMsg(self.ChannelName, Msg)
			self.Step = TTipout.TIPBACK_STEP_PM_TIP
		elif self.Step == TTipout.TIPBACK_STEP_PM_TIP:
			DebugLog(DL_DEBUG, "Tipout StepDogerTipback sending tip pm user: " + self.FromName + " value: " + str(self.Value))
			NotifyTipThread.AddPriorityNotify("Tipback", C_TipBotNick, "!mtip " + self.FromName + " " + str(self.Value))
			self.TipConfirmTimeout = time.time() + self.TipConfirmTimeoutSpeed
			self.Step = TTipout.TIPBACK_STEP_WAIT_CONF
		elif self.Step == TTipout.TIPBACK_STEP_WAIT_CONF:
			if (len(self.ConfirmedUserList) == 1 and self.ConfirmedUserList[0] == self.FromName) or (len(self.FailUserList) == 1 and self.FailUserList == self.FromName):
				DebugLog(DL_DEBUG, "Tipout StepDogerTipback confirmed channel: " + self.ChannelName + ", user: " + self.FromName + " value: " + str(self.Value))
				AuditLog.LogTipback(self.FromName, self.ChannelName, self.Value, self.Message)
				self.Step = TTipout.TIPBACK_STEP_DONE
			elif time.time() > self.TipConfirmTimeout: # not using TipConfirmTimeout as 0.0 for StepTipback
				DebugLog(DL_DEBUG, "Tipout StepDogerTipback not confirmed, Timedout user: " + self.FromName + " value: " + str(self.Value))
				if C_Tip_Confirmation_Resending:
					self.Step = TTipout.TIPBACK_STEP_RESEND
				else:
					DebugLog(DL_DEBUG, "Confirmation resending disabled, ignoring...")
					AuditLog.LogTipback(self.FromName, self.ChannelName, self.Value, "(Return failed!) " + self.Message)
					self.Step = TTipout.TIPBACK_STEP_DONE
		elif self.Step == TTipout.TIPBACK_STEP_DONE:
			if self.FromPrivMsg or self.TipConfirmed: # not confirmation for PrivMsg
				DebugLog(DL_DEBUG, "Tipout StepDogerTipback DONE")
				self.Done = True
			else:
				DebugLog(DL_DEBUG, "Tipout StepDogerTipback DONE, waiting for confirm")
				self.TipConfirmTimeout = time.time() + 10.0
				self.Step = TTipout.TIPBACK_STEP_DONE_WAIT			
		elif self.Step == TTipout.TIPBACK_STEP_DONE_WAIT:
			if self.TipConfirmed:
				self.Done = True
			elif time.time() > self.TipConfirmTimeout:
				DebugLog(DL_WARN, "Tipout StepDogerTipback Done Wait timed out, no confirm received, setting done")
				self.Done = True
		elif self.Step == TTipout.TIPBACK_STEP_RESEND:
			DebugLog(DL_ERROR, "Tipout StepDogerTipback Resend " + self.FromName + " value: " + str(self.Value))
			self.TipConfirmTimeout = time.time() + 10.0
			self.ResendCount = self.ResendCount + 1
			if self.ResendCount > self.MaxResend:
				DebugLog(DL_WARN, "Tipout StepDogerTipback Maximum resend attempts tried, quitting user: " + self.FromName + " value: " + str(self.Value))
				AuditLog.LogTipback(self.FromName, self.ChannelName, self.Value, "(Failed!) " + self.Message)
				self.Step = TTipout.TIPBACK_STEP_DONE
			else:
				self.Step = TTipout.TIPBACK_STEP_RESEND_WAIT
		elif self.Step == TTipout.TIPBACK_STEP_RESEND_WAIT:
			#DebugLog(DL_ERROR, "Tipout StepTipback resend wait")
			if (len(self.ConfirmedUserList) == 1 and self.ConfirmedUserList[0] == self.FromName) or (len(self.FailUserList) == 1 and self.FailUserList == self.FromName):
				DebugLog(DL_DEBUG, "Tipout StepDogerTipback Resend Wait got confirm")
				self.Step = TTipout.TIPBACK_STEP_WAIT_CONF # got all tip confirms
			elif time.time() > self.TipConfirmTimeout:
				self.Step = TTipout.TIPBACK_STEP_RESEND_SEND  
		elif self.Step == TTipout.TIPBACK_STEP_RESEND_SEND:
			DebugLog(DL_DEBUG, "Tipout StepDogerTipback resending " + self.FromName + " value: " + str(self.Value))
			NotifyTipThread.AddNotify("Soak", C_TipBotNick, "!mtip " + self.FromName + " " + str(self.Value))
			self.TipConfirmTimeout = 0.0 # start timeout over
			self.Step = TTipout.TIPBACK_STEP_WAIT_CONF
		else:
			DebugLog(DL_ERROR, "Tipout StepDogerTipback step invalid: " + str(self.Step))
			self.Done = True


# pylint: enable=W0212

#######################################################
###                 TTipoutThread                   ###
#######################################################

class TTipoutThread(TThread):

	def __init__(self):
		global C_TipConfirm_Timeout, C_TipConfirm_Timeout_Long
		TThread.__init__(self, self.MainLoop)
		self.Active = False
		self.SoakEnabled = False
		self.UnderbalanceCount = 0
		self.UnderbalanceMax = 3
		self.LastSoakTime = 0.0
		self.StepSpeedActive = 0.01
		self.StepSpeedInactive = 0.1
		self.Tipbacks = []
		self.Tipouts = []
		self.SoakDelay = C_Soak_Delay # seconds
		self.LastBalance = 0.0
		self.AuthUsers = TUserList() # session based caching of whois verified Auth users
		self.AuthUsers.ErrorOnNicks = True # optomize by ignoring Nicks list, error if try to use it.
		self.TipConfirmTimeoutSpeed = C_TipConfirm_Timeout # seconds, starts after last tip PM is sent, reset after each recieved tip confirm
		self.TipConfirmTimeoutSpeedLong = C_TipConfirm_Timeout_Long # seconds, starts after last tip PM is sent, but tips were missed and not reported f
		
	def Reset(self):
		self.AuthUsers.Clear()
		self.ResetSoak()
		self.UnderbalanceCount = 0
		
	def Start(self):
		if not self.Active:
			TThread.Start(self)
			self.SoakEnabled = True
		
	def Stop(self):
		self.SoakEnabled = False
		self.Terminate = True

	def Pause(self):
		self.SoakEnabled = False

	def Resume(self):
		self.SoakEnabled = True
		
	def TouchSoak(self):
		if not self.TryLock():
			return False
		self._TouchSoak()
		self.Unlock()

	def _TouchSoak(self):
		self.LastSoakTime = time.time()

	def CanSoak(self):
		if self.Terminate:
			return False
		return True
		#### One at a time is legacy
		#if self.IsBusy():
		#	return False
		#else:
		#	if self.LastSoakTime == 0.0 or (time.time() - self.LastSoakTime) > self.SoakDelay:
		#		return True
		#	else:
		#		return False
			
	def ResetSoak(self):
		self.LastSoakTime = 0.0

	def SoakWaitLeft(self): 
		if self.LastSoakTime == 0.0:
			return 0.0
		return (self.SoakDelay - (time.time() - self.LastSoakTime))

	def Cancel(self): 
		self.Terminate = True      

	def Resend(self):
		if not self.TryLock():
			return False
		self._Resend()
		self.Unlock()

	def _Resend(self):
		for Tipout in self.Tipouts:
			Tipout.Resend()
		
	def Underbalance(self, User, Value):
		if not self.TryLock():
			return False
		self._Underbalance(User, Value)
		self.Unlock()
		
	def _Underbalance(self, User, Value):
		global __module_name__, NotifyTipThread, OperatorList, NotifyMessageThread
		self._TipFailDetected(User, Value)
		if self.UnderbalanceCount > self.UnderbalanceMax:
			print(__module_name__ + " Underbalance detected, switching to maintenence mode!")
			self.Pause() # TipoutThread
			NotifyTipThread.Reset()

	def AddTipout(self, TipType, ChannelName, FromName, UserList, Value, TipID):
		if not self.TryLock():
			return False
		Tipout = self._AddTipout(TipType, ChannelName, FromName, UserList, Value, TipID)
		self.Unlock()
		return Tipout
			
	def _AddTipout(self, TipType, ChannelName, FromName, UserList, Value, TipID):
		Tipout = TTipout(TipType, ChannelName, FromName, UserList, Value, "", TipID)
		self.Tipouts.append(Tipout)
		DebugLog(DL_DEBUG, "Added tipout TipID: " + TipID + " from " + Tipout.FromName + ", count now: " + str(len(self.Tipouts)))
		return Tipout
		
	def AddTipback(self, ChannelName, ToName, Value, Mesage, TipID):
		if not self.TryLock():
			return False
		Tipout = self._AddTipback(ChannelName, ToName, Value, Mesage, TipID)
		self.Unlock()
		return Tipout
			
	def _AddTipback(self, ChannelName, ToName, Value, Mesage, TipID):
		EmptyUserList = []
		Tipout = TTipout("dtipback", ChannelName, ToName, EmptyUserList, Value, Mesage, TipID)
		#self.Tipbacks.append(Tipout)
		#DebugLog(DL_DEBUG,  "Added tipback TipID: " + TipID + " ChannelName: " + ChannelName + " ToName: " + Tipout.FromName + " Value: " +  str(Value) + " tipback count now: " + str(len(self.Tipbacks)))
		self.Tipouts.append(Tipout)
		DebugLog(DL_DEBUG, "Added tipback TipID: " + TipID + " ChannelName: " + ChannelName + " ToName: " + Tipout.FromName + " Value: " +  str(Value) + " tipback count now: " + str(len(self.Tipouts)))
		return Tipout
		
	def ConfirmedTipID(self, TipID):
		if not self.TryLock():
			return False
		Found = self._ConfirmedTipID(TipID)
		self.Unlock()
		return Found
		
	def _ConfirmedTipID(self, TipID):
		Found = False
		for Tipout in self.Tipouts:
			if Tipout.ConfirmedTipID(TipID):
				DebugLog(DL_DEBUG, "TTipoutThread::ConfirmedTipID Tipout found, setting to confirmed, TipID: " + TipID)
				Found = True
				break
		for Tipout in self.Tipbacks:
			if Tipout.ConfirmedTipID(TipID):
				DebugLog(DL_DEBUG, "TTipoutThread::ConfirmedTipID Tipout Tipbacks, setting to confirmed, TipID: " + TipID)
				Found = True
				break
		return Found

	def UpdateWhoIsAuth(self, Nick, AuthNick):
		if not self.TryLock():
			return False
		self._UpdateWhoIsAuth(Nick, AuthNick)
		self.Unlock()
		
	def _UpdateWhoIsAuth(self, Nick, AuthNick):
		#DebugLog(DL_DEBUG, "TTipoutThread::UpdateWhoIsAuth Nick: " + Nick + ", AuthNick: " + AuthNick) 
		User = self.AuthUsers.AddUniqueUser(Nick)
		User.AuthNick = AuthNick
		for Tipout in self.Tipouts:
			Tipout.UpdateWhoIsAuth(Nick)

	def UpdateWhoNameLine(self, Nick, HostUser, Host, RealName):
		if not self.TryLock():
			return False
		self._UpdateWhoNameLine(Nick, HostUser, Host, RealName)
		self.Unlock()

	def _UpdateWhoNameLine(self, Nick, HostUser, Host, RealName):
		User = self.AuthUsers.AddUniqueUser(Nick)
		User.HostUser = HostUser
		User.Host = Host
		User.RealName = RealName
	
	def UpdateWhoIsEnd(self, Nick):
		if not self.TryLock():
			return False
		self._UpdateWhoIsEnd(Nick)
		self.Unlock()

	def _UpdateWhoIsEnd(self, Nick):
		for Tipout in self.Tipouts:
			Tipout.UpdateWhoIsEnd(Nick)

	def FindUserByNick(self, Nick):
		if not self.TryLock():
			return False
		User = self.AuthUsers.FindUserByNick(Nick)
		self.Unlock()
		return User
	
	def FindAuthUserByNick(self, Nick):
		User = self.AuthUsers.FindUserByNick(Nick)
		if User == False or User.AuthNick == "":
			return False
		return User

	def FindUnAuthUserNicks(self, Nicks):
		if not self.TryLock():
			return False
		UnAuthNicks = self.AuthUsers.FindUnAuthUserNicks(Nicks)
		self.Unlock()
		return UnAuthNicks

	def RemoveAuthNick(self, Nick):
		if not self.TryLock():
			return False
		self._RemoveAuthNick(Nick)
		self.Unlock()
		
	def _RemoveAuthNick(self, Nick):
		self.AuthUsers.RemoveByNick(Nick)
		#DebugLog(DL_DEBUG, "TTipoutThread::RemoveAuthNick Nick: " + Nick + " removed AuthUsers count: " + str(len(self.AuthUsers.Users)))
			
	def ChangeAuthUserNick(self, OldNick, NewNick):
		if not self.TryLock():
			return False
		self._ChangeAuthUserNick(OldNick, NewNick)
		self.Unlock()

	def _ChangeAuthUserNick(self, OldNick, NewNick):
		User = self.AuthUsers.FindUserByNick(OldNick)
		if User != False:
			User.Nick = NewNick		

	def UpdateTipConfirm(self, Nick, Value, Balance):
		if not self.TryLock():
			return False
		self._UpdateTipConfirm(Nick, Value, Balance)
		self.Unlock()

	def _UpdateTipConfirm(self, Nick, Value, Balance):
		if len(self.Tipouts) == 0 and len(self.Tipbacks) == 0:
			DebugLog(DL_WARN, "TTipoutThread::UpdateTipConfirm no active tipouts, tip confirmation unexpected!")
			DebugLog(DL_WARN, "   Nick: " + Nick + " Value: " + str(Value) + " Balance: " + str(Balance) + " Prev Balance: " + str(self.LastBalance))
			return
		if Balance >= 0:
			self.UnderbalanceCount = 0
			self.LastBalance = Balance
		for Tipout in self.Tipbacks:
			Tipout.UpdateTipConfirm(Nick, Value) 
		for Tipout in self.Tipouts:
			Tipout.UpdateTipConfirm(Nick, Value) 

	def UpdateTipConfirmMulti(self, NickList, ConfValue):
		if not self.TryLock():
			return False
		self._UpdateTipConfirmMulti(NickList, ConfValue)
		self.Unlock()

	def _UpdateTipConfirmMulti(self, NickList, ConfValue):
		if len(self.Tipouts) == 0 and len(self.Tipbacks) == 0:
			DebugLog(DL_WARN, "TTipoutThread::UpdateTipConfirmMulti no active tipouts, tip confirmation unexpected! Value: " + str(ConfValue) + " NickList: " + ",".join(NickList))
			return
		for Tipout in self.Tipbacks:
			Tipout.UpdateTipConfirmMulti(NickList, ConfValue) 
		for Tipout in self.Tipouts:
			Tipout.UpdateTipConfirmMulti(NickList, ConfValue) 

	def TipFailDetected(self, Nick, ConfValue):
		if not self.TryLock():
			return False
		self._TipFailDetected(Nick, ConfValue)
		self.Unlock()

	def _TipFailDetected(self, Nick, ConfValue):
		#DebugLog(DL_DEBUG, "TipoutThread::TipFailDetected sending detection to " + str(len(self.Tipouts)) + " tipouts and " + str(len(self.Tipbacks)) + " tipbacks")
		for Tipout in self.Tipbacks:
			Tipout.TipFailDetected(Nick, ConfValue)		
		for Tipout in self.Tipouts:
			Tipout.TipFailDetected(Nick, ConfValue)		

	def TipFailDetectedMulti(self, NickList, ConfValue):
		if not self.TryLock():
			return False
		self._TipFailDetectedMulti(NickList, ConfValue)
		self.Unlock()

	def _TipFailDetectedMulti(self, NickList, ConfValue):
		if len(self.Tipouts) == 0 and len(self.Tipbacks) == 0:
			DebugLog(DL_WARN, "TTipoutThread::TipFailDetectedMulti no active tipouts, tip failure unexpected! Value: " + str(ConfValue) + " NickList: " + ",".join(NickList))
			return
		for Tipout in self.Tipbacks:
			Tipout.TipFailDetectedMulti(NickList, ConfValue) 
		for Tipout in self.Tipouts:
			Tipout.TipFailDetectedMulti(NickList, ConfValue) 

	def AuthUserContainsNick(self, Nick):
		if not self.TryLock():
			return False
		reply = self._AuthUserContainsNick(Nick)
		self.Unlock()
		return reply

	def _AuthUserContainsNick(self, Nick):
		if self.AuthUsers.FindUserByNick(Nick) == False:
			return False
		return True
		
	def IsBusy(self):
		if not self.TryLock():
			return False
		Reply = self._IsBusy()
		self.Unlock()
		return Reply

	def _IsBusy(self):
		if len(self.Tipouts) == 0 and len(self.Tipbacks) == 0:
			return False
		else:
			return True
		
	def MainLoop(self):
		time.sleep(0.001)
		self.Active = True
		if self.Terminate: 
			return # test once, before try block to prevent Unlock
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG Tipout thread started")
		while not self.Terminate:
			if not self.TryLock():
				DebugLog(DL_ERROR, "TTipoutThread::MainLoop TryLock failed")
				return
			try: 
				tidx = 0
				while len(self.Tipouts) > tidx:
					#DebugLog(DL_DEBUG,  "TTipoutThread::MainLoop stepping tipout [" + str(tidx) + "] tipout of " + str(len(self.Tipouts)) + " with fromName: " + self.Tipouts[tidx].FromName)
					self.Tipouts[tidx].Execute()
					if self.Tipouts[tidx].Done:
						#DebugLog(DL_DEBUG,  "TTipoutThread::MainLoop deleteing done tipout [" + str(tidx) + "] tipout of " + str(len(self.Tipouts)) + " with fromName: " + self.Tipouts[tidx].FromName)
						del self.Tipouts[tidx]
						tidx = 0 # deleted one, start back at first
					elif self.Tipouts[tidx].IsCriticalBusy():
						#DebugLog(DL_DEBUG,  "TTipoutThread::MainLoop waiting on critical busy tipout [" + str(tidx) + "] tipout of " + str(len(self.Tipouts)) + " with fromName: " + self.Tipouts[tidx].FromName)
						tidx = len(self.Tipouts) + 100 # don't process another Tipout
					else:
						tidx += 1
				#DebugLog(DL_DEBUG,  "TTipoutThread::MainLoop while over, tipout count: " + str(len(self.Tipouts)) + ", tidx: " + str(tidx))
			except:
				DebugLog(DL_ERROR, "TTipoutThread exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
			finally:
				self.Active = False
				self.TouchHeartbeatLocked()
				self.Unlock()
			if self.Terminate: 
				return
			if len(self.Tipouts) > 0:
				self.SafeSleep(self.StepSpeedActive)
			else:
				self.SafeSleep(self.StepSpeedInactive)
			if self.CrashTest == True:
				raise Exception("TTipoutThread Crash Test Activated!")
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG Tipout thread done")
        
#######################################################
###                 TUser                            ###
#######################################################

class TUser(TLockable):
	def __init__(self, Name):
		TLockable.__init__(self)
		self.Name = Name
		self.FirstTimeDate = time.time()
		self.LastActive = time.time()
		self.Rate = 0.0
        
	def TouchActive(self):
		self.LastActive = time.time()

	def IncRate(self):
		self.Rate = self.Rate + 1
       
#######################################################
###                 TUserMonitorThread           ###
#######################################################

class TUserMonitorThread(TThread):

	def __init__(self):
		global C_User_Active_Threshold, C_ActiveUser_Check_Rate
		TThread.__init__(self, self.MainLoop)
		self.languageID = "default"
		self.Active = False
		self.UserStats = []
		self.CurrentUsers = []
		self.ChannelName = "(new object)"
		self.ChannelJoined = False
		self.LastChannelLeft = time.time()
		self.ChannelLeftReason = "(new object)"
		# Hard Configurations
		self.Delay = 10.0 # seconds 
		self.UserHoldCheckRate = C_ActiveUser_Check_Rate # seconds
		self.CommitUserStatsRate = 120.0 # seconds
		self.UserHoldTime = C_User_Active_Threshold
		
	def Start(self):
		if not self.Active:
			TThread.Start(self)
		
	def Stop(self):
		self.Terminate = True
		
	def Cancel(self): 
		self.Terminate = True        

	def Reset(self):
		if not self.TryLock():
			DebugLog(DL_WARN, "TUserMonitorThread::Reset TryLock Failed")
			return False
		self._Reset()
		self.Unlock()

	def _Reset(self):
		del self.CurrentUsers[:]
		del self.UserStats[:]
		
	def Joined(self, ChannelName):
		if not self.TryLock():
			return False
		self.ChannelName = ChannelName
		self.ChannelJoined = True
		self.Unlock()
		
	def Parted(self, Reason):
		if not self.TryLock():
			return False
		self.ChannelJoined = False
		self.LastChannelLeft = time.time()
		self.ChannelLeftReason = "Parted: " + Reason
		self.Unlock()
		
	def Kicked(self, Reason):
		if not self.TryLock():
			return False
		self.ChannelJoined = False
		self.LastChannelLeft = time.time()
		self.ChannelLeftReason = "Kicked: " + Reason
		self.Unlock()
		
	def RatePerMin(self, Rate):
		return Rate * (60.0 / self.CommitUserStatsRate)
		
	def AllUsers(self):
		if not self.TryLock():
			return False
		Users = self._AllUsers()
		self.Unlock()
		return Users
		
	def _AllUsers(self):
		for CurrentUser in self.CurrentUsers:
			UserStat = self._FindUserStat(CurrentUser.Name)
			if UserStat == False:
				UserStat = TUser(CurrentUser.Name)
				self.UserStats.append(UserStat)
		return list(self.UserStats) # return duplicate of list 

	def AllUserNames(self):
		if not self.TryLock():
			return False
		Users = self._AllUserNames()
		self.Unlock()
		return Users
		
	def _AllUserNames(self):
		UserNames = []
		self._AllUsers() # updates self.UserStats 
		for User in self.UserStats:
			UserNames.append(User.Name)
		return UserNames 
		
	def FindUser(self, Name):
		if not self.TryLock():
			return False
		User = self._FindUser(Name)
		self.Unlock()
		return User
		
	def _FindUser(self, Name):
		User = self._FindUserStat(Name)
		if User != False:
			return User
		User = self._FindCurrentUser(Name)
		if User != False:
			return User		
		return False

	def FindCurrentUser(self, Name):
		if not self.TryLock():
			return False
		User = self._FindCurrentUser(Name)
		self.Unlock()
		return User

	def _FindCurrentUser(self, Name):
		for User in self.CurrentUsers:
			if User.Name == Name:
				return User
		return False

	def FindUserStat(self, Name):
		if not self.TryLock():
			return False
		User = self._FindUserStat(Name)
		self.Unlock()
		return User		
		
	def _FindUserStat(self, Name):
		for User in self.UserStats:
			if User.Name == Name:
				return User
		return False
		
	def ForgetUser(self, Name):
		if not self.TryLock():
			return False
		ret = self._ForgetUser(Name)
		self.Unlock()
		return ret		

	def _ForgetUser(self, Name):
		Found = False
		for idx in range(0, len(self.CurrentUsers)):
			if self.CurrentUsers[idx].Name == Name:
				del self.CurrentUsers[idx]
				Found = True
				break
		for idx in range(0, len(self.UserStats)):
			if self.UserStats[idx].Name == Name:
				del self.UserStats[idx]
				Found = True
				break
		return Found
		
	def ForgetOldUserStats(self):
		global DEBUG_User_Forgotten
		OldTime = time.time() - self.UserHoldTime
		#DebugLog(DL_DEBUG, "Forget old user Stats Now: " + str(time.time()) + " Too Old Time: " + str(OldTime) + " ...")
		#DebugLog(DL_DEBUG, "Forget old user Stats...")
		for idx in range((len(self.UserStats) - 1), -1, -1): # iterate backwards
			if self.UserStats[idx].LastActive < OldTime:
				UserFound = self._FindCurrentUser(self.UserStats[idx].Name)
				if UserFound == False:
					UserStat = self.UserStats[idx]
					Age = time.time() - UserStat.LastActive
					if DEBUG_User_Forgotten == True:
						DebugLog(DL_INFO, "User [" + str(idx) + "] '" + UserStat.Name + "' forgotten, last seen: " + str(UserStat.LastActive) + " Age: " + str(Age))
					del self.UserStats[idx]		
	
	def CommitUserStats(self):
		#DebugLog(DL_DEBUG, "Committing User Stats...")
		for CurrentUser in self.CurrentUsers:
			UserStat = self._FindUserStat(CurrentUser.Name)
			if UserStat == False:
				UserStat = TUser(CurrentUser.Name)
				self.UserStats.append(UserStat)
			UserStat.Rate = (UserStat.Rate + CurrentUser.Rate) / 2.0
			UserStat.TouchActive()
		del self.CurrentUsers[:] # clear CurrentUsers			
		#DebugLog(DL_DEBUG, "Committing User Stats...DONE")

	def AddUpdateUser(self, Name):
		if not self.TryLock():
			return False
		User = self._AddUpdateUser(Name)
		self.Unlock()
		return User

	def _AddUpdateUser(self, Name):
		User = self._FindCurrentUser(Name)
		if User == False:
			User = TUser(Name)
			self.CurrentUsers.append(User)
		User.TouchActive()
		User.IncRate()
		return User
		
	def InjectActiveUser(self, Name, TimeStamp):
		if not self.TryLock():
			return False
		User = self._InjectActiveUser(Name, TimeStamp)
		self.Unlock()
		return User

	def _InjectActiveUser(self, Name, TimeStamp):
		User = self._FindCurrentUser(Name)
		if User == False:
			User = TUser(Name)
			self.CurrentUsers.append(User)
		User.LastActive = TimeStamp
		return User		
	
	def MainLoop(self):
		time.sleep(0.001)
		self.Active = True
		UserHoldChatTime = time.time() + self.UserHoldCheckRate
		CommitUsersTime = time.time() + self.CommitUserStatsRate
		if self.Terminate: 
			return # test once, before try block to prevent Unlock
		if DEBUG_Notify: 
			DebugLog(DL_INFO, "DEBUG User Monitor thread started")
		while not self.Terminate:
			if not self.TryLock():
				DebugLog(DL_INFO, "TUserMonitorThread::MainLoop TryLock failed")
				return False
			try: # try/finally trap for Terminating/Unlock'ing/DeReference'ing
				if time.time() > UserHoldChatTime:
					self.ForgetOldUserStats()
					UserHoldChatTime = time.time() + self.UserHoldCheckRate
				if time.time() > CommitUsersTime:
					self.CommitUserStats()
					CommitUsersTime = time.time() + self.CommitUserStatsRate
			except:
				DebugLog(DL_ERROR, "TUserMonitorThread exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
			finally:
				self.Active = False
				self.TouchHeartbeatLocked()
				self.Unlock()
			if self.Terminate: 
				return
			self.SafeSleep(self.Delay)
			if self.CrashTest == True:
				raise Exception("TUserMonitorThread  Crash Test Activated!")
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG User Monitor thread done")
	
	def RequestSoak(self, ChannelName, UserName, Value, TipID, FromPrivMsg):
		global Debug_RestrictUser, UserMonitorThread, TipoutThread, OperatorList, TextHandler
		if not Debug_RestrictUser or OperatorList.ContainsNick(UserName):
			DebugLog(DL_DEBUG, "Got soak request from: " + UserName + " for value: " + str(Value) + " From PM: " + str(FromPrivMsg))
			return self.Soak(ChannelName, UserName, Value, TipID, FromPrivMsg)
		else:
			DebugLog(DL_DEBUG, "Got soak request from: " + UserName + " for value: " + str(Value) + "  but restricted, FromPM: " + str(FromPrivMsg))
			NewTipout = TipoutThread.AddTipback(ChannelName, UserName, Value, TextHandler.GetTextChan("SOAK_REFUSE_RESTRICTED", ChannelName), TipID)
			if NewTipout:
				NewTipout.FromPrivMsg = FromPrivMsg
			return True # no error, but soak refused
			
	def Soak(self, ChannelName, FromName, Value, TipID, FromPrivMsg):
		if not self.TryLock():
			DebugLog(DL_WARN, "UserMonitorThread::Soak TryLock failed, ChannelName: " + self.ChannelName + ", joined: " + BoolYN(self.ChannelJoined))
			return False
		r = self._Soak(ChannelName, FromName, Value, TipID, FromPrivMsg)
		self.Unlock()
		return r
		
	def _Soak(self, ChannelName, FromName, Value, TipID, FromPrivMsg):
		global TipoutThread, TextHandler
		if not TipoutThread.SoakEnabled:
			DebugLog(DL_INFO, "UserMonitorThread Soak disabled")
			NewTipout = TipoutThread.AddTipback(ChannelName, FromName, Value, TextHandler.GetTextChan("SOAK_REFUSE_DISABLED", ChannelName), TipID)
			if NewTipout:
				NewTipout.FromPrivMsg = FromPrivMsg
		elif not TipoutThread.CanSoak():
			if TipoutThread.IsBusy() and NotifyTipThread.Count() != 0:
				Eta = NotifyTipThread.Count() * 2
				DebugLog(DL_INFO, "UserMonitorThread Soak still busy seconds: " + str(Eta))
				Msg = TextHandler.GetTextChan("SOAK_REFUSE_SOAKBUSYTIPPING", ChannelName).format(Eta)
			else:
				DebugLog(DL_INFO, "UserMonitorThread Soak still waiting for confirmations")
				Msg = TextHandler.GetTextChan("SOAK_REFUSE_SOAKBUSY", ChannelName)
			NewTipout = TipoutThread.AddTipback(ChannelName, FromName, Value, Msg, TipID)
			if NewTipout:
				NewTipout.FromPrivMsg = FromPrivMsg
		else:
			DebugLog(DL_INFO, "UserMonitorThread adding Soak tipout (FromPM: " + str(FromPrivMsg) + ")...")
			NewTipout = TipoutThread.AddTipout("dsoak", ChannelName, FromName, self._AllUsers(), Value, TipID) # send copy of user list, not reference
			if NewTipout:
				NewTipout.FromPrivMsg = FromPrivMsg
		return True
			
#######################################################
###                 TUserMonitors                   ###
#######################################################

class TUserMonitors(TThread):

	def __init__(self):
		global C_DeadChannel_Timeout
		TThread.__init__(self, self.MainLoop)
		self.Active = False
		self.Delay = 300.0 # seconds  (5 minutes)
		self.DeadChannelTimeout = C_DeadChannel_Timeout # seconds 
		self.UserMonitors = []
		self.RecoverMonitorCount = 0 # used in Heartbeat recovery to give up
		
	def Start(self):
		if not self.Active:
			TThread.Start(self)
		
	def Stop(self):
		self.Terminate = True
	
	def WaitStop(self):
		self.TryLock() # ignore lock fail (due to terminating thread)
		DebugLog(DL_DEBUG, "TUserMonitors wait stopping monitor count: " + str(len(self.UserMonitors)))
		for UserMonitor in self.UserMonitors:
			DebugLog(DL_DEBUG, "TUserMonitors wait stopping monitor channel: " + UserMonitor.ChannelName)
			UserMonitor.WaitStop()
		self.Unlock()
		DebugLog(DL_DEBUG, "TUserMonitors wait stopping base")
		TThread.WaitStop(self)
		DebugLog(DL_DEBUG, "TUserMonitors wait stopping done")
        
	def Cancel(self): 
		self.Terminate = True        
	
	def Reset(self):
		if not self.TryLock():
			return False
		DebugLog(DL_DEBUG, "TUserMonitors Reset stopping monitor count: " + str(len(self.UserMonitors)))
		for UserMonitor in self.UserMonitors:
			DebugLog(DL_DEBUG, "TUserMonitors Reset stopping monitor channel: " + UserMonitor.ChannelName)
			UserMonitor.WaitStop()
		DebugLog(DL_DEBUG, "TUserMonitors Reset stopping base")
		self.UserMonitors = []
		DebugLog(DL_DEBUG, "TUserMonitors Reset stopping done")
		self.Unlock()
		
	def Add(self):
		if not self.TryLock():
			return False
		um = self._Add()
		self.Unlock()
		return um
		
	def _Add(self):
		um = TUserMonitorThread()
		self.UserMonitors.append(um)
		um.Start()
		return um
		
	def FindChannel(self, ChannelName):
		if not self.TryLock():
			return False
		um = self._FindChannel(ChannelName)
		self.Unlock()
		return um

	def _FindChannel(self, ChannelName):
		for um in self.UserMonitors:
			if um.ChannelName.lower() == ChannelName.lower():
				return um
		return False

	def FindAddChannel(self, ChannelName):
		if not self.TryLock():
			return False
		um = self._FindAddChannel(ChannelName)
		self.Unlock()
		return um

	def _FindAddChannel(self, ChannelName):
		global DogeSoaker
		um = self._FindChannel(ChannelName)
		if um == False:
			um = self._Add()
		if um == False:
			DebugLog(DL_ERROR, "TUserMonitors::_FindAddChannel find add still is false, add failed? Dumping debug...")
			DogeSoaker.DebugDump()
			return False
		return um
	
	def ForgetUser(self, Nick):
		if not self.TryLock():
			return False
		self._ForgetUser(Nick)
		self.Unlock()

	def _ForgetUser(self, Nick):
		for um in self.UserMonitors:
			um.ForgetUser(Nick)
	
	def JoinAddChannel(self, ChannelName):
		if not self.TryLock():
			return False
		um = self._JoinAddChannel(ChannelName)
		self.Unlock()
		return um

	def _JoinAddChannel(self, ChannelName):
		global DogeSoaker
		um = self._FindChannel(ChannelName)
		if um == False:
			um = self._Add()
		if um == False:
			DebugLog(DL_ERROR, "TUserMonitors::_JoinAddChannel find add still is false, add failed? Dumping debug...")
			DogeSoaker.DebugDump()
			return False			
		um.Joined(ChannelName)
		return um
	
	def RemoveChannel(self, ChannelName):
		if not self.TryLock():
			return False
		r = self._RemoveChannel(ChannelName)
		self.Unlock()
		return r

	def _RemoveChannel(self, ChannelName):
		for idx in range(0, len(self.UserMonitors)):
			if self.UserMonitors[idx].ChannelName.lower() == ChannelName.lower():
				DebugLog(DL_ERROR, "TUserMonitors RemoveChannel removed channel: " + ChannelName)
				self.UserMonitors[idx].WaitStop()
				del self.UserMonitors[idx]
				return True
		DebugLog(DL_WARN, "TUserMonitors::_RemoveChannel channel: " + ChannelName + " not found")
		return False
		
	def JoinChannel(self, ChannelName):
		if not self.TryLock():
			return False
		um = self._JoinChannel(ChannelName)
		self.Unlock()
		return um

	def _JoinChannel(self, ChannelName):
		um = self._FindChannel(ChannelName)
		if um == False:
			um = self._Add()
		um.Joined(ChannelName)
		return um		
		
	def PartChannel(self, ChannelName, Reason):
		if not self.TryLock():
			return False
		um = self._FindChannel(ChannelName)
		if um != False:
			um.Parted(Reason)
		self.Unlock()
		return um
	
	def GetChannelNames(self, IncludeJoined, IncludeUnjoined):
		if not self.TryLock():
			return False
		ChanNameList = self._GetChannelNames(IncludeJoined, IncludeUnjoined)
		self.Unlock()
		return ChanNameList
		
	def _GetChannelNames(self, IncludeJoined, IncludeUnjoined):
		ChanNameList = []
		for UserMonitor in self.UserMonitors:
			if UserMonitor.ChannelName != "(new object)":
				if UserMonitor.ChannelJoined and IncludeJoined:
					ChanNameList.append(UserMonitor.ChannelName)
				elif UserMonitor.ChannelJoined == False and IncludeUnjoined:
					ChanNameList.append(UserMonitor.ChannelName)
		return ChanNameList

	def CheckMonitorHeartBeats(self, OldTime):
		if not self.TryLock():
			return False
		r = self._CheckMonitorHeartBeats(OldTime)
		self.Unlock()
		return r
		
	def _CheckMonitorHeartBeats(self, OldTime):
		global DogeSoaker, NotifyTipThread, TextHandler
		ProblemFound = True # force to test once
		while ProblemFound:
			ProblemFound = False
			for UserMonitor in self.UserMonitors:
				if UserMonitor.HeartbeatIsOlder(OldTime):
					ChannelName = UserMonitor.ChannelName
					DebugLog(DL_ERROR, "UserMonitors::CheckMonitorHeartBeats user monitor thread hung for channel: " + ChannelName + ", heartbeat age: " + str(UserMonitor.HeartbeatAge()))
					DogeSoaker.DebugDump()
					if self.RecoverMonitorCount > 3:
						DebugLog(DL_ERROR, "UserMonitors::CheckMonitorHeartBeats recovery attempts exhausted, giving up")
						return False # could not recover
					self._RemoveChannel(ChannelName)
					NotifyTipThread.AddNotify("Doge_Soaker", ChannelName, TextHandler.GetTextChan("HEART_BEAT_CHAN_WARNING", ChannelName))
					self.RecoverMonitorCount += 1
					ProblemFound = True
					break				
		return True		
		
	def MainLoop(self):
		time.sleep(0.001)
		self.Active = True
		if self.Terminate: 
			return # test once, before try block to prevent Unlock
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG UserMonitors thread started")
		while not self.Terminate:
			if not self.TryLock():
				return False
			try: # try/finally trap for Terminating/Unlock'ing/DeReference'ing
				ExpiredFound = True # force to test once
				while ExpiredFound:
					ExpiredFound = False
					OldestTime = time.time() - self.DeadChannelTimeout
					for UserMonitor in self.UserMonitors:
						if UserMonitor.ChannelJoined == False and UserMonitor.LastChannelLeft <= OldestTime:
							DebugLog(DL_INFO, "UserMonitors monitor timed out for channel: " + UserMonitor.ChannelName + ", Age: " + str(time.time() - UserMonitor.LastChannelLeft) + " seconds, last part reason: " + UserMonitor.ChannelLeftReason)
							self._RemoveChannel(UserMonitor.ChannelName)
							ExpiredFound = True
							break # start itteration over
			except:
				DebugLog(DL_ERROR, "TUserMonitors exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
			finally:
				self.Active = False
				self.TouchHeartbeatLocked()
				self.Unlock()
			if self.Terminate: 
				return
			self.SafeSleep(self.Delay)
			if self.CrashTest == True:
				raise Exception("TUserMonitors Crash Test Activated!")
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG UserMonitors thread done")
			
#######################################################
###                 TIRC                            ###
#######################################################

class TIRC:
	def __init__(self):
			self.Nick = False
			
	def GetNick(self):
		global C_XChat_Plugin, DebugLogger
		if C_XChat_Plugin:
			global xchat
			
			if not xchat:
				Msg = "TIRC::GetNick validate xchat object failed"
				DebugLog(DL_ERROR, Msg)
				print(Msg)
				exit(1)
			Context = xchat.get_context()
			if not Context:
				Msg = "TIRC::GetNick validate Context failed"
				if self.Nick == False:
					Msg = Msg + ", quiting"
					DebugLog(DL_ERROR, Msg)
					print(Msg)
					exit(1)				
				else:
					Msg = Msg + ", returning previous nick: " + self.Nick
					DebugLog(DL_ERROR, Msg)
					print(Msg)
			else: # Context valid
				self.Nick = Context.get_info("nick")
			return self.Nick
		else:
			global embirc
			try:
				Nick = embirc.GetNick()
			except:
				DebugLog(DL_ERROR, "TIRC GetNick exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
			print("TIRC::GetNick Nick: " + Nick)
			if DebugLogger:
				DebugLog(DL_INFO, "TIRC::GetNick Nick: " + Nick)
			return  Nick
	
	def ChanMsg(self, ChannelName, Msg):
		global C_XChat_Plugin
		if C_XChat_Plugin:
			global xchat
			xchat.command("MSG " + ChannelName + " " + Msg)
			#Context.command("say " + Msg)
		else:
			global embirc
			try:
				embirc.ChanMsg(ChannelName, Msg)
			except:
				DebugLog(DL_ERROR, "TIRC ChanMsg exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
	
	def PM(self, User, Msg):
		global C_XChat_Plugin, DEBUG_Echo_PM
		if C_XChat_Plugin:
			global xchat, DEBUG_Echo_PM
			if DEBUG_Echo_PM:
				DebugLog(DL_INFO, "IRC.PM(" + User + "): '" + Msg + "'")
			xchat.command("MSG " + User + " " + Msg)
		else:
			global embirc
			if DEBUG_Echo_PM:
				DebugLog(DL_INFO, "IRC.PM(" + User + "): '" + Msg + "'")
			try:
				embirc.PrivMsg(User, Msg)
			except:
				DebugLog(DL_ERROR, "TIRC PM exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
	
	def Command(self, Msg):
		global C_XChat_Plugin
		if C_XChat_Plugin:
			global DEBUG_Echo_CMD
			if DEBUG_Echo_CMD:
				DebugLog(DL_INFO, "IRC.CMD: '" + Msg + "'")
			xchat.command(Msg)
		else:
			global embirc
			try:
				embirc.Command(Msg)
			except:
				DebugLog(DL_ERROR, "TIRC Command exception:")
				DebugLog(DL_ERROR, traceback.format_exc())

	def Join(self, ChannelName):
		global NotifyCommandThread
		NotifyCommandThread.AddCommand("JOIN " + ChannelName)

	def Part(self, ChannelName, Msg = ""):
		global NotifyCommandThread
		if Msg != "":
			Msg = " " + Msg
		NotifyCommandThread.AddCommand("PART " + ChannelName + Msg)
	
	def ReloadPlugin(self):
		global C_XChat_Plugin
		if C_XChat_Plugin:
			PluginFullPath = os.path.abspath(inspect.getsourcefile(XChat_DS_Reload))
			PluginFileName = os.path.basename(PluginFullPath)
			self.Command("timer 5.0 unload " + PluginFileName)
			self.Command("timer 15.0 load " + PluginFullPath)
		else:
			global embirc
			try:
				embirc.ClientCommand("reload")
			except:
				DebugLog(DL_ERROR, "TIRC ReloadPlugin exception:")
				DebugLog(DL_ERROR, traceback.format_exc())

	def Reconnect(self):
		global C_XChat_Plugin
		if C_XChat_Plugin:
			global NotifyCommandThread
			NotifyCommandThread.AddCommand("RECONNECT")
		else:
			global embirc
			try:
				embirc.ClientCommand("reconnect")
			except:
				DebugLog(DL_ERROR, "TIRC Reconnect exception:")
				DebugLog(DL_ERROR, traceback.format_exc())

	def SetRawLog(self, mode):
		global C_XChat_Plugin
		if C_XChat_Plugin == False:
			global embirc
			try:
				msg = "rawlog "
				if mode == True:
					msg += "on"
				else:
					msg += "off"
				embirc.ClientCommand(msg)
			except:
				DebugLog(DL_ERROR, "TIRC Reconnect exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
	
	def CurrentContext(self):
		global xchat
		return xchat.get_context()
	
	def FindContext(self, ChannelName):
		global xchat
		return xchat.find_context(channel=ChannelName)
		
	def GetContextChannelName(self, Context):
		if not Context:
			Msg = "TIRC::GetNick validate Context failed"
			DebugLog(DL_ERROR, Msg)
			print(Msg)
			return None
		return Context.get_info("channel")

#######################################################
###                 TDogeSoaker                     ###
#######################################################

class TDogeSoaker(TThread):
	def __init__(self, DataDir):
		global C_Heartbeat_Timeout, C_HeartbeatCheck_Delay
		TThread.__init__(self, self.MainLoop)
		self.Active = False
		self.Delay = C_HeartbeatCheck_Delay # seconds
		self.DataDirectory = DataDir
		self.AutoSave = True
		self.BotNick = False
		self.NextChannelNotification = 0.0
		self.HeartbeatExpireAge = C_Heartbeat_Timeout # seconds 300.0 # seconds (5 minutes)
		self.InitDefaults()

	def InitConfig(self, LoadDefaultsOnFail):
		global ConfigFile, TextHandler, C_DEBUG_Simulate, C_TipBotNick, C_Soak_Channel_Default, C_Operator_Channel, C_Tip_Confirmation_Resending, C_Enable_Twitter_Status, C_Enable_Diaspora_Status

		if TextHandler.Init() == False:
			DebugLog(DL_ERROR, "TDogeSoaker::Init TextHandler Init failed")

		ConfigFile.Init(C_Data_Directory)
		if not ConfigFile.ConfigExists():
			if LoadDefaultsOnFail:
				DebugLog(DL_INFO, "TDogeSoaker::Init config file doesn't exist, loading defaults")
				self.InitDefaults()
			else:
				DebugLog(DL_INFO, "TDogeSoaker::Init config file doesn't exist, keeping current config")
		else:
			if not ConfigFile.Load():
				if LoadDefaultsOnFail:
					DebugLog(DL_ERROR, "TDogeSoaker::Init Config Load failed, loading defaults and disabling AutoSave")
					self.AutoSave = False
					self.InitDefaults()
				else:
					DebugLog(DL_ERROR, "TDogeSoaker::Init Config Load failed, loading defaults and disabling AutoSave")
					self.AutoSave = False
			else:
				DebugLog(DL_DEBUG, "TDogeSoaker::Init config file loaded")
		if C_DEBUG_Simulate:
			DebugLog(DL_DEBUG, "TDogeSoaker::Init config simulation mode detected")
			C_Tip_Confirmation_Resending = True # test tip confirmation tracking thoroughly when simulating, but doesn't work for Doger in practice.
			C_Enable_Twitter_Status = False
			C_Enable_Diaspora_Status = False
			self.AutoSave = False
			if len(OperatorList.Nicks) == 0:
				Msg = "ERROR: Simulation Mode requires specified operator nick in C_OperatorNicks"
				DebugLog(DL_ERROR, Msg)
				print(Msg)
				exit(1)
			C_Soak_Channel_Default = C_Operator_Channel
			C_TipBotNick = OperatorList.Nicks[0]
			TipbotList.AddNick(C_TipBotNick)
		else:
			DebugLog(DL_DEBUG, "TDogeSoaker::Init config live mode detected")
		
	def InitDefaults(self):
		global C_OperatorNicks, OperatorList
		global C_Tipbot_Nick_List, TipbotList
		global C_Banned_Nick_List, IgnoreList, IgnoreHostList
		global C_Bot_Ignore_Nick_List
		
		OperatorList.Clear()
		TipbotList.Clear()
		IgnoreList.Clear()
		IgnoreHostList.Clear()
		
		for OperatorNick in C_OperatorNicks:
			OperatorList.AddNick(OperatorNick)

		if not len(C_Tipbot_Nick_List):
			Msg = "Error: Not tipbots specified in C_Tipbot_Nick_List"
			DebugLog(DL_ERROR, Msg)
			print(Msg)
		else:
			for Nick in C_Tipbot_Nick_List:
				IgnoreList.AddNick(Nick)
				TipbotList.AddNick(Nick)

		if len(C_Banned_Nick_List):
			for Nick in C_Banned_Nick_List:
				IgnoreList.AddNick(Nick)
				
		if len(C_Bot_Ignore_Nick_List):
			for Nick in C_Bot_Ignore_Nick_List:
				IgnoreList.AddNick(Nick)
		
	def SaveConfig(self):
		global ConfigFile
		if not ConfigFile.Save():
			DebugLog(DL_WARN, "TDogeSoaker::SaveConfig Config Save failed")
			return False
		DebugLog(DL_DEBUG, "TDogeSoaker::SaveConfig config file saved")
		return True
		
	def Unload(self):
		if self.AutoSave:
			if not self.SaveConfig():
				DebugLog(DL_WARN, "TDogeSoaker::Unload SaveConfig failed")
			else:
				DebugLog(DL_DEBUG, "TDogeSoaker::Unload config AutoSaved")
		return self.WaitStop()
			
	def GetBotNick(self):
		if self.BotNick == False:
			self.BotNick = IRC.GetNick()
		return self.BotNick

	def HandleNickChanged(self, OldNick, Host, NewNick):
		global C_Data_Directory, DebugLogger, AuditLog, TipoutThread
		# '*' is a special signal from the client to change the BotNick, used for nick recovery.
		if OldNick == "*" or self.BotNick == OldNick:
			DebugLog(DL_INFO, "DogeSoaker::HandleNickChanged bot nick changed " + OldNick + " -> " + NewNick + " Host: " + Host)
			self.BotNick = NewNick
			if self.AutoSave:
				if not self.SaveConfig():
					DebugLog(DL_WARN, "TDogeSoaker::HandleNickChanged SaveConfig failed")
				else:
					DebugLog(DL_DEBUG, "TDogeSoaker::HandleNickChanged config AutoSaved")
			DebugLogger.Init(C_Data_Directory)
			AuditLog.Init(C_Data_Directory)
			self.InitConfig(False) # LoadDefaultsOnFail
		else:
			DebugLog(DL_DEBUG, "DogeSoaker::HandleNickChanged OldNick: " + OldNick + ", Host: " + Host + ", NewNick: " + NewNick)
			TipoutThread.ChangeAuthUserNick(OldNick, NewNick)
		
	def GetModuleVersion(self):	
		global __module_name__, __module_version__, __module_description__, sys
		# pylint: disable=W1401
		Msg = "\0034" + __module_name__ + " " + __module_version__ + " - " + __module_description__ + "\0034\n"
		# pylint: enable=W1401
		Msg = Msg + "  Python: " + sys.version
		return Msg
		
	def GetModuleStatus(self):
		global C_DEBUG_Simulate, C_Operator_Channel, C_Soak_Channel_Default, C_TipBotNick, C_Data_Directory, DebugLogger, AuditLog
		Msg = ""
		if C_DEBUG_Simulate:
			Msg = Msg + "!! Simulation Mode !!\n"
		else:
			Msg = Msg + "Live Mode\n"
		Msg = Msg + FormatNameValue("soaker bot", 		self.GetBotNick(), 			18) + "\n"
		Msg = Msg + FormatNameValue("channel",    		C_Soak_Channel_Default, 	18) + "\n"
		if not C_DEBUG_Simulate:
			Msg = Msg + FormatNameValue("ops channel",   C_Operator_Channel, 		18) + "\n"
		Msg = Msg + FormatNameValue("tipbot",     		C_TipBotNick, 				18) + "\n"
		Msg = Msg + FormatNameValue("data dir",  		C_Data_Directory, 			18) + "\n"
		Msg = Msg + FormatNameValue("debug log file",  	DebugLogger.LogFileName,	18) + "\n"
		Msg = Msg + FormatNameValue("audit log file",  	AuditLog.LogFileName,		18) + "\n"
		return Msg
		
	def GetModuleHelpMessage(self):
		global C_Module_HelpMessage
		return "\n".join(C_Module_HelpMessage)
		
	def LogModuleInfoMessage(self, Message): 
		'''
		Adds to debug log message as DL_INFO 
		and makes sure it is printed to the client 
		even if C_DEBUG_Print is disalbed.
		'''
		global __module_name__, C_DEBUG_Print
		Message = __module_name__ + " " + Message
		DebugLog(DL_INFO, Message)
		if not C_DEBUG_Print:
			print(Message)
		
	def IsChannelNotificationReady(self):
		if self.NextChannelNotification == 0.0 or time.time() > self.NextChannelNotification:
			return True
		else:
			return False
		
	def TouchChannelNotification(self, Delay):
		self.NextChannelNotification = time.time() + Delay
		
	def GetActiveUserMessage(self, ChannelName):
		global IgnoreList, UserMonitors, TextHandler
		UserMonitor = UserMonitors.FindChannel(ChannelName)
		if UserMonitor == False:
			return TextHandler.GetTextChan("ACTIVE_USERS_FAILED", ChannelName).format(ChannelName)
		else:
			AllUserNames = UserMonitor.AllUserNames()
			ActiveUserNames = []
			for AllUserName in AllUserNames:
				if IgnoreList.ContainsNick(AllUserName) == False:
					ActiveUserNames.append(AllUserName)
		return TextHandler.GetTextChan("ACTIVE_USERS", ChannelName).format(len(ActiveUserNames))
	
	def GetBarkMessage(self, ChannelName):
		global TipoutThread, NotifyTipThread, TextHandler
		Msg = ""
		if not TipoutThread.SoakEnabled:
			Msg = TextHandler.GetTextChan("STATUS_DISABLED", ChannelName)
		elif TipoutThread.CanSoak():
			Msg = TextHandler.GetTextChan("STATUS_READY", ChannelName)
		else:
			if TipoutThread.IsBusy() and NotifyTipThread.Count() != 0:
				Eta = NotifyTipThread.Count() * 10
				Msg = TextHandler.GetTextChan("STATUS_BUSYTIPPING", ChannelName).format(Eta)
			else:
				Msg = TextHandler.GetTextChan("STATUS_BUSY", ChannelName)
		return Msg		
		
	def HandlePrivMsg(self, FromUser, ToUser, Message):
		global PrivMsgHandlerThread
		#DebugLog(DL_DEBUG, "DogeSoakerHandlePrivMsg received from " + FromUser + " to " + ToUser + ": " + Message + "  PrivMsgHandlerThread count: " + str(len(PrivMsgHandlerThread.PrvMsgs)))
		PrivMsgHandlerThread.AddPrvMsg(FromUser, ToUser, Message)
		
	def Start(self):
		if not self.TryLock():
			return False
		RetValue = self._Start(False) # SkipSelf
		self.Unlock()
		return RetValue			
	
	def _Start(self, SkipSelf):
		global NotifyTipThread, NotifyCommandThread, NotifyMessageThread, PrivMsgHandlerThread, UserMonitors, TipoutThread, Events
		DebugLog(DL_DEBUG, "DogeSoaker::Start starting NotifyTipThread")
		NotifyTipThread.Start()
		DebugLog(DL_DEBUG, "DogeSoaker::Start starting NotifyCommandThread")
		NotifyCommandThread.Start()
		DebugLog(DL_DEBUG, "DogeSoaker::Start starting NotifyMessageThread")
		NotifyMessageThread.Start()
		DebugLog(DL_DEBUG, "DogeSoaker::Start starting PrivMsgHandlerThread")
		PrivMsgHandlerThread.Start()
		DebugLog(DL_DEBUG, "DogeSoaker::Start starting UserMonitors")
		UserMonitors.Start()
		DebugLog(DL_DEBUG, "DogeSoaker::Start starting TipoutThread")
		TipoutThread.Start()
		DebugLog(DL_DEBUG, "DogeSoaker::Start starting Events")
		Events.Start()
		#TestModule is only started on demand
		if not SkipSelf:
			DebugLog(DL_DEBUG, "DogeSoaker::Start starting self thread")
			TThread.Start(self) # TDogeSoaker's Thread
		Sleep(1)
		self.LogModuleInfoMessage("started")
		DebugLog(DL_DEBUG, "DogeSoaker::Start Done")
		return True
	
	def Stop(self):
		if not self.TryLock():
			return False
		RetValue = self._Stop()
		self.Unlock()
		return RetValue			

	def _Stop(self):
		global NotifyTipThread, NotifyCommandThread, NotifyMessageThread, PrivMsgHandlerThread, UserMonitors, TipoutThread, TestModule, Events
		NotifyTipThread.Stop()
		NotifyCommandThread.Stop()
		NotifyMessageThread.Stop()
		PrivMsgHandlerThread.Stop()
		UserMonitors.Stop()
		TipoutThread.Stop()
		Events.Stop()
		TestModule.Stop()
		TThread.Stop(self) # TDogeSoaker's Thread
		Sleep(1)
		self.LogModuleInfoMessage("stopped")
		return True

	def WaitStop(self):
		if not self.TryLock():
			return False
		RetValue = self._WaitStop(False, False) # SkipTestModule, SkipSelf
		self.Unlock()
		return RetValue			

	def _WaitStop(self, SkipTestModule, SkipSelf):
		global NotifyTipThread, NotifyCommandThread, NotifyMessageThread, PrivMsgHandlerThread, UserMonitors, TipoutThread, TestModule, Events
		if SkipTestModule:
			DebugLog(DL_DEBUG, "DogeSoaker::WaitStop skiping TestModule...")
		else:
			DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on TestModule...")
			TestModule.WaitStop()
		DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on Events...")
		Events.WaitStop()
		DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on NotifyTipThread...")
		NotifyTipThread.WaitStop()
		DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on NotifyCommandThread...")
		NotifyCommandThread.WaitStop()
		DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on NotifyMessageThread...")
		NotifyMessageThread.WaitStop()
		DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on PrivMsgHandlerThread...")
		PrivMsgHandlerThread.WaitStop()
		DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on UserMonitors...")
		UserMonitors.WaitStop()
		DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on TipoutThread...")
		TipoutThread.WaitStop()
		if not SkipSelf:
			DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on DogeSoaker thread...")
			TThread.WaitStop(self) # TDogeSoaker's Thread
		DebugLog(DL_DEBUG, "DogeSoaker::WaitStop waiting on finish...")
		Sleep(1)
		self.LogModuleInfoMessage("wait-stopped")
		DebugLog(DL_DEBUG, "DogeSoaker::WaitStop DONE")
		return True

	def RestartFromTest(self): # don't restart testmodule or loop gets locked
		DebugLog(DL_DEBUG, "DogeSoaker::RestartFromTest restarting...")
		if not self.TryLock():
			return False
		RetValue = self._Restart(True, False) # SkipTestModule, SkipSelf
		self.Unlock()
		return RetValue

	def RestartNotSelf(self): # don't restart DogeSoaker
		DebugLog(DL_DEBUG, "DogeSoaker::RestartFromTest restarting...")
		if not self.TryLock():
			return False
		RetValue = self._Restart(False, True) # SkipTestModule, SkipSelf
		self.Unlock()
		return RetValue

	def Restart(self): 
		DebugLog(DL_DEBUG, "DogeSoaker::Restart restarting...")
		if not self.TryLock():
			return False
		RetValue = self._Restart(False, False) # SkipTestModule
		self.Unlock()
		return RetValue	

	def _Restart(self, SkipTestModule, SkipSelf):
		if not self._WaitStop(SkipTestModule, SkipSelf):
			DebugLog(DL_DEBUG, "DogeSoaker:Restart WaitStop failed")
			return False
		Sleep(3)
		if not self._Start(SkipSelf):
			DebugLog(DL_DEBUG, "DogeSoaker:Restart Start failed")
			return False
		Sleep(2)
		self.LogModuleInfoMessage("restarted")
		return True
		
	def Pause(self):
		TipoutThread.Pause()
		self.LogModuleInfoMessage("paused")

	def Resume(self):
		TipoutThread.Resume()
		self.LogModuleInfoMessage("resumed")
		
	def Reset(self):
		global UserMonitors, TipoutThread
		UserMonitors.Reset()
		TipoutThread.Reset()
		self.LogModuleInfoMessage("reset")
	
	def Resend(self):
		global TipoutThread
		TipoutThread.Resend()
		
	def HandleWhoisAuth(self, CurrNick, AuthNick):
		global TipoutThread
		TipoutThread.UpdateWhoIsAuth(CurrNick, AuthNick)
		
	def HandleWhoisNameLine(self, Nick, HostUser, Host, RealName):
		global TipoutThread
		TipoutThread.UpdateWhoNameLine(Nick, HostUser, Host, RealName)
		
	def HandleWhoisEnd(self, Nick):
		global TipoutThread
		TipoutThread.UpdateWhoIsEnd(Nick)

	def HandleJoin(self, Nick, Host, ChannelName):
		global TipoutThread, NotifyCommandThread, UserMonitors
		if Nick == self.BotNick:
			DebugLog(DL_DEBUG, "DogeSoaker::HandleJoin Bot has joined a channel, preparing UserMonitor. Channel: " + ChannelName + ", BotNick: " + Nick + ", Host: " + Host)
			UserMonitors.JoinChannel(ChannelName)
		else:
			#DebugLog(DL_DEBUG, "DogeSoaker::HandleJoin User joined a channel, checking for auth. Nick: " + Nick + ", Host: " + Host + ", Channel: " + ChannelName)			
			AuthUser = TipoutThread.FindAuthUserByNick(Nick)
			if AuthUser == False:
				NotifyCommandThread.AddCommand("WHOIS " + Nick)
				
	def HandlePart(self, Nick, Host, ChannelName, Reason):
		global UserMonitors
		if Nick == self.BotNick:
			DebugLog(DL_DEBUG, "DogeSoaker::HandlePart Bot has parted a channel. Channel: " + ChannelName + ", Nick: " + Nick + ", Host: " + Host + ", Reason: " + Reason)
			UserMonitors.PartChannel(ChannelName, Reason)
		else:
			DebugLog(DL_DEBUG, "DogeSoaker::HandlePart a user has parted a channel. Channel: " + ChannelName + ", Nick: " + Nick + ", Host: " + Host + ", Reason: " + Reason)
			TipoutThread.RemoveAuthNick(Nick) # we can't know the user is still identified if not in channel, so scrub nick from auth cache
			UserMonitor = UserMonitors.FindChannel(ChannelName)
			if UserMonitor == False:
				DebugLog(DL_DEBUG, "DogeSoaker::HandlePart channel not found: " +  ChannelName + ", handling kick of nick: " + Nick)
			else:
				UserMonitor.ForgetUser(Nick)

	def HandleQuit(self, Nick, Host, Reason):
		global TipoutThread
		if Nick == self.BotNick:
			DebugLog(DL_DEBUG, "DogeSoaker::HandleQuit Bot has quit! Nick: " + Nick + ", Host: " + Host + ", Reason: " + Reason)
		else:
			DebugLog(DL_DEBUG, "DogeSoaker::HandleQuit Nick: " + Nick + ", Host: " + Host + ", Reason: " + Reason)
			TipoutThread.RemoveAuthNick(Nick)
			UserMonitors.ForgetUser(Nick)
		
	def HandleKick(self, Nick, ChannelName, Reason, KickerNick):
		global UserMonitors, TipoutThread
		if Nick == self.BotNick:
			DebugLog(DL_DEBUG, "DogeSoaker::HandleKick Bot has been kicked! Nick: " + Nick + ", Channel: " + ChannelName + ", Reason: " + Reason + ", Kicker: " + KickerNick)
			UserMonitors.PartChannel(ChannelName, "(by " + KickerNick + ") " + Reason)
		else:
			DebugLog(DL_DEBUG, "DogeSoaker::HandleKick  Nick: " + Nick + ", Channel: " + ChannelName + ", Reason: " + Reason + ", Kicker: " + KickerNick)
			TipoutThread.RemoveAuthNick(Nick)
			UserMonitor = UserMonitors.FindChannel(ChannelName)
			if UserMonitor == False:
				DebugLog(DL_DEBUG, "DogeSoaker::HandleKick channel not found: " +  ChannelName + ", handling kick of nick: " + Nick)
			else:
				UserMonitor.ForgetUser(Nick)
	
	def JoinAllChannels(self):
		global UserMonitors, IRC
		ChanNameList = UserMonitors.GetChannelNames(False, True) # IncludeJoined, IncludeUnjoined
		for ChanName in ChanNameList:
			IRC.Join(ChanName)
		return ", ".join(ChanNameList)
		
	def HandleChannelMessage(self, ChannelName, UserName, Message):
		global UserMonitors, NextChannelNotification, OperatorList, TipoutThread, C_DEBUG_Simulate, DEBUG_Echo_ChanMsg, DogeSoaker, Long_Name, IRC, TextHandler
		try:
			UserMonitor = UserMonitors.JoinAddChannel(ChannelName)
			if UserMonitor == False:
				print("UserMonitors.FindAddChannel failed")
				DebugLog(DL_ERROR, "Error: DogeSoaker::HandleChannelMessage UserMonitors.JoinAddChannel failed ChanMsg channel: " + ChannelName + " User: '" + UserName + "' Message: " + Message)
				return
			if DEBUG_Echo_ChanMsg == True:
				DebugLog(DL_DEBUG, "ChanMsg Channel: " + ChannelName + " User: '" + UserName + "' Message: " + Message)
			if UserName == "":
				DebugLog(DL_WARN, "Warn: DogeSoaker::HandleChannelMessage empty user name detected. channel: " + ChannelName + " User: '" + UserName + "' Message: " + Message)
				return
			if CmdMatch(Message, "!help"):
				if self.IsChannelNotificationReady():
					if TipoutThread.SoakEnabled: # ignore !active when disabled
						IRC.ChanMsg(ChannelName, TextHandler.GetTextChan("CHAN_HELP_GENERIC", ChannelName))
						self.TouchChannelNotification(10.0)
			elif CmdMatch(Message, "!dsbal"):
				if self.IsChannelNotificationReady():
					if TipoutThread.SoakEnabled: # ignore when disabled
						IRC.ChanMsg(ChannelName, "!balance")
						self.TouchChannelNotification(20.0)
			elif CmdMatch(Message, "!dshelp"):
				if self.IsChannelNotificationReady():
					if TipoutThread.SoakEnabled: # ignore when disabled
						if len(OperatorList.Nicks):
							OpNick = OperatorList.Nicks[0]
						else:
							OpNick = TextHandler.GetTextChan("CHAN_HELP_UNKNOWNOP", ChannelName)
						Msg = TextHandler.GetTextChan("CHAN_HELP", ChannelName).format(OpNick)
						IRC.ChanMsg(ChannelName, Msg)
						self.TouchChannelNotification(10.0)
			elif CmdMatch(Message, "!tos"):
				if self.IsChannelNotificationReady():
					if TipoutThread.SoakEnabled: # ignore !active when disabled
						IRC.ChanMsg(ChannelName, TextHandler.GetTextChan("CHAN_TOS", ChannelName))
						self.TouchChannelNotification(10.0)
			elif CmdMatch(Message, "!bark"):
				if self.IsChannelNotificationReady():
					Msg = self.GetBarkMessage(ChannelName)
					IRC.ChanMsg(ChannelName, Msg)
					self.TouchChannelNotification(12.0)
			elif CmdMatch(Message, "!active"):
				if self.IsChannelNotificationReady():
					if TipoutThread.SoakEnabled: # ignore !active when disabled
						#DebugLog(DL_DEBUG, "Got active channel command")
						Msg = UserName + ", " + self.GetActiveUserMessage(ChannelName)
						IRC.ChanMsg(ChannelName, Msg)
						self.TouchChannelNotification(10.0)
			elif Message.startswith("!") and OperatorList.ContainsNick(UserName): # operator commands
				if CmdMatch(Message, "!dssoak"):
					MessageWords = Message.split()
					dsSoakChannelName = ChannelName
					Value = -1
					if len(MessageWords) == 3 and isInt(MessageWords[2]):
						Value = int(MessageWords[2])
						dsSoakChannelName = MessageWords[1]
					elif len(MessageWords) == 2 and isInt(MessageWords[1]):
						Value = int(MessageWords[1])
					if Value == -1:
						IRC.ChanMsg(ChannelName, "Required format: !dssoak <channel> <amount>")
					else:
						UserMonitor = UserMonitors.FindChannel(dsSoakChannelName)
						if UserMonitor == False:
							IRC.ChanMsg(ChannelName, "Failed to find channel " + dsSoakChannelName + ".")
						else:
							TipID = "[-dssoak-]"
							DebugLog(DL_INFO, "DSSoak request channel: " + dsSoakChannelName + ", value: " + str(Value) + ", tip id: " + TipID)
							UserMonitor.Soak(dsSoakChannelName, UserName, Value, TipID, True) # True - From PM, so it shows as Anonymous
							if not TipoutThread.ConfirmedTipID(TipID):
								DebugLog(DL_WARN, "!dssoak ConfirmedTipID didn't find new PM tipout to insta-confirm TipID: " + TipID)
				elif CmdMatch(Message, "!dsver"):
					IRC.ChanMsg(ChannelName, Long_Name)			
				elif CmdMatch(Message, "!users"):
					DebugLog(DL_INFO, "Got user list request")
					MessageWords = Message.split()
					ReplyMsg = ""
					AllUsers = []
					if len(MessageWords) > 1:
						UserMonitor = UserMonitors.FindChannel(MessageWords[1])
						ReplyMsg = "(" + MessageWords[1] + ") "
					if UserMonitor == False:
						ReplyMsg = "(!users): Channel " + MessageWords[1] + " not found"
					else:
						AllUsers = UserMonitor.AllUsers()
						ReplyMsg = str(len(AllUsers)) + " active user(s): "
					First = True
					for User in AllUsers:
						if First:
							First = False
						else:
							ReplyMsg = ReplyMsg + ", "
						ReplyMsg = ReplyMsg + User.Name + "(" + ("%0.1f" % UserMonitor.RatePerMin(User.Rate)) + ")"
					DebugLog(DL_INFO, "Reply: " + ReplyMsg)
					IRC.ChanMsg(ChannelName, ReplyMsg)
				elif CmdMatch(Message, "!test"):
					MessageWords = Message.split()
					SkipTo = False
					if len(MessageWords) > 1 and isInt(MessageWords[1]):
						SkipTo = int(MessageWords[1])
					IRC.ChanMsg(ChannelName, "Starting testing module (Doger)...")
					DogeSoaker.Test(ChannelName, SkipTo)
				elif CmdMatch(Message, "!cancel"):
					DogeSoaker.StopTest()
				elif Message.startswith("!tip ") or Message.startswith("!mtip "): # specially allowed active command (for operator)
					UserMonitor.AddUpdateUser(UserName)
			elif Message.startswith("Such "):
				if TipbotList.ContainsNick(UserName) or OperatorList.ContainsNick(UserName): # Tib bot channel messages, or operator for testing
					MessageWords = Message.split()
					# Such User1 tipped much Ɖ10 to User2! (to claim /msg Doger help) [e61bc8b9]
					if len(MessageWords) < 7:
						DebugLog(DL_INFO, "Bad Doger channel tip. Args " + str(len(MessageWords)) + ": " + " ".join(MessageWords))
					else:
						SendUser = MessageWords[1]
						ValueString = MessageWords[4]
						RecUser = MessageWords[6]
						Value = Dogecode(ValueString, -1)
						if RecUser[-1] == '!':
							RecUser = RecUser[0:-1]
						if Value == -1:
							DebugLog(DL_INFO, "Bad Doger channel tip value " + str(len(MessageWords)) + ": " + ConStringList(MessageWords) + " * value: '" + ValueString + "'")
						else:
							#DebugLog(DL_INFO, "Channel Doger Tip detected to: " + RecUser + " from: " + SendUser + " for value: " + ValueString + " Bot nick: " + self.GetBotNick())
							if self.GetBotNick() != False and RecUser.lower() == self.GetBotNick().lower(): # case insensitive
								SendUser = MessageWords[1]
								TipID = MessageWords[-1]
								if len(TipID) != 10:
									DebugLog(DL_INFO, "Cannel Doger Tip Detected to bot with bad id length TipID: " + TipID)
								elif TipID[0] != "[" or TipID[-1] != "]":
									DebugLog(DL_INFO, "Cannel Doger Tip Detected to bot with bad id TipID: " + TipID)
								else:
									if not UserMonitor.RequestSoak(ChannelName, SendUser, Value, TipID, False):
										DebugLog(DL_WARN, "TDogeSoaker:: HandleChannelMessage UserMontiro RequestSoak failed")
			elif Message.startswith("!tip ") or Message.startswith("!mtip "): # specially allowed active commands
				UserMonitor.AddUpdateUser(UserName)
			elif not Message.startswith("!") and not Message.startswith("."): # ignore commands
				if len(Message) > 3:
					UserMonitor.AddUpdateUser(UserName)
		except:
			DebugLog(DL_ERROR, "TDogeSoaker::HandleChannelMessage exception:")
			DebugLog(DL_ERROR, traceback.format_exc())
			
	
	def Test(self, ChannelName, Step = False):
		TestModule.StartTest(ChannelName, Step)
		
	def StopTest(self):
		TestModule.StopTest()
		
	def DebugDumpListMessage(self, DebugList):
		Msg = "(" + str(len(DebugList))  + "): " 
		if len(DebugList) > 0:
			Msg += ",".join(DebugList)
		return Msg

	def MainLoop(self):
		global TestModule, Events, UserMonitors, TipoutThread, PrivMsgHandlerThread, NotifyTipThread, NotifyCommandThread, NotifyMessageThread
		time.sleep(0.001)
		self.Active     = True
		if self.Terminate: 
			return # test once, before try block to prevent Unlock
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG DogeSoaker thread started")
		while not self.Terminate:
			if not self.TryLock():
				return False
			try: # try/finally trap for Terminating/Unlock'ing
				if TestModule.Running == False:
					ProblemDetected = False
					ExpireTime = time.time() - self.HeartbeatExpireAge
					if UserMonitors.HeartbeatIsOlder(ExpireTime - UserMonitors.Delay): # UserMonitors has a huge delay cutting it close for heartbeat expiring
						DebugLog(DL_ERROR, "DogeSoaker::MainLoop UserMonitors heartbeat expired age: " + str(UserMonitors.HeartbeatAge()))
						ProblemDetected = True
					elif not UserMonitors.CheckMonitorHeartBeats(ExpireTime):
						DebugLog(DL_ERROR, "DogeSoaker::MainLoop UserMonitors CheckMonitorHeartBeats failed")
						ProblemDetected = True
						
					if TipoutThread.HeartbeatIsOlder(ExpireTime):
						DebugLog(DL_ERROR, "DogeSoaker::MainLoop TipoutThread heartbeat expired age: " + str(TipoutThread.HeartbeatAge()))
						ProblemDetected = True
					if PrivMsgHandlerThread.HeartbeatIsOlder(ExpireTime):
						DebugLog(DL_ERROR, "DogeSoaker::MainLoop PrivMsgHandlerThread heartbeat expired age: " + str(PrivMsgHandlerThread.HeartbeatAge()))
						ProblemDetected = True
					if NotifyTipThread.HeartbeatIsOlder(ExpireTime):
						DebugLog(DL_ERROR, "DogeSoaker::MainLoop NotifyTipThread heartbeat expired age: " + str(NotifyTipThread.HeartbeatAge()))
						ProblemDetected = True
					if NotifyCommandThread.HeartbeatIsOlder(ExpireTime) and TestModule.Running == False:
						DebugLog(DL_ERROR, "DogeSoaker::MainLoop NotifyCommandThread heartbeat expired age: " + str(NotifyCommandThread.HeartbeatAge()))
						ProblemDetected = True
					if NotifyMessageThread.HeartbeatIsOlder(ExpireTime):
						DebugLog(DL_ERROR, "DogeSoaker::MainLoop NotifyMessageThread heartbeat expired age: " + str(NotifyMessageThread.HeartbeatAge()))
						ProblemDetected = True
					if Events.HeartbeatIsOlder(ExpireTime):
						DebugLog(DL_ERROR, "DogeSoaker::MainLoop Events Thread heartbeat expired age: " + str(Events.HeartbeatAge()))
						ProblemDetected = True
					if ProblemDetected:
						DebugLog(DL_ERROR, "DogeSoaker::MainLoop problem detected!")
						self.DebugDump()
						DebugLog(DL_WARN, "DogeSoaker::MainLoop Attempting restart....")
						self._Restart(False, True) #  # SkipTestModule, SkipSelf
						DebugLog(DL_WARN, "DogeSoaker::MainLoop Attempting restart....DONE")			
			except:
				DebugLog(DL_ERROR, "TDogeSoaker exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
			finally:
				self.Active = False
				self.TouchHeartbeatLocked()
				self.Unlock()
			if self.Terminate: 
				return
			self.SafeSleep(self.Delay)
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG DogeSoaker thread done")

	def DebugDump(self):
		print("dumping debug")
		global UserMonitors, NotifyTipThread, NotifyMessageThread, NotifyCommandThread, PrivMsgHandlerThread, TipoutThread, Events
		DebugLog(DL_INFO, "------------------DEBUG-------------")
		DebugLog(DL_INFO, "NotifyTipThread:")
		DebugLog(DL_INFO, FormatNameValue("Thread Running",      	NotifyTipThread.Running,      			20))
		DebugLog(DL_INFO, FormatNameValue("Thread Terminate",    	NotifyTipThread.Terminate,    			20))
		DebugLog(DL_INFO, FormatNameValue("Is Busy",    			NotifyTipThread.IsBusy(),    			20))
		DebugLog(DL_INFO, FormatNameValue("Heart beat age",    		NotifyTipThread.HeartbeatAge(),    		20))
		DebugLog(DL_INFO, FormatNameValue("Notify Velocity",    	NotifyTipThread.NofifyVelocity,    		20))
		DebugLog(DL_INFO, FormatNameValue("Notify Speed",    		NotifyTipThread.NotifySpeed,    		20))
		DebugLog(DL_INFO, FormatNameValue("Notification Count",    	len(NotifyTipThread.Notifications),    	20))
		DebugLog(DL_INFO, "---")
		DebugLog(DL_INFO, "NotifyMessageThread:")
		DebugLog(DL_INFO, FormatNameValue("Thread Running",      	NotifyMessageThread.Running,      			20))
		DebugLog(DL_INFO, FormatNameValue("Thread Terminate",    	NotifyMessageThread.Terminate,    			20))
		DebugLog(DL_INFO, FormatNameValue("Is Busy",    			NotifyMessageThread.IsBusy(),    			20))
		DebugLog(DL_INFO, FormatNameValue("Heart beat age",    		NotifyMessageThread.HeartbeatAge(),    		20))
		DebugLog(DL_INFO, FormatNameValue("Notify Velocity",    	NotifyMessageThread.NofifyVelocity,    		20))
		DebugLog(DL_INFO, FormatNameValue("Notify Speed",    		NotifyMessageThread.NotifySpeed,    		20))
		DebugLog(DL_INFO, FormatNameValue("Notification Count",    	len(NotifyMessageThread.Notifications),    	20))
		DebugLog(DL_INFO, "---")
		DebugLog(DL_INFO, "NotifyCommandThread:")
		DebugLog(DL_INFO, FormatNameValue("Thread Running",      	NotifyCommandThread.Running,      			20))
		DebugLog(DL_INFO, FormatNameValue("Thread Terminate",    	NotifyCommandThread.Terminate,    			20))
		DebugLog(DL_INFO, FormatNameValue("Is Busy",    			NotifyCommandThread.IsBusy(),    			20))
		DebugLog(DL_INFO, FormatNameValue("Heart beat age",    		NotifyCommandThread.HeartbeatAge(),    		20))
		DebugLog(DL_INFO, FormatNameValue("Notify Velocity",    	NotifyCommandThread.NofifyVelocity,    		20))
		DebugLog(DL_INFO, FormatNameValue("Notify Speed",    		NotifyCommandThread.NotifySpeed,    		20))
		DebugLog(DL_INFO, FormatNameValue("Notification Count",    	len(NotifyCommandThread.Notifications),    	20))
		DebugLog(DL_INFO, "---")
		DebugLog(DL_INFO, "PrivMsgHandlerThread:")
		DebugLog(DL_INFO, FormatNameValue("Thread Running",      	PrivMsgHandlerThread.Running,      			20))
		DebugLog(DL_INFO, FormatNameValue("Thread Terminate",    	PrivMsgHandlerThread.Terminate,    			20))
		DebugLog(DL_INFO, FormatNameValue("Heart beat age",    		PrivMsgHandlerThread.HeartbeatAge(),    	20))
		DebugLog(DL_INFO, FormatNameValue("Delay",    				PrivMsgHandlerThread.Delay,    				20))
		DebugLog(DL_INFO, FormatNameValue("Msg Count",    			len(PrivMsgHandlerThread.PrvMsgs),    		20))
		DebugLog(DL_INFO, "---")
		DebugLog(DL_INFO, "EventsThread:")
		DebugLog(DL_INFO, FormatNameValue("Thread Running",      	Events.Running,      			20))
		DebugLog(DL_INFO, FormatNameValue("Thread Terminate",    	Events.Terminate,    			20))
		DebugLog(DL_INFO, FormatNameValue("Is Busy",    			Events.IsBusy(),    			20))
		DebugLog(DL_INFO, FormatNameValue("Heart beat age",    		Events.HeartbeatAge(),    		20))
		DebugLog(DL_INFO, FormatNameValue("Event Count",    		len(Events.Events),    		20))
		DebugLog(DL_INFO, "---")
		DebugLog(DL_INFO, "TipoutThread:")
		DebugLog(DL_INFO, FormatNameValue("Thread Running",      	TipoutThread.Running,      			20))
		DebugLog(DL_INFO, FormatNameValue("Thread Terminate",    	TipoutThread.Terminate,    			20))
		DebugLog(DL_INFO, FormatNameValue("Is Busy",    			TipoutThread.IsBusy(),    			20))
		DebugLog(DL_INFO, FormatNameValue("Soak Enabled",    		TipoutThread.SoakEnabled,    		20))
		DebugLog(DL_INFO, FormatNameValue("Heart beat age",    		TipoutThread.HeartbeatAge(),    	20))
		DebugLog(DL_INFO, FormatNameValue("Last soak age",    		(time.time() - TipoutThread.LastSoakTime),    		20))
		DebugLog(DL_INFO, FormatNameValue("soak wait",    			int(math.floor(TipoutThread.SoakWaitLeft())),    	20))
		DebugLog(DL_INFO, FormatNameValue("Step Speed",    			TipoutThread.StepSpeed,    							20))
		DebugLog(DL_INFO, FormatNameValue("Soak Delay",    			TipoutThread.SoakDelay,    							20))
		DebugLog(DL_INFO, FormatNameValue("Tipout Count",    		len(TipoutThread.Tipouts),    						20))
		DebugLog(DL_INFO, FormatNameValue("Tipback Count",    		len(TipoutThread.Tipbacks),    						20))
		DebugLog(DL_INFO, FormatNameValue("Last Balance",    		TipoutThread.LastBalance,    						20))
		for Tipout in TipoutThread.Tipouts:
			DebugLog(DL_INFO, "   Tipout:")
			DebugLog(DL_INFO, FormatNameValue("Type",    			Tipout.Type,    			30))
			DebugLog(DL_INFO, FormatNameValue("Done",    			Tipout.Done,    			30))
			DebugLog(DL_INFO, FormatNameValue("Step",    			Tipout.Step,    			30))
			DebugLog(DL_INFO, FormatNameValue("TipConfirmTimeout",  Tipout.TipConfirmTimeout,   30))
			DebugLog(DL_INFO, FormatNameValue("TipConfirmed",  		Tipout.TipConfirmed,   		30))
			DebugLog(DL_INFO, FormatNameValue("FromName",    		Tipout.FromName,    		30))
			DebugLog(DL_INFO, FormatNameValue("Value",    			Tipout.Value,    			30))
			DebugLog(DL_INFO, FormatNameValue("UserValue",    		Tipout.UserValue,    		30))
			DebugLog(DL_INFO, FormatNameValue("TipID",    			Tipout.TipID,    			30))
			DebugLog(DL_INFO, FormatNameValue("Message",    		Tipout.Message,    			30))
			DebugLog(DL_INFO, FormatNameValue("VerifiedUserList",   self.DebugDumpListMessage(Tipout.VerifiedUserList),   30))
			DebugLog(DL_INFO, FormatNameValue("InvalidUserList",    self.DebugDumpListMessage(Tipout.InvalidUserList),    30))
			DebugLog(DL_INFO, FormatNameValue("ConfirmedUserList",  self.DebugDumpListMessage(Tipout.ConfirmedUserList),  30))
			DebugLog(DL_INFO, FormatNameValue("FailUserList",  		self.DebugDumpListMessage(Tipout.FailUserList),  		30))
			DebugLog(DL_INFO, "   ---")
		for Tipout in TipoutThread.Tipbacks:
			DebugLog(DL_INFO, "   Tipback:")
			DebugLog(DL_INFO, FormatNameValue("Type",    			Tipout.Type,    			30))
			DebugLog(DL_INFO, FormatNameValue("Done",    			Tipout.Done,    			30))
			DebugLog(DL_INFO, FormatNameValue("Step",    			Tipout.Step,    			30))
			DebugLog(DL_INFO, FormatNameValue("TipConfirmTimeout",  Tipout.TipConfirmTimeout,   30))
			DebugLog(DL_INFO, FormatNameValue("TipConfirmed",  		Tipout.TipConfirmed,   		30))
			DebugLog(DL_INFO, FormatNameValue("FromName",    		Tipout.FromName,    		30))
			DebugLog(DL_INFO, FormatNameValue("Value",    			Tipout.Value,    			30))
			DebugLog(DL_INFO, FormatNameValue("UserValue",    		Tipout.UserValue,    		30))
			DebugLog(DL_INFO, FormatNameValue("TipID",    			Tipout.TipID,    			30))
			DebugLog(DL_INFO, FormatNameValue("Message",    		Tipout.Message,    			30))
			DebugLog(DL_INFO, FormatNameValue("VerifiedUserList",   self.DebugDumpListMessage(Tipout.VerifiedUserList),   30))
			DebugLog(DL_INFO, FormatNameValue("InvalidUserList",    self.DebugDumpListMessage(Tipout.ConfirmedUserList),  30))
			DebugLog(DL_INFO, FormatNameValue("FailUserList",  	 	self.DebugDumpListMessage(Tipout.FailUserList),  		30))
			DebugLog(DL_INFO, "   ---")
		DebugLog(DL_INFO, "---")
		DebugLog(DL_INFO, "   Tipout Auth User Cache:") 
		for AuthUser in TipoutThread.AuthUsers.Users:
			DebugLog(DL_INFO, FormatNameValue("Nick",    			AuthUser.Nick,    			30))
			DebugLog(DL_INFO, FormatNameValue("HostUser",    		AuthUser.HostUser,    		30))
			DebugLog(DL_INFO, FormatNameValue("Host",    			AuthUser.Host,    			30))
			DebugLog(DL_INFO, FormatNameValue("RealName",    		AuthUser.RealName,    		30))
			DebugLog(DL_INFO, FormatNameValue("AuthNick",    		AuthUser.AuthNick,    		30))
			DebugLog(DL_INFO, "   ---")
		DebugLog(DL_INFO, "---")
		DebugLog(DL_INFO, "UserMonitors:")
		DebugLog(DL_INFO, FormatNameValue("Thread Running",      	UserMonitors.Running,      			20))
		DebugLog(DL_INFO, FormatNameValue("Thread Terminate",    	UserMonitors.Terminate,    			20))
		DebugLog(DL_INFO, FormatNameValue("Delay",    				UserMonitors.Delay,    			20))
		DebugLog(DL_INFO, FormatNameValue("Dead Channel Timeout",   UserMonitors.DeadChannelTimeout,    			20))
		DebugLog(DL_INFO, FormatNameValue("Heart beat age",    		UserMonitors.HeartbeatAge(),    	20))
		for UserMonitor in UserMonitors.UserMonitors:
			DebugLog(DL_INFO, "UserMonitorThread:")
			DebugLog(DL_INFO, FormatNameValue("Thread Running",      		UserMonitor.Running,      			30))
			DebugLog(DL_INFO, FormatNameValue("Thread Terminate",    		UserMonitor.Terminate,    			30))
			DebugLog(DL_INFO, FormatNameValue("Active",    					UserMonitor.Active,    				30))
			DebugLog(DL_INFO, FormatNameValue("Delay",    					UserMonitor.Delay,    				30))
			DebugLog(DL_INFO, FormatNameValue("Heart beat age",    			UserMonitor.HeartbeatAge(),    	30))

			DebugLog(DL_INFO, FormatNameValue("Channel Name",    		UserMonitor.ChannelName,    				30))
			DebugLog(DL_INFO, FormatNameValue("Channel Joined",    		UserMonitor.ChannelJoined,    				30))
			DebugLog(DL_INFO, FormatNameValue("Channel Left at",    	str(UserMonitor.LastChannelLeft) + " (" + str(time.time() - UserMonitor.LastChannelLeft) + " sec ago)",    				30))
			DebugLog(DL_INFO, FormatNameValue("Channel Left reason",    UserMonitor.ChannelLeftReason,    				30))

			DebugLog(DL_INFO, FormatNameValue("UserHoldCheckRate",    		UserMonitor.UserHoldCheckRate,    	30))
			DebugLog(DL_INFO, FormatNameValue("CommitUserStatsRate",    	UserMonitor.CommitUserStatsRate,    	30))
			DebugLog(DL_INFO, FormatNameValue("UserHoldTime",    			UserMonitor.UserHoldTime,    			30))
			Msg = "(" + str(len(UserMonitor.CurrentUsers)) + "):"
			for User in UserMonitor.UserStats:
				Msg += User.Name + ","
			DebugLog(DL_INFO, FormatNameValue("UserStats", Msg, 30))
			Msg = "(" + str(len(UserMonitor.CurrentUsers)) + "):"
			for User in UserMonitor.CurrentUsers:
				Msg += User.Name + ","
			DebugLog(DL_INFO, FormatNameValue("CurrentUsers", Msg, 30))
			DebugLog(DL_INFO, "   ---")
		DebugLog(DL_INFO, "---")
		DebugLog(DL_INFO, "------------------DEBUG DONE-------------")		

#######################################################
###           TTestModule                           ###
#######################################################

class TTestModule(TThread):
	def __init__(self):
		TThread.__init__(self, self.MainLoop)
		self.SkipToTest = False
		self.Mode = "Doger" 
		self.Step = 0
		self.UserMonitor = False
		self.TipBotNick = ""
		self.OperatorNick = ""
		self.BotNick = ""
		self.SavedDebug_RestrictUser = 0
		self.Save_TipConfirm_Timeout = 0
		self.ChannelName = ""
		
	def StartTest(self, ChannelName, SkipTo = False):
		if not self.Running:
			self.Mode = "Doger" 
			self.SkipToTest = SkipTo
			self.ChannelName = ChannelName
			self.Step = 0
			self.Start()

	def StopTest(self):
		self.Terminate = True
		
	def ShowMessage(self, Msg):
		global IRC
		DebugLog(DL_INFO, Msg)
		IRC.ChanMsg(self.ChannelName, Msg)
		
	def PrivMsgTip(self, FromUser, Value, TipID):
		if self.Mode == "Doger":
			# Such SoCo_cpp has tipped you Ɖ2 (to claim /msg Doger help) [6315b33e]
			return "Such " + FromUser + " has tipped you Ɖ" + str(Value) + " (to claim /msg Doger help) " + TipID
			
	def ChannelTip(self, FromUser, ToUser, Value, TipID):
		if self.Mode == "Doger":
			# Such SoCo_cpp tipped much Ɖ7 to Doge_Soaker! (to claim /msg Doger help) [26d75f17]
			return "Such " + FromUser + " tipped much Ɖ" + str(Value) + " to " + ToUser + "! (to claim /msg Doger help) " + TipID
			
	def PrivMsgTipConfrm(self, ToUser, Value):
		if self.Mode == "Doger":
			# SoCo_cpp: Tipped: Doge_Soaker 2 DogeXM 50 [b56f900b]
			return self.BotNick + ": Tipped: " + ToUser + " " + str(Value) + "  [TestConf]"		
			
	def IsNotifyTipBusy(self):
		if self.Mode == "Doger":
			return NotifyTipThread.IsBusy()

	def MainLoop(self):
		time.sleep(0.001)
		if self.Terminate: 
			return # test once, before try block to prevent Unlock
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG TTestModule thread started")
		while not self.Terminate:
			if not self.TryLock():
				return False
			try: # try/finally trap for Terminating/Unlock'ing/DeReference'ing
				RetValue = False
				if self.Step == 0:
					RetValue = self.StepTestBegin()
				elif self.Step == 1:
					if self.SkipToTest != False:
						self.Step = self.SkipToTest
						self.ShowMessage("TestModule skipping to test step: " + str(self.SkipToTest))
						self.Step = self.Step - 1 # will be incremented
						RetValue = True
					else:
						RetValue = self.StepTest1()
				elif self.Step == 2:
					RetValue = self.StepTest2()
				elif self.Step == 3:
					RetValue = self.StepTest3()
				elif self.Step == 4:
					RetValue = self.StepTest4()
				elif self.Step == 5:
					RetValue = self.StepTest5()
				elif self.Step == 6:
					RetValue = self.StepTest6()
				elif self.Step == 7:
					RetValue = self.StepTest7()
				elif self.Step == 8:
					RetValue = self.StepTest8()
				elif self.Step == 9:
					RetValue = self.StepTest9()
				elif self.Step == 10:
					RetValue = self.StepTest10()
				elif self.Step == 11:
					RetValue = self.StepTest11()
				elif self.Step == 12:
					RetValue = self.StepTest12()
				elif self.Step == 13:
					RetValue = self.StepTest13()
				elif self.Step == 14:
					RetValue = self.StepTest14()
				elif self.Step == 15:
					RetValue = self.StepTest15()
				elif self.Step == 16:
					RetValue = self.StepTest16()
				elif self.Step == 17:
					RetValue = self.StepTestFinish()
				if not RetValue:
					DebugLog(DL_INFO, "TTestModule:: MainLoop Test step failed, step: " + str(self.Step))
					self.StopTest()
				else:
					self.Step = self.Step + 1
					if self.Step >= 17:
						self.ShowMessage("TTestModule All Tests Done, stopping thread.")
						self.StopTest()
			except:
				DebugLog(DL_ERROR, "TTestModule exception:")
				DebugLog(DL_ERROR, traceback.format_exc())
				self.ShowMessage("Test module encountered an exception")
				self.StopTest()
			finally:
				self.TouchHeartbeatLocked()
				self.Unlock()
			if self.Terminate: 
				return
			self.SafeSleep(0.2)
		if self.FinishTesting() == False:
			DebugLog(DL_WARN, "DEBUG TTestModule FinishTesting failed, continuing to stop thread...")
		if DEBUG_Notify:
			DebugLog(DL_INFO, "DEBUG TTestModule thread done")
		
	def StepTestBegin(self):
		global OperatorList, C_DEBUG_Simulate, DogeSoaker, Debug_RestrictUser, TipoutThread, UserMonitors, IRC
		if len(OperatorList.Nicks) == 0 or not C_DEBUG_Simulate:
			Msg = "StepTestBegin Error: DogeSoaker_Test - Not ready for test or not in Simulation mode, or empty Operator list"
			DebugLog(DL_ERROR, Msg)
			print(Msg)
			IRC.ChanMsg(self.ChannelName, Msg)
			return False			
		#DebugLog(DL_DEBUG, "StepTestBegin trying DogeSoaker Restart")
		#if not DogeSoaker.RestartFromTest(): # use FromTest version so we don't get stuck in a loop
		#	DebugLog(DL_DEBUG, "StepTestBegin DogeSoaker Restart failed.")
		#	return False
		self.TipBotNick   				= OperatorList.Nicks[0] # simulate tips by sending to operator
		self.OperatorNick 				= OperatorList.Nicks[0]
		self.BotNick      				= DogeSoaker.GetBotNick()
		self.SavedDebug_RestrictUser 	= Debug_RestrictUser # save previous setting
		self.Save_TipConfirm_Timeout 	= TipoutThread.TipConfirmTimeoutSpeed
		self.UserMonitor 				= UserMonitors.JoinAddChannel(self.ChannelName)
		TipoutThread.TipConfirmTimeoutSpeed = 10.0
		Debug_RestrictUser = False
		if self.Terminate:
			DebugLog(DL_DEBUG, "StepTestBegin detected teminate mid preparation.")
			return False
		#DebugLog(DL_DEBUG, "StepTestBegin DogeSoaker Restart DONE ")
		self.SafeSleep(2)
		NotifyCommandThread.Stop() # we will be faking all WHOIS and want no real replies
		self.SafeSleep(5)
		self.ShowMessage(FormatNameValue("tip bot nick", self.TipBotNick, 15))
		self.ShowMessage(FormatNameValue("operator nick", self.OperatorNick, 15))
		self.ShowMessage(FormatNameValue("soak bot nick", self.BotNick, 15))
		self.ShowMessage(FormatNameValue("test channel", self.ChannelName, 15))
		return True
		
	def StepTestFinish(self):
		global TipoutThread, DogeSoaker, UserMonitors, NotifyCommandThread, TipoutThread, NotifyCommandThread
		if TipoutThread.IsBusy():
			DebugLog(DL_ERROR, "Test Finish still busy, dumping debug")
			DogeSoaker.DebugDump()
			DebugLog(DL_ERROR, "Test Finish was still busy, waiting longer")
			self.SafeSleep(30)
			if TipoutThread.IsBusy():
				Title = "Test Finish All tipouts NOT confirmed done!"
				self.SafeSleep(30)
			else:
				Title = "Test Finish All tipouts confirmed done after extra wait."
		else:
			Title = "Test Finish All tipouts confirmed done."
		self.ShowMessage(Title)
		self.StopTest()
		return True

	def FinishTesting(self):
		global TipoutThread, UserMonitors, NotifyCommandThread, TipoutThread, NotifyCommandThread, Debug_RestrictUser
		self.UserMonitor.WaitStop()
		del self.UserMonitor
		self.UserMonitor = False
		NotifyCommandThread.Clear()
		del NotifyCommandThread.Notifications[:] # clear whois commands waiting to go out
		TipoutThread.TipConfirmTimeoutSpeed = self.Save_TipConfirm_Timeout
		Debug_RestrictUser = self.SavedDebug_RestrictUser
		NotifyCommandThread.Start() # restart temporarily stopped processing
		#TipoutThread.ResetSoak()
		UserMonitors.Reset()
		return True
		
	def StepTest1(self):
		global C_Minimum_Tip_Value, UserMonitors, DogeSoaker
		Value = int(C_Minimum_Tip_Value * 2)
		self.ShowMessage("TEST 1 - Disabled, return " + str(Value)  + " to " + self.OperatorNick)
		DogeSoaker.Pause()
		self.UserMonitor.Reset()
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test01-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test01-]"))
		self.SafeSleep(2)
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm(self.OperatorNick, Value))
		DogeSoaker.Resume()
		self.SafeSleep(4)
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 1 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 1 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 1 ready after waiting longer")
		return True
		
	def StepTest2(self):
		global C_Minimum_Tip_Value, UserMonitors, DogeSoaker
		#Value = int(C_Minimum_Tip_Value * 2)
		self.ShowMessage("TEST 2 - Ignore tip not to bot")
		self.UserMonitor.Reset()
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, "NotTheBotNick", 1000000, "[-Test02-]"))
		# No privmsg Doger tip confirm
		self.SafeSleep(4)
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 2 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 2 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 2 ready after waiting longer")
		return True
		
	def StepTest3(self):
		global C_Minimum_Tip_Value, UserMonitors, DogeSoaker
		Value = 2000000
		self.ShowMessage("TEST 3 - Ignore tip message not from tipbot")
		self.UserMonitor.Reset()
		DogeSoaker.HandleChannelMessage(self.ChannelName, "NotTheTipBot", self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test03-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg("NotTheTipBot", self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test03-]"))
		self.SafeSleep(4)
		return True

	def StepTest4(self):
		global C_Minimum_Tip_Value, UserMonitors, DogeSoaker, Debug_RestrictUser
		Value = int(C_Minimum_Tip_Value * 10)
		self.ShowMessage("TEST 4 - Restricted, but not the operator, return " + str(Value) + " to " + "NotTheOperator")
		Debug_RestrictUser = True
		self.UserMonitor.Reset()
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip("NotTheOperator", self.BotNick, Value, "[-Test04-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip("NotTheOperator", Value, "[-Test04-]"))
		self.SafeSleep(3)
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm("NotTheOperator", Value))
		self.SafeSleep(4)
		Debug_RestrictUser = False
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 4 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 4 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 4 ready after waiting longer")
		return True

	def StepTest5(self):
		global C_Minimum_Tip_Value, UserMonitors, DogeSoaker
		Value = int(C_Minimum_Tip_Value - 1)
		self.ShowMessage("TEST 5 - Refuse too small tip 1, return  " + str(Value)  + " to " + self.OperatorNick)
		self.UserMonitor.Reset()
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test05-]"))
		self.SafeSleep(2)
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test05-]"))
		self.SafeSleep(3)
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm(self.OperatorNick, Value))
		self.SafeSleep(4)
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 5 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 5 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 5 ready after waiting longer")
		return True

	def StepTest6(self):
		global C_Minimum_Tip_Value, UserMonitors, DogeSoaker
		Value = int(C_Minimum_Tip_Value * 2)
		self.ShowMessage("TEST 6 - Refuse no active users, return  " + str(Value)  + " to " + self.OperatorNick)
		self.UserMonitor.Reset()
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test06-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test06-]"))
		self.SafeSleep(3)
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm(self.OperatorNick, Value))
		self.SafeSleep(4)
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 6 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 6 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 6 ready after waiting longer")
		return True

	def StepTest7(self):
		global C_Minimum_Tip_Value, DogeSoaker, TipoutThread
		Value = int(C_Minimum_Tip_Value * 2)
		self.ShowMessage("TEST 7 - Refuse no authenticated users, return  " + str(Value)  + " to " + self.OperatorNick)
		TipoutThread.ResetSoak() # previous failure still trips soak delay
		self.UserMonitor.Reset()
		self.UserMonitor.AddUpdateUser("TestUser-1-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-2-QdeUdjg39slknme")
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test07-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test07-]"))
		self.SafeSleep(3)
		self.ShowMessage("Sending non-identified")
		TipoutThread.UpdateWhoIsEnd("TestUser-1-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsEnd("TestUser-2-QdeUdjg39slknme")
		self.ShowMessage("6 sec...")
		self.SafeSleep(6)
		self.ShowMessage("Sending tipback confirm")
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm(self.OperatorNick, Value))
		self.SafeSleep(4)
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 7 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 7 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 7 ready after waiting longer")
		return True

	def StepTest8(self):
		global C_Minimum_Tip_Value, DogeSoaker, TipoutThread
		Value = int(C_Minimum_Tip_Value + 1)
		self.ShowMessage("TEST 8 - Refuse too small tip 2, return  " + str(Value)  + " to " + self.OperatorNick)
		TipoutThread.Reset() 
		self.UserMonitor.Reset()
		self.UserMonitor.AddUpdateUser("TestUser-1-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-2-QdeUdjg39slknme")
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test08-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test08-]"))
		self.SafeSleep(3)
		TipoutThread.UpdateWhoIsAuth("TestUser-1-QdeUdjg39slknme", "TestUser-1-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-2-QdeUdjg39slknme", "TestUser-2-QdeUdjg39slknme")
		self.SafeSleep(5)
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm(self.OperatorNick, Value))
		self.SafeSleep(4)
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 8 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 8 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 8 ready after waiting longer")
		return True

	def StepTest9(self):
		self.ShowMessage("TEST 9 - Disabled, soak delay removed for multi-chan support")
		return True

	def StepTest10(self):
		global C_Minimum_Tip_Value, DogeSoaker, TipoutThread
		Value = int(C_Minimum_Tip_Value * 4)
		UserValue = int(Value / 2)
		self.ShowMessage("TEST 10 - Successful soak of " + str(UserValue) + " to 2 test users")
		TipoutThread.Reset()
		self.UserMonitor.Reset()
		self.UserMonitor.AddUpdateUser("TestUser-1-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-2-QdeUdjg39slknme")
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test10-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test10-]"))
		self.SafeSleep(3)
		TipoutThread.UpdateWhoIsAuth("TestUser-1-QdeUdjg39slknme", "TestUser-1-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-2-QdeUdjg39slknme", "TestUser-2-QdeUdjg39slknme")
		self.SafeSleep(4)
		while self.IsNotifyTipBusy(): 
			self.ShowMessage("...")
			self.SafeSleep(4)
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm("TestUser-1-QdeUdjg39slknme", UserValue))
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm("TestUser-2-QdeUdjg39slknme", UserValue))
		self.SafeSleep(15) # let previous tipout finish for clean test
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 10 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 10 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 10 ready after waiting longer")
		return True

	def StepTest11(self):
		global C_Minimum_Tip_Value, DogeSoaker, TipoutThread
		Value = int(C_Minimum_Tip_Value * 4)
		self.ShowMessage("TEST 11 - Ignore PM tip message from non-tipbot")
		TipoutThread.Reset()
		self.UserMonitor.Reset()
		DogeSoaker.HandlePrivMsg("NotTheTipbot", self.BotNick, self.PrivMsgTip("TestUser-1-QdeUdjg39slknme", Value, "[-Test11-]"))
		self.SafeSleep(2)
		return True

	def StepTest12(self):
		self.ShowMessage("TEST 12 - Disabled, PM soak not currently supported")
		return True

	def StepTest13(self):
		global C_Minimum_Tip_Value, DogeSoaker, TipoutThread
		Value = int(C_Minimum_Tip_Value * 8)
		UserValue = int(Value / 4)
		self.ShowMessage("TEST 13 - Successful soak of " + str(UserValue) + " to 4 test users, resend 2")
		TipoutThread.Reset()
		self.UserMonitor.Reset()
		self.UserMonitor.AddUpdateUser("TestUser-13-1-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-13-2-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-13-3-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-13-4-QdeUdjg39slknme")
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test13-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test13-]"))
		self.SafeSleep(3)
		self.ShowMessage("Sending whois replies...")
		TipoutThread.UpdateWhoIsAuth("TestUser-13-1-QdeUdjg39slknme", "TestUser-13-1-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-13-2-QdeUdjg39slknme", "TestUser-13-2-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-13-3-QdeUdjg39slknme", "TestUser-13-3-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-13-4-QdeUdjg39slknme", "TestUser-13-4-QdeUdjg39slknme")
		self.SafeSleep(5)
		while self.IsNotifyTipBusy(): 
			self.ShowMessage("...")
			self.SafeSleep(4)
		self.ShowMessage("Sending 2 tip confirmations...")
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm("TestUser-13-1-QdeUdjg39slknme", UserValue))
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm("TestUser-13-2-QdeUdjg39slknme", UserValue))
		self.ShowMessage("Waiting for 2 retries...")
		self.ShowMessage("25 sec...")
		self.SafeSleep(5)
		self.ShowMessage("20 sec...")
		self.SafeSleep(10)
		self.ShowMessage("10 sec...")
		self.SafeSleep(10)
		self.ShowMessage("sending retry confirms...")
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm("TestUser-13-3-QdeUdjg39slknme", UserValue))
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm("TestUser-13-4-QdeUdjg39slknme", UserValue))
		self.ShowMessage("20 sec...")
		self.SafeSleep(10)
		self.ShowMessage("10 sec...")
		self.SafeSleep(10)
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 13 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 13 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 13 ready after waiting longer")
		return True
		
	def StepTest14(self):
		global C_Minimum_Tip_Value, DogeSoaker, TipoutThread
		if self.Mode != "Doger":
			DebugLog(DL_DEBUG, "TTestModule::StepTest14 skipping test which is only for Doger mode, Mode: " + self.Mode)
			return True
		Value = int(C_Minimum_Tip_Value * 8)
		#UserValue = int(Value / 4)
		self.ShowMessage("TEST 14 - Channel tip not confirmed by pm message (Doger chasing tail) " + str(Value))
		TipoutThread.Reset()
		self.UserMonitor.Reset()
		self.UserMonitor.AddUpdateUser("TestUser-14-1-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-14-2-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-14-3-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-14-4-QdeUdjg39slknme")
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test14-]"))
		self.ShowMessage("8 sec...")
		self.SafeSleep(8)
		self.ShowMessage("Sending whois replies...")
		TipoutThread.UpdateWhoIsAuth("TestUser-14-1-QdeUdjg39slknme", "TestUser-14-1-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-14-2-QdeUdjg39slknme", "TestUser-14-2-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-14-3-QdeUdjg39slknme", "TestUser-14-3-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-14-4-QdeUdjg39slknme", "TestUser-14-4-QdeUdjg39slknme")
		self.ShowMessage("7 sec...")
		self.SafeSleep(7)
		self.ShowMessage("Sending confirm")
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTipConfrm(self.OperatorNick, Value))
		self.ShowMessage("10 sec...")
		self.SafeSleep(10)
		
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 14 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 14 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 14 ready after waiting longer")
		return True

	def StepTest15(self):
		global C_Minimum_Tip_Value, DogeSoaker, TipoutThread
		if self.Mode != "Doger":
			DebugLog(DL_DEBUG, "TTestModule::StepTest15 skipping test which is only for Doger mode, Mode: " + self.Mode)
			return True
		Value = int(C_Minimum_Tip_Value * 8)
		UserValue = int(Value / 4)
		self.ShowMessage("TEST 15 - Channel tip to 4 users, 2 confirmed, 2 failed (offline) " + str(UserValue) + " each")
		TipoutThread.Reset()
		self.UserMonitor.Reset()
		self.UserMonitor.AddUpdateUser("TestUser-15-1-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-15-2-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-15-3-QdeUdjg39slknme")
		self.UserMonitor.AddUpdateUser("TestUser-15-4-QdeUdjg39slknme")
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test15-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test15-]"))
		self.ShowMessage("18 sec...")
		self.SafeSleep(8)
		TipoutThread.UpdateWhoIsAuth("TestUser-15-1-QdeUdjg39slknme", "TestUser-15-1-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-15-2-QdeUdjg39slknme", "TestUser-15-2-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-15-3-QdeUdjg39slknme", "TestUser-15-3-QdeUdjg39slknme")
		TipoutThread.UpdateWhoIsAuth("TestUser-15-4-QdeUdjg39slknme", "TestUser-15-4-QdeUdjg39slknme")
		self.ShowMessage("5 sec...")
		self.SafeSleep(5)
		self.ShowMessage("Sending confirm with 2 fails")
		DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.BotNick + ": Tipped: TestUser-15-1-QdeUdjg39slknme " + str(UserValue) + " TestUser-15-2-QdeUdjg39slknme " + str(UserValue) + " [-Test15-]  Failed: TestUser-15-3-QdeUdjg39slknme (offline) TestUser-15-4-QdeUdjg39slknme (offline)")
		self.ShowMessage("10 sec...")
		self.SafeSleep(10)
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 15 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 15 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 15 ready after waiting longer")
		return True

	def StepTest16(self):
		global C_Minimum_Tip_Value, DogeSoaker, TipoutThread
		if self.Mode != "Doger":
			DebugLog(DL_DEBUG, "TTestModule::StepTest16 skipping test which is only for Doger mode, Mode: " + self.Mode)
			return True
		UserCount = 50
		Value = int(C_Minimum_Tip_Value * UserCount + 20)
		UserValue = int(math.floor(Value / UserCount))
		self.ShowMessage("TEST 16 - Channel tipping " + str(Value) + " to " + str(UserCount) + " users, all but 2 confirmed, 2 failed (offline) " + str(UserValue) + " each")
		TipoutThread.Reset()
		self.UserMonitor.Reset()
		TestNicks = []
		for i in range(0, UserCount):
			TestNicks.append("TestUser-16-" + str(i))
		for Nick in TestNicks:
			self.UserMonitor.AddUpdateUser(Nick)
		DogeSoaker.HandleChannelMessage(self.ChannelName, self.TipBotNick, self.ChannelTip(self.OperatorNick, self.BotNick, Value, "[-Test16-]"))
		if self.Mode == "Doger":
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, self.PrivMsgTip(self.OperatorNick, Value, "[-Test16-]"))
		self.SafeSleep(3)
		self.ShowMessage("Updatting auth...")
		for Nick in TestNicks:
			TipoutThread.UpdateWhoIsAuth(Nick, Nick)
		self.ShowMessage("Updatting auth...Done")
		self.ShowMessage("10 sec...")
		self.SafeSleep(10)
		idx = 0
		cnt = 19
		left = UserCount - 2
		self.ShowMessage("Sending tip confirms...")
		while left > 0:
			Msg = self.BotNick + ": Tipped: "
			for Nick in TestNicks[idx:idx+cnt]:
				Msg += Nick + " " + str(UserValue) + " "
			Msg += "[-Test15-]"
			idx += cnt
			left -= cnt
			if left <= 0:
				Msg +=  " Failed:"
				for Nick in TestNicks[-2:]:
					Msg += " " + Nick + " (offline) "
			elif left < cnt:
				cnt = left
			self.ShowMessage("Confirming (left " + str(left) + "): " + Msg)
			DogeSoaker.HandlePrivMsg(self.TipBotNick, self.BotNick, Msg)
			self.SafeSleep(4)
		self.ShowMessage("Confirmes Done")
		self.ShowMessage("30 sec...")
		self.SafeSleep(30)
		if TipoutThread.IsBusy():
			self.ShowMessage("Test 16 still busy, dumping debug")
			DogeSoaker.DebugDump()
			self.SafeSleep(10)
			if TipoutThread.IsBusy():
				self.ShowMessage("Test 16 was still busy after waiting longer")
				return False
			else:
				self.ShowMessage("Test 16 ready after waiting longer")
		return True

#######################################################
###           Non-XChat Callbacks                   ###
#######################################################
def DS_Init():
	global C_Data_Directory, C_Notify_PM_Delay, C_Notify_CMD_Delay, C_Notify_Msg_Delay, IRC, IgnoreList, IgnoreHostList, TipbotList, OperatorList, DogeSoaker, ConfigFile, DebugLogger, AuditLog, NotifyTipThread, NotifyCommandThread, NotifyMessageThread, Events, PrivMsgHandlerThread, UserMonitors, TipoutThread, TestModule, TextHandler
	try:
		print("DS_Init")
		IRC = TIRC()        
		
		TextHandler = TTextHandler()

		IgnoreList = TUserList()
		IgnoreHostList = TUserList()
		TipbotList = TUserList()
		OperatorList = TUserList()


		DogeSoaker = TDogeSoaker(C_Data_Directory)

		ConfigFile = TConfigFile(C_Data_Directory)
		DebugLogger = TDebugLogger(C_Data_Directory)
		AuditLog = TAuditLog(C_Data_Directory)

		NotifyTipThread = TNotifyThread()
		NotifyCommandThread = TNotifyThread()
		NotifyMessageThread = TNotifyThread()

		Events = TEventThread()

		PrivMsgHandlerThread = TPrivMsgHandlerThread()
		UserMonitors = TUserMonitors()
		TipoutThread = TTipoutThread()

		NotifyTipThread.NotifySpeed     = C_Notify_PM_Delay
		NotifyCommandThread.NotifySpeed = C_Notify_CMD_Delay
		NotifyMessageThread.NotifySpeed = C_Notify_Msg_Delay

		TestModule = TTestModule()
		DogeSoaker.InitConfig(True) # LoadDefaultsOnFail
		print(DogeSoaker.GetModuleStatus())
		if DogeSoaker.Start() == False:
			DebugLog(DL_ERROR, "DS_Init DogeSoaker Start failed")
		DebugLog(DL_DEBUG, "DS_Init Done.")
	except:
		print("Doge Soaker Error: DS_Init exception:")
		print(traceback.format_exc())
		DebugLog(DL_ERROR, "DS_Init exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	

def DS_Unload():
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_Unload validate DogeSoaker object failed")
		return
	DebugLog(DL_DEBUG, "DS_Unload")
	try:
		DogeSoaker.Unload()
	except:
		DebugLog(DL_ERROR, "DS_Unload exception:")
		DebugLog(DL_ERROR, traceback.format_exc())

def DS_HandlePrivMsg(FromUser, ToUser, Message):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "DS_HandlePrivMsg validate DogeSoaker object failed")
			return
		#DebugLog(DL_DEBUG, "DS_HandlePrivMsg FromUser: " + FromUser + ", ToUser: " + ToUser + ", Message: " + Message)
		DogeSoaker.HandlePrivMsg(FromUser, ToUser, Message)
	except:
		DebugLog(DL_ERROR, "DS_HandlePrivMsg exception:")
		DebugLog(DL_ERROR, traceback.format_exc())

def DS_HandleChannelMessage(ChannelName, UserName, Message):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_HandleChannelMessage validate DogeSoaker object failed")
		return
	#DebugLog(DL_DEBUG, "DS_HandleChannelMessage ChannelName: " + ChannelName + ", UserName: " + UserName + ", Message: " + Message)
	try:
		DogeSoaker.HandleChannelMessage(ChannelName, UserName, Message)
	except:
		DebugLog(DL_ERROR, "DS_HandleChannelMessage exception:")
		DebugLog(DL_ERROR, traceback.format_exc())

def DS_HandleWhoisAuth(CurrNick, AuthNick):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_HandleWhoisAuth validate DogeSoaker object failed")
		return
	try:
		DogeSoaker.HandleWhoisAuth(CurrNick, AuthNick)
	except:
		DebugLog(DL_ERROR, "DS_HandleWhoisAuth exception:")
		DebugLog(DL_ERROR, traceback.format_exc())

def DS_HandleWhoisNameLine(Nick, HostUser, Host, RealName):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_HandleWhoisNameLine validate DogeSoaker object failed")
		return
	try:
		DogeSoaker.HandleWhoisNameLine(Nick, HostUser, Host, RealName)
	except:
		DebugLog(DL_ERROR, "DS_HandleWhoisNameLine exception:")
		DebugLog(DL_ERROR, traceback.format_exc())


def DS_HandleWhoisEnd(Nick):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_HandleWhoisEnd validate DogeSoaker object failed")
		return
	try:
		DogeSoaker.HandleWhoisEnd(Nick)
	except:
		DebugLog(DL_ERROR, "DS_HandleWhoisEnd exception:")
		DebugLog(DL_ERROR, traceback.format_exc())

def DS_HandleJoin(Nick, Host, Channel):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_HandleJoin validate DogeSoaker object failed")
		return
	try:
		DogeSoaker.HandleJoin(Nick, Host, Channel)
	except:
		DebugLog(DL_ERROR, "DS_HandleJoin exception:")
		DebugLog(DL_ERROR, traceback.format_exc())

def DS_HandleQuit(Nick, Host, Reason):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_HandleQuit validate DogeSoaker object failed")
		return
	try:
		DogeSoaker.HandleQuit(Nick, Host, Reason)
	except:
		DebugLog(DL_ERROR, "DS_HandleQuit exception:")
		DebugLog(DL_ERROR, traceback.format_exc())

def DS_HandleNickChanged(OldNick, Host, NewNick):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_HandleNickChanged validate DogeSoaker object failed")
		return
	try:
		DogeSoaker.HandleNickChanged(OldNick, Host, NewNick)
	except:
		DebugLog(DL_ERROR, "DS_HandleNickChanged exception:")
		DebugLog(DL_ERROR, traceback.format_exc())

def DS_HandleKick(Nick, ChannelName, Reason, KickerNick):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_HandleKick validate DogeSoaker object failed")
		return
	try:
		DogeSoaker.HandleKick(Nick, ChannelName, Reason, KickerNick)
	except:
		DebugLog(DL_ERROR, "DS_HandleKick exception:")
		DebugLog(DL_ERROR, traceback.format_exc())

def DS_HandlePart(Nick, Host, ChannelName, Reason):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "DS_HandlePart validate DogeSoaker object failed")
		return
	try:
		DogeSoaker.HandlePart(Nick, Host, ChannelName, Reason)
	except:
		DebugLog(DL_ERROR, "DS_HandlePart exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	
#######################################################
###           XChat Callbacks                       ###
#######################################################
# pylint: disable=W0613
def XChat_DS_Help(word, word_eol, userdata):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "XChat_DS_Help validate DogeSoaker object failed")
		return
	print(DogeSoaker.GetModuleVersion())
	print(DogeSoaker.GetModuleHelpMessage())
	return xchat.EAT_ALL

def XChat_DS_Start(word, word_eol, userdata):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "XChat_DS_Start validate DogeSoaker object failed")
		return
	DogeSoaker.Start()
	return xchat.EAT_ALL
	
def XChat_DS_Stop(word, word_eol, userdata):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "XChat_DS_Stop validate DogeSoaker object failed")
		return
	DogeSoaker.Stop()
	return xchat.EAT_ALL
	
def XChat_DS_Restart(word, word_eol, userdata):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "XChat_DS_Restart validate DogeSoaker object failed")
		return
	DogeSoaker.Restart()
	return xchat.EAT_ALL

def XChat_DS_Reset(word, word_eol, userdata):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "XChat_DS_Reset validate DogeSoaker object failed")
		return
	DogeSoaker.Reset()
	return xchat.EAT_ALL

def XChat_DS_Pause(word, word_eol, userdata):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "XChat_DS_Pause validate DogeSoaker object failed")
		return
	DogeSoaker.Pause()
	return xchat.EAT_ALL
	
def XChat_DS_Resume(word, word_eol, userdata):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "XChat_DS_Resume validate DogeSoaker object failed")
		return
	DogeSoaker.Resume()
	return xchat.EAT_ALL
	
def XChat_DS_DebugDump(word, word_eol, userdata):
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "XChat_DS_DebugDump validate DogeSoaker object failed")
		print("XChat_DS_DebugDump validate DogeSoaker object failed")
		return xchat.EAT_ALL
	DogeSoaker.DebugDump()
	return xchat.EAT_ALL
	
def XChat_DS_Unload(userdata):
	global DogeSoaker, TestModule
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_Unload validate DogeSoaker object failed")
		elif not DogeSoaker.Unload(): 
			DebugLog(DL_ERROR, "XChat_DS_Unload DogeSoaker Unload failed")
		DebugLog(DL_DEBUG, "XChat_DS_Unload DogeSoaker Unload DONE")
	except:
		DebugLog(DL_ERROR, "XChat_DS_Unload DogeSoaker Unload exception: ")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_ALL
	
def XChat_DS_PrivMsg(word, word_eol, userdata):
	global DogeSoaker, IRC
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_PrivMsg validate DogeSoaker object failed")
			return
		FromUser = word[0]
		Msg = ConStringList(word[1:]) # all but [0]
		ToUser = IRC.GetNick()
		DogeSoaker.HandlePrivMsg(FromUser, ToUser, Msg)
	except:
		DebugLog(DL_ERROR, "XChat_DS_PrivMsg exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE
		
def XChat_DS_ChanMsg(word, word_eol, userdata):
	global DogeSoaker, IRC
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_ChanMsg validate DogeSoaker object failed: " + ",".join(word))
			return
		if not xchat:
			DebugLog(DL_ERROR, "XChat_DS_ChanMsg validate xchat failed: " + ",".join(word))
			return
		Context = IRC.CurrentContext()
		if not Context:
			DebugLog(DL_ERROR, "XChat_DS_ChanMsg validate Context failed: " + ",".join(word))
			return
		ChannelName = IRC.GetContextChannelName(Context)
		UserName = word[0] 
		Message = word[1] # word[2] is "@"   
		if len(UserName) > 3 and UserName[0] == chr(3): # Colored user name option leaves escaped color prepend such as 0x3,'2','8'
			UserName = UserName[3:] # trim off color code
		DogeSoaker.HandleChannelMessage(ChannelName, UserName, Message)
	except:
		DebugLog(DL_ERROR, "XChat_DS_ChanMsg exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE
	
def XChat_DS_WhoIsAuthenticated(word, word_eol, userdata):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_WhoIsAuthenticated validate DogeSoaker object failed")
			return
		CurrNick = word[0]
		AuthNick = word[2]
		DogeSoaker.HandleWhoisAuth(CurrNick, AuthNick)
	except:
		DebugLog(DL_ERROR, "XChat_DS_WhoIsAuthenticated exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE

def XChat_DS_WhoIsNameLine(word, word_eol, userdata):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_WhoIsNameLine validate DogeSoaker object failed")
			return
		Nick = word[0]
		HostUser = word[1]
		Host = word[2]
		RealName = word[3]
		DogeSoaker.HandleWhoisNameLine(Nick, HostUser, Host, RealName)
	except:
		DebugLog(DL_ERROR, "XChat_DS_WhoIsNameLine exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE
	
def XChat_DS_WhoIsEnd(word, word_eol, userdata):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_WhoIsEnd validate DogeSoaker object failed")
			return
		Nick = word[0]
		DogeSoaker.HandleWhoisEnd(Nick)
	except:
		DebugLog(DL_ERROR, "XChat_DS_WhoIsEnd exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE
	
def XChat_DS_Quit(word, word_eol, userdata):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_Quit validate DogeSoaker object failed")
			return
		Reason = ""
		Host = ""
		Nick = word[0]
		if len(word) > 1:
			Reason = word[1]
		if len(word) > 2:
			Host = word[2]
		#DebugLog(DL_DEBUG, "XChat_DS_Quit Nick: " + Nick + ", Host: " + Host + ", Reason: " + Reason)
		DogeSoaker.HandleQuit(Nick, Host, Reason)
	except:
		DebugLog(DL_ERROR, "XChat_DS_Quit exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE

def XChat_DS_Kick(word, word_eol, userdata):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_Kick validate DogeSoaker object failed")
			return
		KickerNick = word[0]
		Nick = word[1]
		ChannelName = word[2]
		Reason = word[3]
		DebugLog(DL_DEBUG, "XChat_DS_Kick Nick: " + Nick + ", kicked by: " + KickerNick + ", from channel: " + ChannelName + ", reason: " + Reason)
		DogeSoaker.HandleKick(Nick, ChannelName, Reason, KickerNick)	
	except:
		DebugLog(DL_ERROR, "XChat_DS_Kick exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE
	
def XChat_DS_Join(word, word_eol, userdata):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_Join validate DogeSoaker object failed")
			return
		Nick = word[0]
		ChannelName = word[1]
		Host = word[2]
		#DebugLog(DL_DEBUG, "XChat_DS_Join Nick: " + Nick + ", Channel: " + ChannelName + ", Host: " + Host)
		DogeSoaker.HandleJoin(Nick, Host, ChannelName)
	except:
		DebugLog(DL_ERROR, "XChat_DS_Join exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE
	
def XChat_DS_Part(word, word_eol, userdata):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_Part validate DogeSoaker object failed")
			return
		Nick = word[0]
		Host = word[1]
		ChannelName = word[2]
		if len(word) > 3:
			Reason = word[3]
		else:
			Reason = ""
		#DebugLog(DL_DEBUG, "XChat_DS_Part Nick: " + Nick + ", Host: " + Host + ", Channel: " + ChannelName + ", Reason: " + Reason)
		DogeSoaker.HandlePart(Nick, Host, ChannelName, Reason)
	except:
		DebugLog(DL_ERROR, "XChat_DS_Part exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE

def XChat_DS_TipBot(word, word_eol, userdata):
	global C_TipBotNick, C_Tipbot_Nick_List, TipbotList, IgnoreList
	try:
		tblist = []
		if len(word) > 1:
			tblist = " ".join(word[1:]).strip().split(",")
			if len(tblist) > 0:
				C_Tipbot_Nick_List = []
				TipbotList.Clear()
				for Nick in tblist:
					Nick = Nick.strip()
					C_Tipbot_Nick_List.append(Nick)
					TipbotList.AddNick(Nick)
					IgnoreList.AddUniqueNick(Nick)
				C_TipBotNick = C_Tipbot_Nick_List[0]
				print("New Tip Bots: " + ",".join(C_Tipbot_Nick_List))
				return xchat.EAT_ALL	
		print("Current Tip Bots: " + ",".join(C_Tipbot_Nick_List))
	except:
		DebugLog(DL_ERROR, "XChat_DS_TipBot exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_ALL
		
	
def XChat_DS_Test(word, word_eol, userdata):
	#csv = ""
	#UserNicks = csv.split(",")
	#for UserNick in UserNicks:
	#	NotifyTipThread.AddNotify("test return", C_TipBotNick, "send " + UserNick + " 16")
	return xchat.EAT_ALL	
	
def XChat_DS_ChangeNick(word, word_eol, userdata):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_ChangeNick validate DogeSoaker object failed")
			return
		if len(word) >= 2:
			OldNick = word[0]
			NewNick = word[1]
			DogeSoaker.HandleNickChanged(OldNick, "<host unknown>", NewNick)
		else:
			DebugLog(DL_ERROR, "XChat_DS_ChangeNick parameter missing")
	except:
		DebugLog(DL_ERROR, "XChat_DS_ChangeNick exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE	

def XChat_DS_Status(word, word_eol, userdata):
	global DogeSoaker
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_Status validate DogeSoaker object failed")
			return
		print(DogeSoaker.GetModuleVersion())
		print(DogeSoaker.GetModuleStatus())
	except:
		DebugLog(DL_ERROR, "XChat_DS_Status exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_ALL
	
def XChat_DS_SASL_Success(word, word_eol, userdata):
	global DogeSoaker
	# :zelazny.freenode.net 903 Doge_Soaker4 :SASL authentication successful
	try:
		if not DogeSoaker:
			DebugLog(DL_ERROR, "XChat_DS_SASL_Success validate DogeSoaker object failed")
			return
		elif len(word) > 2:
			NewNick = word[2]
			DogeSoaker.HandleNickChanged(DogeSoaker.BotNick, "<host unknown>", NewNick) # SASL Success doesn have an OldNick to change from, grab current BotNick
		else:
			DebugLog(DL_ERROR, "XChat_DS_SASL_Success parameter missing")
	except:
		DebugLog(DL_ERROR, "XChat_DS_SASL_Success exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE
	
def XChat_DS_StopConnection(word, word_eol, userdata):
	global NotifyMessageThread
	print("DogeSoaker forcing reconnect...")
	NotifyMessageThread.AddCommand("RECONNECT")
	return xchat.EAT_NONE

def XChat_DS_ConnectionFailed(word, word_eol, userdata):
	global NotifyMessageThread
	try:
		print("DogeSoaker forcing reconnect...")
		NotifyMessageThread.AddCommand("RECONNECT")
	except:
		DebugLog(DL_ERROR, "XChat_DS_SASL_Success exception:")
		DebugLog(DL_ERROR, traceback.format_exc())
	return xchat.EAT_NONE

def XChat_DS_YouKicked(word, word_eol, userdata):
	# nick, host, channel, reason
	global DogeSoaker
	if not DogeSoaker:
		DebugLog(DL_ERROR, "XChat_DS_YouKicked validate DogeSoaker object failed")
		return
	elif len(word) >= 4:
		Nick 	= word[0]
		KickerNick 	= word[1]
		ChannelName = word[2]
		Reason		= word[3]
		DogeSoaker.HandleKick(Nick, ChannelName, Reason, KickerNick)
	else:
		DebugLog(DL_ERROR, "XChat_DS_YouKicked parameter missing")
	return xchat.EAT_NONE
	
def XChat_DS_Crash(word, word_eol, userdata):
	global UserMonitors, NotifyTipThread, NotifyCommandThread, NotifyMessageThread, TipoutThread, PrivMsgHandlerThread
	if len(word) < 2:
		print("Required format: /dscrash <type> [<channel>] (types: monitor <channel>, monitors, notifytip, notifycmd, notifypm, tipout, pm)")
		return xchat.EAT_ALL
	Type = word[1]
	if Type == "monitor":
		if len(word) != 3:
			print("Required format: /dscrash monitor <channel>")
			return xchat.EAT_ALL
		ChannelName = word[2]
		UserMonitor = UserMonitors.FindChannel(ChannelName)
		if UserMonitor == False:
			print("Channel " + ChannelName + " not found. Required format: /dscrash monitor <channel>")
			return xchat.EAT_ALL
		print("Crashing monitor for channel: " + ChannelName + "...")
		UserMonitor.CrashTest = True
	elif Type == "monitors":
		print("Crashing UserMonitors...")
		UserMonitors.CrashTest = True
	elif Type == "notifytip":
		print("Crashing NotifyTipThread...")
		NotifyTipThread.CrashTest = True
	elif Type == "notifycmd":
		print("Crashing NotifyCommandThread...")
		NotifyCommandThread.CrashTest = True
	elif Type == "notifypm":
		print("Crashing NotifyMessageThread...")
		NotifyMessageThread.CrashTest = True
	elif Type == "tipout":
		print("Crashing TipoutThread...")
		TipoutThread.CrashTest = True
	elif Type == "pm":
		print("Crashing PrivMsgHandlerThread...")
		PrivMsgHandlerThread.CrashTest = True
	else:
		print("Crash test, unknown type: " + Type)
	return xchat.EAT_ALL

def XChat_DS_Save(word, word_eol, userdata):
	global DogeSoaker
	if DogeSoaker.SaveConfig():
		print("Config saved.")
	else:
		print("Config save failed.")
	return xchat.EAT_ALL

def XChat_DS_Reload(word, word_eol, userdata):
	global IRC
	print("Reloading...")
	IRC.ReloadPlugin()
	return xchat.EAT_ALL
	
# pylint: enable=W0613
#-------------------------------------------------------------
	
if C_Auto_Start == False:
	IRC = False
	TextHandler = False
	IgnoreList = False
	IgnoreHostList = False
	TipbotList = False
	OperatorList = False
	DogeSoaker = False
	ConfigFile = False
	DebugLogger = False
	AuditLog = False
	NotifyTipThread = False
	NotifyCommandThread = False
	NotifyMessageThread = False
	Events = False
	PrivMsgHandlerThread = False
	UserMonitors = False
	TipoutThread = False
	TestModule = False
else:
	print("Auto starting...")
	IRC = TIRC()        

	TextHandler = TTextHandler()
	
	IgnoreList = TUserList()
	IgnoreHostList = TUserList()
	TipbotList = TUserList()
	OperatorList = TUserList()


	DogeSoaker = TDogeSoaker(C_Data_Directory)

	ConfigFile = TConfigFile(C_Data_Directory)
	DebugLogger = TDebugLogger(C_Data_Directory)
	AuditLog = TAuditLog(C_Data_Directory)

	NotifyTipThread = TNotifyThread()
	NotifyCommandThread = TNotifyThread()
	NotifyMessageThread = TNotifyThread()

	Events = TEventThread()

	PrivMsgHandlerThread = TPrivMsgHandlerThread()
	UserMonitors = TUserMonitors()
	TipoutThread = TTipoutThread()

	NotifyTipThread.NotifySpeed     = C_Notify_PM_Delay
	NotifyCommandThread.NotifySpeed = C_Notify_CMD_Delay
	NotifyMessageThread.NotifySpeed = C_Notify_Msg_Delay

	TestModule = TTestModule()

if C_XChat_Plugin:
	CommandHelpMessage = "/dshelp list command help"
	xchat.hook_unload(XChat_DS_Unload)

	xchat.hook_server('903', XChat_DS_SASL_Success) # RPL_SASLSUCCESS

	xchat.hook_print("Join",						XChat_DS_Join)
	xchat.hook_print("Part",						XChat_DS_Part)
	xchat.hook_print("Part With Reason",			XChat_DS_Part)
	xchat.hook_print("Quit",						XChat_DS_Quit)
	xchat.hook_print("Kick",						XChat_DS_Kick)
	xchat.hook_print("Private Message to Dialog", 	XChat_DS_PrivMsg)
	xchat.hook_print("Channel Message", 			XChat_DS_ChanMsg)
	xchat.hook_print("Channel Msg Hilight", 		XChat_DS_ChanMsg)
	xchat.hook_print("Channel Action", 				XChat_DS_ChanMsg)
	xchat.hook_print("Channel Action Hilight", 		XChat_DS_ChanMsg)
	xchat.hook_print("Change Nick", 				XChat_DS_ChangeNick)

	xchat.hook_print("You Join",					XChat_DS_Join)
	xchat.hook_print("You Part",					XChat_DS_Part)
	xchat.hook_print("You Part with Reason",		XChat_DS_Part)
	xchat.hook_print("You Kicked",					XChat_DS_YouKicked)

	xchat.hook_print("WhoIs Authenticated", 		XChat_DS_WhoIsAuthenticated)
	xchat.hook_print("WhoIs Name Line", 			XChat_DS_WhoIsNameLine)
	xchat.hook_print("WhoIs End", 					XChat_DS_WhoIsEnd)
	xchat.hook_print("Your Nick Changing", 			XChat_DS_ChangeNick)
	xchat.hook_print("Stop Connection", 			XChat_DS_StopConnection)
	xchat.hook_print("Connection Failed", 			XChat_DS_ConnectionFailed)

	xchat.hook_command("dshelp",    XChat_DS_Help, 	  	help=CommandHelpMessage)   
	xchat.hook_command("dsstart",   XChat_DS_Start, 	help=CommandHelpMessage)   
	xchat.hook_command("dsstop",    XChat_DS_Stop, 	  	help=CommandHelpMessage)   
	xchat.hook_command("dsrestart", XChat_DS_Restart,   help=CommandHelpMessage)   
	xchat.hook_command("dsreset",   XChat_DS_Reset,	  	help=CommandHelpMessage) # resets soak delay and clears active user list
	xchat.hook_command("dspause",   XChat_DS_Pause,     help=CommandHelpMessage)   
	xchat.hook_command("dsresume",  XChat_DS_Resume,    help=CommandHelpMessage)   
	xchat.hook_command("dsdebug",   XChat_DS_DebugDump, help=CommandHelpMessage)   
	xchat.hook_command("dstipbot",  XChat_DS_TipBot,    help=CommandHelpMessage)   
	xchat.hook_command("dstest",    XChat_DS_Test,      help=CommandHelpMessage)   
	xchat.hook_command("dsstatus",  XChat_DS_Status,    help=CommandHelpMessage)   
	xchat.hook_command("dscrash",  	XChat_DS_Crash,    	help=CommandHelpMessage)   
	xchat.hook_command("dssave",  	XChat_DS_Save,    	help=CommandHelpMessage)   
	xchat.hook_command("dsreload",  XChat_DS_Reload,    help=CommandHelpMessage)   

DefaultLanguageText = '''
#------------------------------------------
:en English (US)

EVENT_BAL_TIMEOUT				= "Balance query timed out."
EVENT_DONATE_TIMEOUT			= "Timedout waiting for donation tip. Please do !donate again before sending donation tip to try again."
EVENT_ANONSOAK_TIMEOUT			= "Timedout waiting for anonymous soak tip. Please do !anonsoak again before sending tip to try again."

SOAK_AUTHCHECK					= "Finding targets..."
SOAK_ANONYMOUS_NAME				= "Anonymous shibe"
SOAK_CHANMSG_HEADER_SINGLE		= "{0} is soaking 1 shibe with Ɖ{2}"
SOAK_CHANMSG_HEADER_MULTI		= "{0} is soaking {1} shibes with Ɖ{2}"
SOAK_CHANMSG_USERLIST_STYLELIST = ""
# These messages will be inserted as {1} into SOAK_REFUSE_BASE
SOAK_REFUSE_BASE				= "Sorry {0}, {1}. (silently returning tip)"
SOAK_REFUSE_RESTRICTED			= "usage currently restricted for testing"
SOAK_REFUSE_DISABLED			= "temporarily disabled"
SOAK_REFUSE_NOTIPCONFIRM		= "Doger is chasing his tail"
SOAK_REFUSE_MINTIP				= "not enough tip to go around"
SOAK_REFUSE_NOUSERS				= "no verified users active"
SOAK_REFUSE_NOTENOUGH			= "not enough tip to go around, at least Ɖ{0} required"
SOAK_REFUSE_SOAKBUSYTIPPING		= "still soaking for around {0} seconds"
SOAK_REFUSE_SOAKBUSY			= "still chasing tail..."

HEART_BEAT_CHAN_WARNING			= "Warning! Recovering from internal error. Please be patient while I learn the active users for this channel again."
ACTIVE_USERS_FAILED				= "I failed to find channel {0}. Try again a little later or double check the channel name."
ACTIVE_USERS					= "I see {0} active shibes, but only users identified with NickServ will be included."

# Bark statuses
STATUS_DISABLED					= "^AACTION barks (sorry, currently disabled)^A"
STATUS_READY					= "^AACTION barks (ready)^A"
STATUS_BUSYTIPPING				= "^AACTION barks (still soaking for around {0} seconds)^A"
STATUS_BUSY						= "^AACTION barks (still chasing tail)^A"

CHAN_HELP_GENERIC				= "Use !dshelp for help."
CHAN_HELP						= "Help: Send me tips and I will spread them out to active users in the channel. !tos for Terms of Service. For disputes or issues please contact the bot operator: {0}"
CHAN_HELP_UNKNOWNOP				= "<None!>"
CHAN_TOS						= "TOS: Doge Soaker is provided as a free service with absolutely no warranty or guarantee; Use at your own risk."
#------------------------------------------
'''


if C_Auto_Start:
	print("  auto start: enabled")
	print(DogeSoaker.GetModuleVersion())
	print(DogeSoaker.GetModuleHelpMessage())
	print("For command list: /dshelp")
	DogeSoaker.InitConfig(True) # LoadDefaultsOnFail
	print(DogeSoaker.GetModuleStatus())
	DogeSoaker.Start()
else:
	print("  auto start: off")

########################################################################	
# pylint: disable=W0105
'''	
------------------------------------------------------------------------
The MIT License (MIT)

Copyright (c) 2014 SoCo Software

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
------------------------------------------------------------------------
'''
# pylint: enable=W0105
########################################################################	
