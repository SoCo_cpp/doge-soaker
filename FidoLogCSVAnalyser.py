#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

if len(sys.argv) < 2:
	print("arg missing")
	exit()

inFileName = sys.argv[1]
outFileName = inFileName + ".analysis.csv"

class TUser:
	def __init__(self, name):
		self.name = name
		self.sent = 0
		self.conf = 0
		
	def incSent(self, value):
		self.sent = self.sent + value

	def incConf(self, value):
		self.conf = self.conf + value
		
		
users = []

	
def findUser(users, name):
	for user in users:
		if user.name == name:
			return user
	return False
	

with open(inFileName, 'r') as f:
	print("reading...")
	lines = f.readlines()
	print("reading...Done")
	for line in lines:
		if len(line):
			
			fields = line.split(",")
			timestamp = fields[0]
			rtype = fields[1]
			user = fields[2]
			value = int(fields[3])
			
			userObject = findUser(users, user)
			if userObject == False:
				userObject = TUser(user)
				users.append(userObject)
				userObject = findUser(users, user)
			if rtype == "send tip":
				userObject.incSent(value)
			elif  rtype == "tip confirm":
				userObject.incConf(value)
			else:
				print("Invalid record type: " + rtype)

outFile = open(outFileName, 'w')
			
for user in users:
	outData = str(user.name) + "," +  str(user.sent) + "," +  str(user.conf) + "," + str( user.sent - user.conf )  + "\n"
	outFile.write(outData)
outFile.close()
	
