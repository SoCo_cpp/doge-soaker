#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys, datetime

if len(sys.argv) < 2:
	print("arg missing")
	exit()
inFileName = sys.argv[1]
outFileName = inFileName + ".csv"

print("In File: " + inFileName)
print("Out File: " + outFileName)

Sent = []
Confirmed = []


	
def isInt(String):
	try:
		int(String)
		return True
	except ValueError:
		return False


def Dogecode(ValueString, DefaultValue):
	# "Ð200" -> (int) 200
	if len(ValueString) == 0:
		return DefaultValue # might not be an int
	if len(ValueString) >= 2 and ValueString[0] == chr(195) and ValueString[1] == chr(144): # 2 byte unicode Ð
		ValueString = ValueString[2:]
	elif ValueString[0] == 'Ð': # some single Ð
		ValueString = ValueString[1:]
	if not isInt(ValueString):
		return DefaultValue # might not be an int
	return int(ValueString)

outFile = open(outFileName, 'w')
with open(inFileName, 'r') as f:
	print("reading...")
	lines = f.readlines()
	print("reading...Done")
	for line in lines:
		if len(line):
			timestampstr = str(line[2:10])
			timestamp = timestampstr[:-3]
			if isInt(timestamp):
				timestampInt = int(timestamp)
				#timestamp = str(datetime.datetime.fromtimestamp(timestampInt))
				timestamp = str(datetime.datetime.utcfromtimestamp(timestampInt))
				print("timestamp: " + timestamp)
			endDelimiter = chr(9)
			#print(line)
			
			while len(line) and line[0] != endDelimiter:
				line = line[1:]
			line = line[1:] # get end del
			msg = ""
			#for i in range(0,len(line)):
			#	msg = msg + "[" + str(ord(line[i])) + "],"
			#print(msg)
			#print(line)
			if line.startswith("Successfully sent Ð"):
				#Successfully sent Ð8008 to moolah_.  You have Ð750 remaining in your account.
				#     0        1    2     3    4.      5   6    7    9        .... 
				words = line.split()
				User = words[4]
				if User[-1] == ".":
					User = User[:-1] # trim period off end of nick
				Value = Dogecode(words[2], "(?)" + words[2])
				Balance =  Dogecode(words[7], "(?)" + words[7])
				outData = timestampstr + ",tip confirm," + User + "," + str(Value) + "," + str(Balance) + "\n"
				print(outData)
				outFile.write(outData)
			elif line.startswith("send "):
				words = line.split()
				User = words[1]
				Value = words[2]
				Sent = [User, Value]
				outData = timestampstr + ",send tip," + User + "," + str(Value) + ",\n"
				print(outData)
				outFile.write(outData)
			
outFile.close()
			
			
