#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys, datetime

if len(sys.argv) < 2:
	print("arg missing")
	exit()
inFileName = sys.argv[1]
outFileName = inFileName + ".soak.csv"

print("In File: " + inFileName)
print("Out File: " + outFileName)

Sent = []
Confirmed = []

def ConStringList(word):
	s = ""
	f = True
	for w in word:
		if not f:
			s = s + " "
		s = s + w
	return s

	
def isInt(String):
	try:
		int(String)
		return True
	except ValueError:
		return False


def Dogecode(ValueString, DefaultValue):
	# "Ð200" -> (int) 200
	if len(ValueString) == 0:
		return DefaultValue # might not be an int
	if len(ValueString) >= 2 and ValueString[0] == chr(195) and ValueString[1] == chr(144): # 2 byte unicode Ð
		ValueString = ValueString[2:]
	elif len(ValueString) >= 2 and ValueString[0] == chr(198) and ValueString[1] == chr(137): # 2 byte unicode Ð
		ValueString = ValueString[2:]
	elif ValueString[0] == 'Ð': # some single Ð   
		ValueString = ValueString[1:]
	
	if not isInt(ValueString):
		#print(ValueString)
		#print("Ord: " + str(ord(ValueString[0])) + "," + str(ord(ValueString[1])) + "," + str(ord(ValueString[2])))
		return DefaultValue # might not be an int
	return int(ValueString)

outFile = open(outFileName, 'w')
with open(inFileName, 'r') as f:
	print("reading...")
	lines = f.readlines()
	print("reading...Done")
	LastValue = False
	LastUser = False
	LastTimeStamp = False
	for line in lines:
		words = line.split()
		if len(words) > 2:
			line = line[ ( len(words[0]) + len(words[1]) + 2) :] # trim timestamp off
			timestamp = words[0] + " " + words[1]
			timestamp = timestamp[:-1]
			#print("---'" + line + "'---")
			if line.startswith("Got soak request from : "):
				#16/05/14 10:34:36PM> Got soak request from : ExperimentalBot for value: 360
				if words[6] == ":":
					User = words[7]
					Value = words[10]
				else:
					User = words[6]
					Value = words[9]
				LastValue = Value
				LastUser = User
				LastTimeStamp = timestamp
				
				
			elif line.startswith("Tipout StepSoak sending channel message:"):
				#16/05/14 10:36:24PM> Tipout StepSoak sending channel message: SuchWow is soaking 9 shibes with Ɖ1111: 
				#print(line)
				User = words[7]
				UserCount = words[10]
				UserValue = Dogecode(words[13][:-1], "(error)???")
				UserList = ConStringList(words[14:])
				#print("'" + words[13] + "' -> '" + words[13][:-1] + "' = " + str(UserValue))
				if LastUser != False and LastValue != False and LastTimeStamp != False and LastUser == User:
					outData = "soak," + LastTimeStamp + "," + timestamp + "," + User + "," + str(LastValue) + "," + str(UserValue) + "," + UserList + "\n"
					print(outData)
					outFile.write(outData)
				else:
					print("error no last user/value/timestamp")
				LastValue = False
				LastUser = False
				LastTimeStamp = False
			
outFile.close()		
			
			
			
