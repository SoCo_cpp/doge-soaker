#---------------------------------------
# Ingore code is too awsome errors/warnings:
#---------------------------------------
# W0312 tabs instead of spaces
# C0301 line too long
# C0103 invalid name for constant
# C0111 missing doc string
# R0913 too many arguments
# W0602 global used with no assignment
# R0915 too many statements
# R0201 method could be a function 
# R0912 too many branches
# R0914 too many local variables
# R0902 too many instance attributes
# R0904 too many public methods
# E1121 too many positional arguments
# C0302 Too many lines in module

#C0303: Trailing whitespace
#C0325: Unnecessary parens after %r keyword
#---------------------------------------

pylint dogesoaker.py --disable=W0312,C0301,C0103,C0111,R0913,W0602,R0915,R0201,R0912,R0914,R0902,R0904,C0302,E1121,C0303,C0325  > lint.log
